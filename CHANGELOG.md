CHANGELOG
=========

## 0.21.5

- Ingress for docs now allows to add hosts.

## 0.21.4

- Refactored BAR API to be REST compliant and added more functionality, please check the docs
to start using the new API.
- BAR UI was moved to [ska-ser-bar-ui](https://gitlab.com/ska-telescope/ska-ser-bar-ui)
- Ingress now allows to add hosts.

## 0.21.3

- Configure the Unleash client to use the Unleash Proxy for feature toggle evaluation instead of directly communicating with the Unleash Server.
- Add required configuration for the Unleash Proxy, including:
  - UNLEASH_PROXY_URL
  - UNLEASH_PROXY_SDK_TOKEN
- Replaced occurrences of variables pointing to the Unleash Server, such as:
  - UNLEASH_API_URL → Updated to UNLEASH_PROXY_URL
  - UNLEASH_API_TOKEN → Updated to UNLEASH_PROXY_SDK_TOKEN

### **Release Notes Information**
- This `CHANGELOG.md` file was added starting with version 0.21.3. You can find the previous release's notes in the [Release Notes](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/releases/0.20.3) for version 0.20.3.