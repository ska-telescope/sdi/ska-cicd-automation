# flake8: noqa E501
import logging

import pytest
from fastapi.testclient import TestClient
from pytest_bdd import given, parsers, scenarios, then

from ska_cicd_automation.main import app
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook
from ska_cicd_automation.plugins.gitlab_mr.routers.mrevent import router

# Used to Print
LOGGER = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


@given(parsers.parse("Actual post event from json file {file}"))
def http_with_body(file: str):
    hook_info = MRHook.parse_file(path=file)
    response = client_router.post("/events/", json=hook_info.dict())
    pytest.confirm = response.status_code


@then(parsers.parse("Return code {message}"))
def check_message(message):
    assert int(message) == pytest.confirm


scenarios("./real_event.feature")
