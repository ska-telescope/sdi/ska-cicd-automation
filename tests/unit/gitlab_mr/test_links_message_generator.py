import json
from unittest.mock import AsyncMock, patch

import pytest

from ska_cicd_automation.plugins.gitlab_mr.models.links_message_generator import (  # noqa: E501
    LinksMessageGenerator,
)


@pytest.mark.asyncio
async def test_generate_metadata_empty_links():
    logger_name = "test_logger"
    generator = LinksMessageGenerator(logger_name)

    # No links added to the generator
    expected_json = json.dumps(generator.links)
    expected_output = "".join(
        (
            "<!--\nMRServiceID: ska-devsecops-mr-links-id\n",
            f"links: {expected_json}\n-->",
        )
    )

    result = await generator.generate_metadata()

    assert result == expected_output
    assert "{}" == expected_json


@pytest.mark.asyncio
async def test_generate_metadata_with_links():
    logger_name = "test_logger"
    generator = LinksMessageGenerator(logger_name)

    generator.links = {
        "google": "https://www.google.com",
        "github": "https://www.github.com",
    }

    # Expected JSON output within the HTML comments
    expected_json = "".join(
        (
            '{"google": "https://www.google.com", ',
            '"github": "https://www.github.com"}',
        )
    )
    expected_output = (
        "<!--\n"
        f"MRServiceID: {LinksMessageGenerator.message_marker}\n"
        f"links: {expected_json}\n"
        "-->"
    )

    result = await generator.generate_metadata()

    assert result == expected_output


@pytest.mark.asyncio
async def test_add_pretext():
    logger_name = "test_logger"
    generator = LinksMessageGenerator(logger_name)

    test_pretext = "This is a test pretext."

    await generator.add_pretext(test_pretext)

    assert generator.pretext == test_pretext


@pytest.mark.asyncio
@patch(
    "ska_cicd_automation.plugins.gitlab_mr.models.links_message_generator.logging"  # noqa: E501
)
async def test_add_posttext(mock_logger):
    logger_name = "test_logger"
    generator = LinksMessageGenerator(logger_name)

    test_posttext = "This is a test posttext."

    await generator.add_posttext(test_posttext)

    assert generator.posttext == test_posttext


@pytest.mark.asyncio
async def test_get_message():
    logger_name = "test_logger"
    generator = LinksMessageGenerator(logger_name)

    # Setup initial states
    generator.pretext = "Pretext here"
    generator.content = "Content here"
    generator.posttext = "Posttext here"

    # Mock generate_metadata
    mock_metadata_str = (
        "<!--\nMRServiceID: ska-devsecops-mr-links-id\nlinks: {}\n-->"
    )

    generator.generate_metadata = AsyncMock(return_value=mock_metadata_str)

    result = await generator.get_message()

    expected_result = "\n\n".join(
        [
            mock_metadata_str,
            "Pretext here",
            "Content here",
            "Posttext here",
        ]
    )
    assert result == expected_result


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.models.links_message_generator.LinksMessageGenerator.generate_links"  # noqa: E501
)
@patch(
    "ska_cicd_automation.plugins.gitlab_mr.models.links_message_generator.LinksMessageGenerator.generate_list"  # noqa: E501
)
@patch(
    "ska_cicd_automation.plugins.gitlab_mr.models.links_message_generator.LinksMessageGenerator.generate_namespace_header"  # noqa: E501
)
@patch("prettytable.PrettyTable.get_html_string")
@pytest.mark.asyncio
async def test_add_links_template(
    mock_get_html_string,
    mock_generate_namespace_header,
    mock_generate_list,
    mock_generate_links,
):
    logger_name = "test_logger"
    generator = LinksMessageGenerator(logger_name)

    # Setup mocks
    mock_generate_links.return_value = (
        ['<a href="htpps://namespace.example.com">Namespace</a>'],
        ['<a href="htpps://logs.example.com">Logs</a>'],
        ['<a href="htpps://metrics.example.com">Metrics</a>'],
    )
    mock_generate_namespace_header.return_value = "Namespace Header"
    mock_get_html_string.return_value = (
        "<table><tr><td>Table Content</td></tr></table>"
    )
    mock_generate_list.return_value = "Mocked list"

    urls = {
        "deployment_type": "prod",
        "kube_namespace": "default",
        "cluster": "clusterA",
        "timestamp": "2023-08-01T12:00:00Z",
    }

    await generator.add_links_template(urls)

    expected_content = "".join(
        [
            "<details>\n\nNamespace Header\n\n<table>",
            "<tr><td>Table Content</td></tr></table>",
            "\n\n<em>This deployment happened at ",
            "2023-08-01T12:00:00Z.</em>\n\n</details>",
        ]
    )

    assert generator.content in expected_content


@pytest.mark.asyncio
async def test_generate_namespace_header():

    deployment_type = "Test Deployment"
    namespace_name = "Test Namespace"
    cluster = "Test Cluster"

    generator = LinksMessageGenerator("test_logger")
    result = await generator.generate_namespace_header(
        deployment_type, namespace_name, cluster
    )
    expected_result = " | ".join(
        [
            "<summary>:label: <b>Deployment type:</b> Test Deployment",
            "<b>Namespace:</b> Test Namespace",
            "<b>Cluster:</b> Test Cluster</summary>",
        ]
    )

    # Verify
    assert result == expected_result


@pytest.mark.asyncio
async def test_generate_links():
    # Setup
    urls = {
        "namespace_ds": "http://namespace.example.com",
        "deviceserver_ds": "http://deviceserver.example.com",
        "database_ds": "http://database.example.com",
        "job_logs": "http://joblogs.example.com",
        "testpod_logs": "http://testpodlogs.example.com",
        "namespace_logs": "http://namespacelogs.example.com",
        "deviceconf_logs": "http://deviceconflogs.example.com",
        "deviceserver_logs": "http://deviceserverlogs.example.com",
        "databaseds_logs": "http://databasedslogs.example.com",
        "namespace_metrics": "http://namespacemetrics.example.com",
        "deviceserver_metrics": "http://deviceservermetrics.example.com",
        "workload_metrics": "http://workloadmetrics.example.com",
        "pod_metrics": "http://podmetrics.example.com",
    }

    generator = LinksMessageGenerator("test_logger")
    result = await generator.generate_links(urls)

    expected_ds_links = [
        '<a href="http://namespace.example.com">Namespace</a>',
        '<a href="http://deviceserver.example.com">Device Server</a>',
        '<a href="http://database.example.com">DatabaseDS</a>',
    ]

    expected_logs_links = [
        '<a href="http://joblogs.example.com">Job</a>',
        '<a href="http://testpodlogs.example.com">Test Pod</a>',
        '<a href="http://namespacelogs.example.com">Namespace</a>',
        "".join(
            (
                '<a href="http://deviceconflogs.example.com">',
                "Device Configuration Logs/Operator</a>",
            )
        ),
        '<a href="http://deviceserverlogs.example.com">DeviceServer</a>',
        '<a href="http://databasedslogs.example.com">DatabaseDS</a>',
    ]

    expected_metrics_links = [
        '<a href="http://namespacemetrics.example.com">Namespace Summary</a>',
        '<a href="http://deviceservermetrics.example.com">DeviceServer</a>',
        "".join(
            (
                '<a href="http://workloadmetrics.example.com">',
                "Compute Resources by Workload</a>",
            )
        ),
        '<a href="http://podmetrics.example.com">Compute resources by Pod</a>',
    ]

    # Verify
    assert result[0] == expected_ds_links
    assert result[1] == expected_logs_links
    assert result[2] == expected_metrics_links


@pytest.mark.asyncio
async def test_generate_list():
    items = ["Item A", "Item B", "Item C"]
    generator = LinksMessageGenerator("test_logger")

    result = await generator.generate_list(items)

    expected_result = "- Item A\n- Item B\n- Item C"
    assert result == expected_result
