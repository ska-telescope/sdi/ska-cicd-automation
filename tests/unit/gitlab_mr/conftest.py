import pytest


def pytest_bdd_apply_tag(tag, function):
    """Skip manual tests"""
    if tag == "manual":
        marker = pytest.mark.skip(reason="This is a manual debugging test")
        marker(function)
        return True
    else:
        # Fall back to the default behavior of pytest-bdd
        return None
