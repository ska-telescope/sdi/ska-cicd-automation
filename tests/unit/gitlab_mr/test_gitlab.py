# flake8: noqa E501
import asyncio
import base64
import collections
import importlib
import json
import logging
import os
from json import JSONDecodeError
from unittest.mock import ANY, AsyncMock, MagicMock, patch

import pytest
from fastapi import BackgroundTasks
from fastapi.testclient import TestClient
from gidgetlab.exceptions import GitLabException
from jira.exceptions import JIRAError
from pytest_bdd import given, parsers, scenarios, then
from ska_cicd_services_api.readthedocs_api import HTTPException

from ska_cicd_automation.main import app
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import (
    JobURLs,
    MRHook,
    PipelineHook,
)
from ska_cicd_automation.plugins.gitlab_mr.routers.mrevent import (
    STATUS_CHECK_FEATURE_TOGGLE,
    CheckResult,
    check_should_run,
    clear_dashboards_and_logs_comment,
    mr_metrics,
    perform_check,
    process_checks,
    process_mr,
    process_urls,
    router,
    set_status_check,
)

# Used to Print
LOGGER = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


# Mock Class to hold methods
# This class is populated with dynamically generated methods later
class GitLabApiMock:
    mock = "This is a Mock Class"

    def throw_exception(self, *args, **kwargs):
        raise GitLabException


class JiraApiMock:
    mock = "This is a Mock Class"

    async def authenticate(self):
        pass

    def throw_exception(self, *args, **kwargs):
        raise JIRAError


class ReadTheDocsApiMock:
    mock = "This is a Mock Class"

    def throw_exception(self, *args, **kwargs):
        raise HTTPException(*args, **kwargs)


class NexusApiMock:
    mock = "This is a Mock Class"

    def throw_exception(self, *args, **kwargs):
        raise HTTPException(*args, **kwargs)


# Helper functions to configure return values for mocks
def append_pair_to_mock_method(method, key, value, service):
    pytest.mock_service[service].return_dict[method][key] = value
    return pytest.mock_service[service].return_dict[method]


def append_object_with_attribute_to_mock_method(
    method, attribute, value, service
):
    mock_obj = MagicMock
    setattr(mock_obj, attribute, value)
    pytest.mock_service[service].return_dict[method] = mock_obj
    return pytest.mock_service[service].return_dict[method]


def append_value_to_mock_method(method, value, service):
    pytest.mock_service[service].return_dict[method] = value
    return pytest.mock_service[service].return_dict[method]


def append_value_file_to_mock_method(method, file, service):
    with open(file, "rb") as myfile:
        data = myfile.read()

    attrs = {f"{method}.return_value": data}

    pytest.mock_service[service].configure_mock(**attrs)
    return pytest.mock_service[service]


async def yield_to_mock_method(method, service):
    results = pytest.mock_service[service].return_dict[method]
    for i in results:
        yield i


def format_value(value):
    try:
        if value != "None":
            value = json.loads(value)
        else:
            value = None
    except JSONDecodeError:
        # If it's not json go with string
        pass
    return value


# References to the services to Mock
serviceList = {
    "gitlab": GitLabApiMock(),
    "jira": JiraApiMock(),
    "rtd": ReadTheDocsApiMock(),
    "nexus": NexusApiMock(),
}

pytest.mock_service = {}
pytest.mr_hook_dict = collections.defaultdict(dict)
pytest.pipeline_hook_dict = collections.defaultdict(dict)


@pytest.fixture(autouse=True)
def setup_services():
    yield
    pytest.mock_service = {}
    pytest.mr_hook_dict = collections.defaultdict(dict)
    pytest.pipeline_hook_dict = collections.defaultdict(dict)


# TODO set up mock APIs
# def set_up_mock_apis(api):
#     pass


@given(parsers.parse("Main route"))
def check_server():
    response = client.get("/")
    pytest.confirm = response.status_code


@given(
    parsers.parse(
        "Authenticate {method} http request to {target} with Header "
        "env var name is {token_header_env} and Token env var name "
        "is {token_env}"
    )
)
def good_token_auth(
    token_header_env: str, token_env: str, method: str, target: str
):
    assert (
        token_header_env is not None
    ), "Token Header (token_header_env) is not found on plugins.conf.yaml"

    assert (
        token_env is not None
    ), "Token (token_env) is not found on plugins.conf.yaml"

    token_header = os.getenv(token_header_env)
    token = os.getenv(token_env)

    assert token_header is not None, "Token Header env variable is empty"

    assert token is not None, "Token env variable is empty"

    method = method.lower()
    if method == "post":
        response = client.post(target, headers={token_header: token})
    elif method == "get":
        response = client.get(target, headers={token_header: token})

    pytest.confirm = response.status_code


@given(
    parsers.parse(
        "Authenticate {method} http request to {target} with "
        "Header env var name is {token_header_env} and wrong token"
    )
)
def bad_token_auth(token_header_env: str, method: str, target: str):
    assert (
        token_header_env is not None
    ), "Token Header (token_header_env) is not found on plugins.conf.yaml"

    token_header = os.getenv(token_header_env)

    assert token_header is not None, "Token Header env variable is empty"

    token = "wrong token"

    method = method.lower()
    if method == "post":
        response = client.post(target, headers={token_header: token})
    elif method == "get":
        response = client.get(target, headers={token_header: token})

    pytest.confirm = response.status_code


@given(parsers.parse("Actual post event from json file {file}"))
def http_with_body(file: str):
    hook_info = MRHook.parse_file(path=file)
    response = client_router.post("/events/", json=hook_info.dict())
    pytest.confirm = response.status_code


@then(parsers.parse("Return code {message}"))
def check_message(message):
    assert int(message) == pytest.confirm


@given(parsers.parse("a mock {service} API"))
def setup_bare_mock_spec(service):
    api = serviceList.get(service)
    if api is None:
        assert False, f"Please specify a valid service: {service}"

    # Create the mock class
    mock_api = AsyncMock(api)
    # Save the return dict
    placeholder_return_dict = {}
    mock_api.return_dict = placeholder_return_dict

    # Save the mocked api
    pytest.mock_service[service] = mock_api


@given(parsers.parse("a mock {service} API with {method} method"))
def setup_mock_spec(service, method):
    api = serviceList.get(service)
    if api is None:
        assert False, f"Please specify a valid service: {service}"

    # ST-1030: AsyncMock can be called as if it is a function
    setattr(api, method, AsyncMock(return_value={}))

    # Create the mock class
    mock_api = AsyncMock(api)

    # Save the return dict
    placeholder_return_dict = {}
    placeholder_return_dict[method] = {}
    mock_api.return_dict = placeholder_return_dict

    # Save the mocked api
    pytest.mock_service[service] = mock_api


@given(parsers.parse("with {method} method in {service} api"))
def append_mock_spec(method, service):
    # ST-1030: The new method needs to be appended to the existing service
    api = pytest.mock_service[service]

    # ST-1030: AsyncMock can be called as if it is a function
    setattr(api, method, AsyncMock(return_value={}))

    # Save the return dict
    pytest.mock_service[service].return_dict[method] = {}


@given(
    parsers.parse(
        "append key-value pair of {key} with {value}"
        " for {method} method in {service} api"
    )
)
def append_to_mock(key, value, method, service):
    value = format_value(value)
    result_dict = append_pair_to_mock_method(method, key, value, service)
    attrs = {f"{method}.return_value": result_dict}
    pytest.mock_service[service].configure_mock(**attrs)


@given(
    parsers.parse(
        "append object with attribute {attribute} and {value}"
        " for {method} method in {service} api"
    )
)
def append_object_to_mock(method, attribute, value, service):
    value = format_value(value)
    result_dict = append_object_with_attribute_to_mock_method(
        method, attribute, value, service
    )
    attrs = {f"{method}.return_value": result_dict}
    pytest.mock_service[service].configure_mock(**attrs)


@given(
    parsers.parse(
        "append key-value pair of {key} with file {file}"
        " for {method} method in {service} api"
    )
)
def append_to_mock_from_file(key, file, method, service):
    with open(file, "rb") as fd:
        base64_bytes = base64.b64encode(fd.read())
    value = format_value(base64_bytes.decode("utf-8"))
    result_dict = append_pair_to_mock_method(method, key, value, service)
    attrs = {f"{method}.return_value": result_dict}
    pytest.mock_service[service].configure_mock(**attrs)


@given("append diff from <old_path> to <new_path>")
def append_diff(old_path, new_path):
    append_to_mock(
        "diffs",
        '[{"old_path": "' + old_path + '", "new_path": "' + new_path + '"}]',
        "get_single_single_mr_diff_version",
        "gitlab",
    )


@given(parsers.parse("append {value} for {method} method in {service} api"))
def append_yield_to_mock(value, method, service):
    value = format_value(value)
    append_value_to_mock_method(method, value, service)


@given(
    parsers.parse(
        "append commits <commit_message> for {method} method in {service} api"
    )
)
def append_yield_to_mock_for_commits(commit_message, method, service):
    value = f'[{{"short_id": "FAKEID", "message": {commit_message}}}]'
    value = format_value(value)
    append_value_to_mock_method(method, value, service)


@given(
    parsers.parse(
        "append <tree_items> with <tree_types> for {method} "
        "method in {service} api"
    )
)
def append_yield_to_mock_for_examples_repo_tree(
    tree_items, tree_types, method, service
):
    tree_items = [item.strip() for item in tree_items.strip().split(",")]
    tree_types = [item.strip() for item in tree_types.strip().split(",")]
    value = [
        {"path": tree_item, "type": tree_type}
        for tree_item, tree_type in zip(tree_items, tree_types)
    ]
    append_yield_to_mock(json.dumps(value), method, service)


@given(
    parsers.parse("append file {value} for {method} method in {service} api")
)
def append_yield_file_to_mock(value, method, service):
    append_value_file_to_mock_method(method, value, service)


@given(
    parsers.parse(
        "finally create generator for {method} method in {service} api"
    )
)
def create_async_generator(method, service):
    result = yield_to_mock_method(method, service)
    attrs = {f"{method}.return_value": result}
    pytest.mock_service[service].configure_mock(**attrs)


@given(parsers.parse("throw exception for {method} method in {service} api"))
def add_exception_to_mock(method, service):
    api = serviceList.get(service)
    attrs = {f"{method}.side_effect": api.throw_exception}
    pytest.mock_service[service].configure_mock(**attrs)


@given(parsers.parse("an MR Hook with field {field} with {value}"))
def populate_mr_hook(field, value):
    value = format_value(value)
    fields = field.split(".")
    dic = pytest.mr_hook_dict
    for field in fields[:-1]:
        if dic[field] is None:
            dic[field] = {}
        dic = dic[field]
    dic[fields[-1]] = value


@given(parsers.parse("an Pipeline Hook with field {hook_file}"))
def populate_pipeline_hook(hook_file):
    with open(hook_file, "r", encoding="utf-8") as myfile:
        data = myfile.read()

    pytest.pipeline_hook_dict = str(data)


@then(parsers.parse("pipeline hook {check_name} should return {result}"))
def check_check_pipeline(check_name, result):
    mr_hook = json.loads(pytest.pipeline_hook_dict)
    mr_hook = PipelineHook.parse_obj(mr_hook)
    result = json.loads(result)
    try:
        mod = importlib.import_module(
            f"ska_cicd_automation.plugins.gitlab_mr.models.pipeline_checks.{check_name}"
        )
    except:
        mod = importlib.import_module(
            f"ska_cicd_automation.plugins.gitlab_mr.models.mr_checks.{check_name}"
        )
    check_class = getattr(mod, check_name.title().replace("_", ""))
    check = check_class(pytest.mock_service["gitlab"], "pytest-logging")
    assert result == asyncio.run(check.check(mr_hook, 1, 2))


@then(parsers.parse("{check_name} should return {result}"))
def check_check(check_name, result):
    mr_hook = MRHook.parse_obj(pytest.mr_hook_dict)
    result = json.loads(result)
    try:
        mod = importlib.import_module(
            f"ska_cicd_automation.plugins.gitlab_mr.models.pipeline_checks.{check_name}"
        )
    except:
        mod = importlib.import_module(
            f"ska_cicd_automation.plugins.gitlab_mr.models.mr_checks.{check_name}"
        )

    check_class = getattr(mod, check_name.title().replace("_", ""))
    if "jira" in pytest.mock_service:
        check = check_class(
            pytest.mock_service["gitlab"],
            pytest.mock_service["jira"],
            "pytest-logging",
        )
    elif "rtd" in pytest.mock_service:
        check = check_class(
            pytest.mock_service["gitlab"],
            pytest.mock_service["rtd"],
            "pytest-logging",
        )
    elif "nexus" in pytest.mock_service:
        check = check_class(
            pytest.mock_service["gitlab"],
            pytest.mock_service["nexus"],
            "pytest-logging",
        )
    else:
        check = check_class(pytest.mock_service["gitlab"], "pytest-logging")
    bool_result = asyncio.run(check.check(mr_hook, 1, 2))
    pytest.last_mitigation_strategy = asyncio.run(check.mitigation_strategy())
    assert result == bool_result


@then(parsers.parse(("mitigation strategy should include {message}")))
def check_check_mitigation(message):
    LOGGER.info(pytest.last_mitigation_strategy)
    assert message in pytest.last_mitigation_strategy


@then(parsers.parse("{check_name} should return <result>"))
def check_check_for_examples(check_name, result):
    check_check(check_name, result)


@then(
    parsers.parse(
        "mr event router should return a string starting with {result}"
    )
)
def check_marvin_mr_event(result):
    mr_hook = MRHook.parse_obj(pytest.mr_hook_dict)
    mod = importlib.import_module(
        "ska_cicd_automation.plugins.gitlab_mr.routers"
    )
    mr_router = getattr(mod, "mrevent")
    # ST-1551: Mocking background tasks and passing empty string
    with patch("fastapi.BackgroundTasks.add_task") as mock:

        def side_effect(*args, **kwargs):
            pass

        mock.side_effect = side_effect

        assert asyncio.run(
            mr_router.new_event(mr_hook, BackgroundTasks())
        ).startswith(result)


@then(parsers.parse("{method} method in {service} api was not called"))
def check_method_not_called(method, service):
    api = pytest.mock_service[service]
    getattr(api, method).assert_not_called()


@then(parsers.parse("{method} method in {service} api was called"))
def check_method_called(method, service):
    api = pytest.mock_service[service]
    getattr(api, method).assert_called()


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_filters",
    [{"projects": [123], "subgroups": [], "run_checks": ["check1"]}],
)
def test_check_should_run_project_in_projects_list():
    mr_body = MagicMock(
        project=MagicMock(id=123), path_with_namespace="group/subgroup/project"
    )
    assert check_should_run(mr_body, "check1") is True


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_filters",
    [
        {
            "projects": [],
            "subgroups": ["group/subgroup/project"],
            "run_checks": ["check1"],
        }
    ],
)
def test_check_should_run_subgroup_in_subgroups_list():
    mr_body = MagicMock(
        project=MagicMock(id=123), path_with_namespace="group/subgroup/project"
    )
    assert check_should_run(mr_body, "check1") is True


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_filters",
    [{"projects": [], "subgroups": [], "run_checks": ["check1"]}],
)
def test_check_should_run_no_project_or_subgroup():
    mr_body = MagicMock(
        project=MagicMock(id=123), path_with_namespace="group/subgroup/project"
    )
    assert check_should_run(mr_body, "check1") is True


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_filters",
    [
        {
            "projects": [123],
            "subgroups": ["group/subgroup/project"],
            "run_checks": ["check1"],
        }
    ],
)
def test_check_should_run_check_not_in_run_checks():
    mr_body = MagicMock(
        project=MagicMock(id=123), path_with_namespace="group/subgroup/project"
    )
    assert check_should_run(mr_body, "check2") is False


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_filters",
    [
        {
            "projects": [123],
            "subgroups": ["group/subgroup/project"],
            "skip_checks": ["check1"],
        }
    ],
)
def test_check_should_run_check_in_skip_checks_list():
    mr_body = MagicMock(
        project=MagicMock(id=123), path_with_namespace="group/subgroup/project"
    )
    assert check_should_run(mr_body, "check1") is False


@patch(
    "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_filters",
    [
        {
            "projects": [123],
            "subgroups": ["group/subgroup/project"],
            "skip_checks": ["check1"],
        }
    ],
)
def test_check_should_run_check_not_in_skip_checks_list():
    mr_body = MagicMock(
        project=MagicMock(id=123), path_with_namespace="group/subgroup/project"
    )
    assert check_should_run(mr_body, "check2") is True


@pytest.mark.asyncio
async def test_set_status_check():
    # Mock aiohttp.ClientSession
    mock_session = MagicMock()
    mock_session.__aenter__ = AsyncMock(return_value=mock_session)
    mock_session.__aexit__ = AsyncMock(return_value=None)

    # Mock GitLabApi
    mock_gitlab_api = MagicMock()
    mock_gitlab_api.get_status_checks = AsyncMock(
        return_value=[{"id": 123, "name": "Marvin"}]
    )
    mock_gitlab_api.set_status_check = AsyncMock()

    # Variables for the test
    proj_id = 123
    mr_id = 456
    commit = "abcdef"
    url = "url"
    status = "pending"

    # Patch aiohttp.ClientSession, GitLabApi, and feature_toggler.is_enabled
    with patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.aiohttp.ClientSession",
        return_value=mock_session,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.GitLabApi",
        return_value=mock_gitlab_api,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.feature_toggler.is_enabled",
        return_value=True,
    ) as mock_feature_toggle:
        await set_status_check(proj_id, mr_id, commit, url, status)

    mock_feature_toggle.assert_called_once_with(
        STATUS_CHECK_FEATURE_TOGGLE, fallback_function=ANY
    )
    mock_gitlab_api.get_status_checks.assert_awaited_once_with(proj_id, mr_id)
    mock_gitlab_api.set_status_check.assert_awaited_once_with(
        proj_id, mr_id, commit, 123, status
    )


# Testing prometheus counters behaviour in process_mr() in case MR is successfull
@pytest.mark.asyncio
async def test_successful_process_mr():
    # Mock aiohttp.ClientSession
    mock_session = MagicMock()
    mock_session.__aenter__ = AsyncMock(return_value=mock_session)
    mock_session.__aexit__ = AsyncMock(return_value=None)

    # Mock GitLabApi
    mock_gitlab_api = MagicMock()
    mock_gitlab_api.get_project_settings_info = AsyncMock(
        return_value={"tag_list": ["ci", "dev"]}
    )

    # Mock functions
    mock_set_status_check = AsyncMock()
    mock_generate_marvin_review = AsyncMock()
    mock_message_generator = MagicMock()
    mock_clear_dashboards_and_logs_comment = AsyncMock()

    # Mock process_checks to return a CheckResult with block_mr=False
    check_result1 = CheckResult(
        name="test1", result=True, type=MessageType.INFO, runtime=1.0
    )
    check_result2 = CheckResult(
        name="test2", result=True, type=MessageType.INFO, runtime=2.0
    )
    check_result = [check_result1, check_result2]
    mock_process_checks = AsyncMock(return_value=check_result)

    # Variables for the test
    proj_id = 123
    mr_id = 456
    commit = "abcdef"
    mr_url = "http://test.com/mr/1"

    body = MagicMock()
    body.object_attributes.iid = mr_id
    body.project.id = proj_id
    body.object_attributes.state = "opened"
    body.object_attributes.url = mr_url
    body.object_attributes.last_commit.id = commit

    project_tags_blocklist = {"external"}

    # Patch aiohttp.ClientSession, GitLabApi, and functions/variables
    with patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.aiohttp.ClientSession",
        return_value=mock_session,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.GitLabApi",
        return_value=mock_gitlab_api,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.set_status_check",
        mock_set_status_check,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.process_checks",
        mock_process_checks,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.generate_marvin_review",
        mock_generate_marvin_review,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.clear_dashboards_and_logs_comment",
        mock_clear_dashboards_and_logs_comment,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.project_tags_blocklist",
        project_tags_blocklist,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.MessageGenerator",
        return_value=mock_message_generator,
    ), patch.object(
        mr_metrics, "save_metrics"
    ):

        await process_mr(body)

    # Assertions
    mock_gitlab_api.get_project_settings_info.assert_awaited_once_with(proj_id)
    mock_set_status_check.assert_any_await(
        proj_id, mr_id, commit, mr_url, "pending"
    )
    mock_process_checks.assert_any_await(
        body, proj_id, mr_id, "mr_checks", mock_message_generator
    )
    mock_generate_marvin_review.assert_any_await(
        proj_id, mr_id, check_result, mock_message_generator
    )
    mock_clear_dashboards_and_logs_comment.assert_any_await(proj_id, mr_id)
    mock_set_status_check.assert_any_await(
        proj_id, mr_id, commit, mr_url, "passed"
    )
    assert (
        mr_metrics.mr_runtime_summary.labels(
            proj_id=proj_id, mr_id=mr_id
        )._count.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test1", proj_id=proj_id
        )._count.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test1", proj_id=proj_id
        )._sum.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test2", proj_id=proj_id
        )._count.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test2", proj_id=proj_id
        )._sum.get()
        == 2
    )
    assert (
        mr_metrics.mr_pass_counter.labels(
            proj_id=proj_id, mr_id=mr_id
        )._value.get()
        == 1
    )
    assert (
        mr_metrics.mr_fail_counter.labels(
            proj_id=proj_id, mr_id=mr_id
        )._value.get()
        == 0
    )

    mr_metrics.mr_runtime_summary.labels(
        proj_id=proj_id, mr_id=mr_id
    )._count.set(0)
    mr_metrics.check_runtime_summary.labels(
        check_name="test1", proj_id=proj_id
    )._count.set(0)
    mr_metrics.check_runtime_summary.labels(
        check_name="test2", proj_id=proj_id
    )._count.set(0)
    mr_metrics.mr_pass_counter.labels(proj_id=proj_id, mr_id=mr_id)._value.set(
        0
    )
    mr_metrics.mr_fail_counter.labels(proj_id=proj_id, mr_id=mr_id)._value.set(
        0
    )


# Testing prometheus counters behaviour in process_mr() in case MR fails
@pytest.mark.asyncio
async def test_unsuccessful_process_mr():
    # Mock aiohttp.ClientSession
    mock_session = MagicMock()
    mock_session.__aenter__ = AsyncMock(return_value=mock_session)
    mock_session.__aexit__ = AsyncMock(return_value=None)

    # Mock GitLabApi
    mock_gitlab_api = MagicMock()
    mock_gitlab_api.get_project_settings_info = AsyncMock(
        return_value={"tag_list": ["ci", "dev"]}
    )

    # Mock functions
    mock_set_status_check = AsyncMock()
    mock_generate_marvin_review = AsyncMock()
    mock_message_generator = MagicMock()
    mock_clear_dashboards_and_logs_comment = AsyncMock()

    # Mock process_checks to return a CheckResult with block_mr=False
    check_result1 = CheckResult(
        name="test3", result=False, type=MessageType.FAILURE, runtime=4.0
    )
    check_result2 = CheckResult(
        name="test4", result=True, type=MessageType.INFO, runtime=2.5
    )
    check_result = [check_result1, check_result2]
    mock_process_checks = AsyncMock(return_value=check_result)

    # Variables for the test
    proj_id = 123
    mr_id = 456
    commit = "abcdef"
    mr_url = "http://test.com/mr/1"

    body = MagicMock()
    body.object_attributes.iid = mr_id
    body.project.id = proj_id
    body.object_attributes.state = "opened"
    body.object_attributes.url = mr_url
    body.object_attributes.last_commit.id = commit

    project_tags_blocklist = {"external"}

    # Patch aiohttp.ClientSession, GitLabApi, and functions/variables
    with patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.aiohttp.ClientSession",
        return_value=mock_session,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.GitLabApi",
        return_value=mock_gitlab_api,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.set_status_check",
        mock_set_status_check,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.process_checks",
        mock_process_checks,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.generate_marvin_review",
        mock_generate_marvin_review,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.clear_dashboards_and_logs_comment",
        mock_clear_dashboards_and_logs_comment,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.project_tags_blocklist",
        project_tags_blocklist,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.MessageGenerator",
        return_value=mock_message_generator,
    ), patch.object(
        mr_metrics, "save_metrics"
    ):

        await process_mr(body)

    # Assertions
    mock_gitlab_api.get_project_settings_info.assert_awaited_once_with(proj_id)
    mock_set_status_check.assert_any_await(
        proj_id, mr_id, commit, mr_url, "pending"
    )
    mock_process_checks.assert_any_await(
        body, proj_id, mr_id, "mr_checks", mock_message_generator
    )
    mock_generate_marvin_review.assert_any_await(
        proj_id, mr_id, check_result, mock_message_generator
    )
    mock_clear_dashboards_and_logs_comment.assert_any_await(proj_id, mr_id)
    mock_set_status_check.assert_any_await(
        proj_id, mr_id, commit, mr_url, "failed"
    )
    assert (
        mr_metrics.mr_runtime_summary.labels(
            proj_id=proj_id, mr_id=mr_id
        )._count.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test3", proj_id=proj_id
        )._count.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test3", proj_id=proj_id
        )._sum.get()
        == 4
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test4", proj_id=proj_id
        )._count.get()
        == 1
    )
    assert (
        mr_metrics.check_runtime_summary.labels(
            check_name="test4", proj_id=proj_id
        )._sum.get()
        == 2.5
    )
    assert (
        mr_metrics.mr_pass_counter.labels(
            proj_id=proj_id, mr_id=mr_id
        )._value.get()
        == 0
    )
    assert (
        mr_metrics.mr_fail_counter.labels(
            proj_id=proj_id, mr_id=mr_id
        )._value.get()
        == 1
    )

    mr_metrics.mr_runtime_summary.labels(
        proj_id=proj_id, mr_id=mr_id
    )._count.set(0)
    mr_metrics.check_runtime_summary.labels(
        check_name="test3", proj_id=proj_id
    )._count.set(0)
    mr_metrics.check_runtime_summary.labels(
        check_name="test4", proj_id=proj_id
    )._count.set(0)
    mr_metrics.mr_pass_counter.labels(proj_id=proj_id, mr_id=mr_id)._value.set(
        0
    )
    mr_metrics.mr_fail_counter.labels(proj_id=proj_id, mr_id=mr_id)._value.set(
        0
    )
    # mock_save_metrics.assert_not_called()


# Testing prometheus check_fail_counter behaviour in perform_check
@pytest.mark.asyncio
async def test_check_fail_counter():
    # Create mock objects for the dependencies
    check = AsyncMock()
    check.check = AsyncMock(return_value=False)  # Simulate a failing check
    check.type = AsyncMock(return_value="type")
    check.description = AsyncMock(return_value="description")
    check.mitigation_strategy = AsyncMock(return_value="mitigation")
    check.feature_toggle = "feature_toggle"

    mr_message = AsyncMock()
    mr_event = MagicMock()
    proj_id = 123
    mr_id = 456

    with patch.object(mr_metrics, "save_metrics"):

        result = await perform_check(
            check, mr_message, mr_event, proj_id, mr_id
        )

    # Assertions
    assert result.result is False
    assert (
        mr_metrics.check_fail_counter.labels(
            check_name=check.__class__.__name__, proj_id=proj_id
        )._value.get()
        == 1
    )

    # Reset the counter after the test
    mr_metrics.check_fail_counter.labels(
        check_name=check.__class__.__name__, proj_id=proj_id
    )._value.set(0)


# Testing prometheus check_exception_counter behaviour in process_checks
@pytest.mark.asyncio
async def test_exception_counter():
    # Mock the MRHook and MessageGenerator
    body = MagicMock()
    mr_message = MagicMock()
    proj_id = 123
    mr_id = 456
    check_type = "some_check_type"

    # Mock the AioHttpSessionManager
    session_manager_mock = AsyncMock()
    session_manager_mock.__aenter__.return_value = session_manager_mock
    session_manager_mock.__aexit__.return_value = False

    # Mock the checks dictionary
    checks_mock = {
        "some_check_type": [
            {"name": "check1", "class": MagicMock, "apis": MagicMock()},
            {"name": "check2", "class": MagicMock, "apis": MagicMock()},
        ]
    }

    # Mock the check_should_run function to always return True
    check_should_run_mock = MagicMock(return_value=True)

    with patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.checks",
        checks_mock,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.check_should_run",
        check_should_run_mock,
    ), patch.object(
        mr_metrics, "save_metrics"
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.perform_check"
    ) as mock_perform_check:

        # Set the side_effect for perform_check
        mock_perform_check.side_effect = [
            CheckResult(
                name="check1", result=True, type="type", runtime=0.0
            ),  # First check succeeds
            CheckResult(
                name="check2",
                result=Exception("Test exception"),
                type="type",
                runtime=0.0,
            ),  # Second check fails
        ]

        # Call the method under test
        results = await process_checks(
            body, proj_id, mr_id, check_type, mr_message
        )

        # Assertions
        assert results[0].result is True

        # Verify that the exception counter was incremented for the exception
        assert (
            mr_metrics.check_exception_counter.labels(
                check_name="check2",
                exception_type="Exception",
                proj_id=proj_id,
                mr_id=mr_id,
            )._value.get()
            == 1
        )

        # Reset the counter after the test
        mr_metrics.check_exception_counter.labels(
            check_name="check2",
            exception_type="Exception",
            proj_id=proj_id,
            mr_id=mr_id,
        )._value.set(0)


@pytest.mark.asyncio
async def test_process_urls():

    job_urls = JobURLs(
        project_id="123",
        mr_id="456",
        kube_namespace="default",
        deployment_type="dev",
        timestamp="test",
        namespace_ds="https://url.example",
        deviceserver_ds="https://url.example",
        database_ds="https://url.example",
        job_logs="https://url.example",
        testpod_logs="https://url.example",
        namespace_logs="https://url.example",
        deviceconf_logs="https://url.example",
        deviceserver_logs="https://url.example",
        databaseds_logs="https://url.example",
        namespace_metrics="https://url.example",
        deviceserver_metrics="https://url.example",
        workload_metrics="https://url.example",
        pod_metrics="https://url.example",
    )

    # Mock GitLabApi
    mock_gitlab_api = AsyncMock()
    mock_header = "\n".join(
        [
            "<!--",
            "MRServiceID: ska-devsecops-mr-links-id",
            "links: {}",
            "-->",
        ]
    )

    mock_gitlab_api.get_merge_request_comments = AsyncMock()
    mock_gitlab_api.get_merge_request_comments.return_value.__aiter__.return_value = [
        {"body": mock_header}
    ]
    mock_comment_executor = AsyncMock()

    mock_marvin_comments = MagicMock()
    mock_marvin_comments.get_random_links_quote = MagicMock()
    mock_marvin_comments.get_random_links_quote.return_value = (
        "Final Marvin Quote."
    )

    # Patch aiohttp.ClientSession, GitLabApi, and feature_toggler.is_enabled
    with patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.aiohttp.ClientSession",
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.GitLabApi",
        return_value=mock_gitlab_api,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.LinksMessageGenerator.message_marker",
        new="ska-devsecops-mr-links-id",
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.CommentExecutor",
        return_value=mock_comment_executor,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.MarvinComments",
        return_value=mock_marvin_comments,
    ):
        result = await process_urls(job_urls)

    expected_comment = (
        "<!--\nMRServiceID: ska-devsecops-mr-links-id\nlinks: ",
        '{"default": {"kube_namespace": "default", "deployment_type": ',
        '"dev", "cluster": null, "timestamp": "test", "namespace_ds": ',
        '"https://url.example", "deviceserver_ds": "https://url.example", ',
        '"database_ds": "https://url.example", "job_logs": "https://url.example", ',
        '"testpod_logs": "https://url.example", "namespace_logs": "https://url.example", '
        '"deviceconf_logs": "https://url.example", "deviceserver_logs": "https://url.example", ',
        '"databaseds_logs": "https://url.example", "namespace_metrics": "https://url.example", ',
        '"deviceserver_metrics": "https://url.example", "workload_metrics": "https://url.example", ',
        '"pod_metrics": "https://url.example"}}\n-->\n\n# :page_with_curl: ',
        "Dashboards and Logs\n\n<details>\n\n<summary>:label: <b>Deployment type:</b> dev | ",
        "<b>Namespace:</b> default | <b>Cluster:</b> None</summary>\n\n<table>\n    ",
        "<thead>\n        <tr>\n            <th>Headlamp Dashboards</th>\n            ",
        "<th>Kibana Logs</th>\n            <th>Grafana Metrics</th>\n        </tr>\n    ",
        '</thead>\n    <tbody>\n        <tr>\n            <td>- <a href="https://url.example">',
        'Namespace</a><br>- <a href="https://url.example">Device Server</a><br>- ',
        '<a href="https://url.example">DatabaseDS</a></td>\n            <td>- <a ',
        'href="https://url.example">Job</a><br>- <a href="https://url.example">Test ',
        'Pod</a><br>- <a href="https://url.example">Namespace</a><br>- ',
        '<a href="https://url.example">Device Configuration Logs/Operator</a><br>- ',
        '<a href="https://url.example">DeviceServer</a><br>- <a href=',
        '"https://url.example">DatabaseDS</a></td>\n            <td>- ',
        '<a href="https://url.example">Namespace Summary</a><br>- ',
        '<a href="https://url.example">DeviceServer</a><br>- ',
        '<a href="https://url.example">Compute Resources by Workload</a><br>- ',
        '<a href="https://url.example">Compute resources by Pod</a></td>',
        "\n        </tr>\n    </tbody>\n</table>\n\n<em>This deployment ",
        'happened at test.</em>\n\n</details>\n\n*"Final Marvin Quote."*',
    )
    expected_comment = "".join(expected_comment)

    assert result == "Comment with Links generated successfully."

    mock_gitlab_api.get_merge_request_comments.assert_awaited_once_with(
        job_urls.project_id, job_urls.mr_id
    )
    mock_comment_executor.action.assert_awaited_once_with(
        job_urls.project_id,
        job_urls.mr_id,
        expected_comment,
        "ska-devsecops-mr-links-id",
    )


@pytest.mark.asyncio
async def test_clear_dashboards_and_logs_comment():

    project_id = ("123",)
    mr_id = ("456",)
    # Mock aiohttp.ClientSession
    mock_session = AsyncMock()
    mock_session.__aenter__ = AsyncMock(return_value=mock_session)
    mock_session.__aexit__ = AsyncMock(return_value=None)

    # Mock GitLabApi
    mock_gitlab_api = AsyncMock()

    mock_comment_executor = AsyncMock()
    mock_message_generator = AsyncMock()
    mock_message_generator.get_message = AsyncMock()
    mock_message_generator.get_message.return_value = "Test Comment"

    # Patch aiohttp.ClientSession, GitLabApi, and feature_toggler.is_enabled
    with patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.aiohttp.ClientSession",
        return_value=mock_session,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.GitLabApi",
        return_value=mock_gitlab_api,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.LinksMessageGenerator",
        return_value=mock_message_generator,
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.LinksMessageGenerator.message_marker",
        new="ska-devsecops-mr-links-id",
    ), patch(
        "ska_cicd_automation.plugins.gitlab_mr.routers.mrevent.CommentExecutor",
        return_value=mock_comment_executor,
    ):
        await clear_dashboards_and_logs_comment(project_id, mr_id)

    mock_message_generator.add_pretext.assert_awaited_once_with(
        "# :page_with_curl: Dashboards and Logs"
    )
    mock_message_generator.add_posttext.assert_awaited_once_with(
        (
            '*"Oh, you wanted logs and metrics? '
            "The universe and I have something in "
            'common then—we both have nothing to offer."*'
        )
    )
    mock_comment_executor.action.assert_awaited_once_with(
        project_id, mr_id, "Test Comment", "ska-devsecops-mr-links-id"
    )


scenarios("./gitlab.feature")
