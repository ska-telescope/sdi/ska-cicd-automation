Feature: Test Web Functionality

    Scenario: Check Server
        Given Main route
        Then  Return code 200

    Scenario: Check Test Coverage is present
        Given a mock gitlab API with get_job_artifacts method
        And an Pipeline Hook with field tests/integration/event-pipeline.json
        And append file tests/unit/gitlab_mr/files/good_artefact.bin for get_job_artifacts method in gitlab api
        Then pipeline hook check_test_coverage should return true

    Scenario: Check Test Coverage is missing
        Given a mock gitlab API with get_job_artifacts method
        And an Pipeline Hook with field tests/integration/event-pipeline.json
        And append file tests/unit/gitlab_mr/files/bad_artefact.bin for get_job_artifacts method in gitlab api
        Then pipeline hook check_test_coverage should return false

    Scenario: Check Test Coverage ci-metrics job failed
        Given a mock gitlab API with get_job_artifacts method
        And an Pipeline Hook with field tests/unit/gitlab_mr/files/event-pipeline-failed-job.json
        And append file tests/unit/gitlab_mr/files/good_artefact.bin for get_job_artifacts method in gitlab api
        Then pipeline hook check_test_coverage should return true

    Scenario: Check an authorized Authentication with http header token
        Given Authenticate GET http request to /gitlab/mr/testAuth with Header env var name is GITLAB_HEADER and Token env var name is GITLAB_TOKEN
        Then  Return code 200

    Scenario: Check Assignees with more than 1 assignee
        Given a mock gitlab API
        Given an MR Hook with field assignees with [{"id": "1", "name": "ugur"}]
        Then check_assignees should return true

    Scenario: Check Assignees with 0 assignee
        Given a mock gitlab API
        Given an MR Hook with field assignees with []
        Then check_assignees should return false

    Scenario: Check Project Slug with invalid name
        Given a mock gitlab API
        Given an MR Hook with field project.path_with_namespace with ska-telescope/sdi/cicd-automation
        Then check_proj_slug should return false

    Scenario: Check Project Slug with valid name
        Given a mock gitlab API
        Given an MR Hook with field project.path_with_namespace with ska-telescope/sdi/ska-cicd-automation
        Then check_proj_slug should return true

    Scenario: Check skip tests with a marvin generated branch
        Given an MR Hook with field object_attributes.source_branch with marvin-quarantine-st-691
        Then mr event router should return a string starting with Skipping checks

    Scenario: Check background tasks
        Given an MR Hook with field object_attributes.source_branch with st-691
        Then mr event router should return a string starting with MR acknowledged

    Scenario: Check Docker Compose with docker compose present
        Given a mock gitlab API with get_search_results method
        And an MR Hook with field object_attributes.source_branch with docker-compose-check
        And append [{"path": "FAKEPATH", "filename": "docker-compose.yml", "startline": "42"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        Then check_docker_compose_usage should return false

    Scenario: Check Docker Compose with docker compose present only in documentation
        Given a mock gitlab API with get_search_results method
        And an MR Hook with field object_attributes.source_branch with docker-compose-check
        And append [{"path": "FAKEPATH", "filename": "docker-compose.md", "startline": "42"},{"path": "FAKEPATH.rst", "filename": "docker-compose.rst", "startline": "42"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        Then check_docker_compose_usage should return true

    Scenario: Check Docker Compose with docker compose present also in documentation
        Given a mock gitlab API with get_search_results method
        And an MR Hook with field object_attributes.source_branch with docker-compose-check
        And append [{"path": "FAKEPATH", "filename": "docker-compose.yml", "startline": "42"},{"path": "FAKEPATH", "filename": "docker-compose.rst", "startline": "42"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        Then check_docker_compose_usage should return false

    Scenario: Check Docker Compose with no docker compose
        Given a mock gitlab API with get_search_results method
        And an MR Hook with field object_attributes.source_branch with docker-compose-check
        And append [] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        Then check_docker_compose_usage should return true

    Scenario Outline: Check Documentation updates
        Given a mock gitlab API with get_mr_diff_versions method
        And with get_single_single_mr_diff_version method in gitlab api
        And append diff from <old_path> to <new_path>
        And append [{"state" : "collected", "id": "FAKEID"}] for get_mr_diff_versions method in gitlab api
        And finally create generator for get_mr_diff_versions method in gitlab api
        Then check_documentation_changes should return <result>

        Examples: README files
            | old_path   | new_path   | result |
            | README.md  | new        | true   |
            | README.rst | new        | true   |
            | readme.md  | new        | true   |
            | readme.rst | new        | true   |
            | old        | README.md  | true   |
            | old        | README.rst | true   |
            | old        | readme.md  | true   |
            | old        | readme.rst | true   |
            | read       | me.rst     | false  |
            | read       | me.md      | false  |

        Examples: Changelog files
            | old_path      | new_path      | result |
            | CHANGELOG.md  | new           | true   |
            | CHANGELOG.rst | new           | true   |
            | changelog.md  | new           | true   |
            | changelog.rst | new           | true   |
            | old           | CHANGELOG.md  | true   |
            | old           | CHANGELOG.rst | true   |
            | old           | changelog.md  | true   |
            | old           | changelog.rst | true   |
            | change        | log.rst       | false  |
            | change        | log.md        | false  |

        Examples: docs/src changes
            | old_path | new_path | result |
            | docs/src | new      | true   |
            | old      | docs/src | true   |
            | doc      | s/src    | false  |

        Examples: docs/source changes
            | old_path    | new_path    | result |
            | docs/source | new         | true   |
            | old         | docs/source | true   |

        Examples: No documentation changes
            | old_path | new_path | result |
            | old      | new      | false  |
            | new      | old      | false  |


    Scenario: Check License Information with bsd-3-clause license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with {"key" : "bsd-3-clause"} for get_project_settings_info method in gitlab api
        Then check_license_information should return true

    Scenario: Check License Information with unknown license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with {"key" : "unknown"} for get_project_settings_info method in gitlab api
        Then check_license_information should return false

    Scenario: Check License Information with no license
        Given a mock gitlab API with get_project_settings_info method
        And append key-value pair of license with None for get_project_settings_info method in gitlab api
        Then check_license_information should return false

    Scenario: Check Pipeline Jobs without any project include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"src/ska_test/test.py", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_any_project_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include python

    Scenario: Check Pipeline Jobs without python include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"src/ska_test/test.py", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_python_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include python

    Scenario: Check Pipeline Jobs without notebook include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"notebooks/example.ipynb", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_notebook_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include notebook

    Scenario: Check Pipeline Jobs without helm include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts/ska-test/Chart.yaml", "type": "blob"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_helm_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include helm

    Scenario: Check Pipeline Jobs without oci include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_oci_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include oci

    Scenario: Check Pipeline Jobs without docs include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_docs_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include docs

    Scenario: Check Pipeline Jobs without finaliser include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_finaliser_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include finaliser

    Scenario: Check Pipeline Jobs without k8s include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts/ska-test/Chart.yaml", "type": "blob"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_k8s_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include k8s

    Scenario: Check Pipeline Jobs without release include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_without_release_include.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false
        And mitigation strategy should include release

    Scenario: Check Pipeline Jobs with includes
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_pipeline_jobs_with_includes.yml for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return true

    Scenario: Check Pipeline Jobs with no include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of message with "404 File Not Found" for get_file_from_repository method in gitlab api
        Then check_pipeline_jobs should return false

    Scenario: Check Makefile without oci include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_makefile_without_oci_include.mk for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return false
        And mitigation strategy should include oci

    Scenario: Check Makefile without helm include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts/ska-test/Chart.yaml", "type": "blob"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_makefile_without_helm_include.mk for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return false
        And mitigation strategy should include helm

    Scenario: Check Makefile without python include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"src/ska_test/test.py", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_makefile_without_python_include.mk for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return false
        And mitigation strategy should include python

    Scenario: Check Makefile without raw include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_makefile_without_raw_include.mk for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return false
        And mitigation strategy should include raw

    Scenario: Check Makefile without base include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_makefile_without_base_include.mk for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return false
        And mitigation strategy should include base

    Scenario: Check Makefile with no include
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with "" for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return false

    Scenario: Check Makefile with includes
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"path":"charts", "type": "tree"}, {"path":"Dockerfile", "type": "blob"}, {"path":"pyproject.toml", "type": "blob"}, {"path":"docs", "type": "tree"}, {"path":"raw", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_makefile_with_includes.mk for get_file_from_repository method in gitlab api
        Then check_makefile_includes should return true

    Scenario: Check Read the Docs Integration with valid set up
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_repo_tree method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append [{"path":"docs", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api
        Then check_readthedocs_integration should return true

    Scenario: Check Read the Docs Integration with no subproject
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_repo_tree method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And throw exception for get_subproject_detail method in rtd api
        And append [{"path":"docs", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api
        Then check_readthedocs_integration should return false

    Scenario: Check Read the Docs Integration with no hooks
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_repo_tree method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append [{"path":"docs", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And append [] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api
        Then check_readthedocs_integration should return false

    Scenario: Check Read the Docs Integration with multiple hooks with one valid hook
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_repo_tree method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append [{"path":"docs", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"},{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd1"},{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd-2"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api
        Then check_readthedocs_integration should return true

    Scenario: Check Read the Docs Integration with multiple hooks with no valid hook
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_repo_tree method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append [{"path":"docs", "type": "tree"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd-1"},{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-nonvalid-rtd-2"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api
        Then check_readthedocs_integration should return false

    Scenario: Check Read the Docs Integration with no docs/
        Given a mock gitlab API
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with valid-rtd
        And with get_repo_tree method in gitlab api
        And with get_project_hooks method in gitlab api
        And a mock rtd API
        And with get_subproject_detail method in rtd api
        And append key-value pair of content with rtd-subproject for get_subproject_detail method in rtd api
        And append [] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And append [{"url" : "https://readthedocs.org/api/v2/webhook/ska-telescope-valid-rtd"}] for get_project_hooks method in gitlab api
        And finally create generator for get_project_hooks method in gitlab api
        Then check_readthedocs_integration should return false

    Scenario: Check MR Settings with everything as expected
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return true
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was not called

    Scenario: Check MR Settings with no reviewers
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with None for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was not called

    Scenario: Check MR Settings with force_remove_source_branch not checked
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with false for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with squash true
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with true for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with merge_method as ff
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with ff for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with resolve_outdated_diff_discussions false
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with false for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with printing_merge_request_link disabled
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with remove_source_branch_after_merge disabled
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with false for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with only_allow_merge_if_pipeline_succeeds false
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with false for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with overriding_approvers_per_merge_request disabled
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was not called

    Scenario: Check MR Settings with reset_approvals_on_push not set
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was called

    Scenario: Check MR Settings with merge_requests_author_approval set
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 1 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And add_comment method in gitlab api was not called

    Scenario: Check MR Settings with no approvals_required
        Given a mock gitlab API with get_merge_request_info method
        And with get_project_settings_info method in gitlab api
        And with get_single_single_mr_diff_version method in gitlab api
        And with get_project_merge_request_approvals_info method in gitlab api
        And with get_merge_request_approvals_info method in gitlab api
        And with get_job_token_scope method in gitlab api
        And with modify_merge_request method in gitlab api
        And with modify_project method in gitlab api
        And with modify_mr_level_mr_approval_settings method in gitlab api
        And with modify_project_level_mr_approval_settings method in gitlab api
        And with modify_job_token_access method in gitlab api
        And with add_comment method in gitlab api
        And append key-value pair of reviewers with merge for get_merge_request_info method in gitlab api
        And append key-value pair of force_remove_source_branch with true for get_merge_request_info method in gitlab api
        And append key-value pair of squash with false for get_merge_request_info method in gitlab api
        And append key-value pair of merge_method with merge for get_project_settings_info method in gitlab api
        And append key-value pair of resolve_outdated_diff_discussions with true for get_project_settings_info method in gitlab api
        And append key-value pair of printing_merge_request_link_enabled with true for get_project_settings_info method in gitlab api
        And append key-value pair of remove_source_branch_after_merge with true for get_project_settings_info method in gitlab api
        And append key-value pair of only_allow_merge_if_pipeline_succeeds with true for get_project_settings_info method in gitlab api
        And append key-value pair of disable_overriding_approvers_per_merge_request with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of reset_approvals_on_push with true for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of merge_requests_author_approval with false for get_project_merge_request_approvals_info method in gitlab api
        And append key-value pair of approvals_required with 0 for get_merge_request_approvals_info method in gitlab api
        And append key-value pair of request_access_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of security_and_compliance_access_level with private for get_project_settings_info method in gitlab api
        And append key-value pair of issues_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_cancel_pending_pipelines with enabled for get_project_settings_info method in gitlab api
        And append key-value pair of allow_merge_on_skipped_pipeline with false for get_project_settings_info method in gitlab api
        And append key-value pair of auto_devops_enabled with false for get_project_settings_info method in gitlab api
        And append key-value pair of build_git_strategy with fetch for get_project_settings_info method in gitlab api
        And append key-value pair of ci_config_path with null for get_project_settings_info method in gitlab api
        And append key-value pair of keep_latest_artifact with true for get_project_settings_info method in gitlab api
        And append key-value pair of squash_option with never for get_project_settings_info method in gitlab api
        And append key-value pair of inbound_enabled with true for get_job_token_scope method in gitlab api
        And append key-value pair of only_allow_merge_if_all_status_checks_passed with true for get_project_settings_info method in gitlab api
        Then check_settings should return false
        And modify_merge_request method in gitlab api was not called
        And modify_project method in gitlab api was not called
        And modify_mr_level_mr_approval_settings method in gitlab api was not called
        And modify_project_level_mr_approval_settings method in gitlab api was not called
        And modify_job_token_access method in gitlab api was not called
        And add_comment method in gitlab api was not called

    Scenario: Check repository with CODEOWNERS file
        Given a mock gitlab API
        And with get_file_from_repository method in gitlab api
        And append key-value pair of content with true for get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with st-856
        Then check_codeowners_exist should return true

    Scenario: Check repository without CODEOWNERS file
        Given a mock gitlab API
        And with get_file_from_repository method in gitlab api
        And append key-value pair of message with 404 file not found for get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with st-856
        Then check_codeowners_exist should return false

    Scenario Outline: Check Directory Compliance
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append <tree_items> with <tree_types> for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        And with get_file_from_repository method in gitlab api
        Then check_directory_compliance should return <result>

        Examples: Valid Python Repos
            | tree_items                                                | tree_types       | result |
            | pyproject.toml,src/ska_unit_tests/main.py                 | blob, blob       | true   |
            | setup.py, requirements.txt, src/ska_unit_tests/main.py    | blob, blob, blob | true   |
            | pyproject.toml,src/ska_unit_tests_v2/main.py              | blob, blob       | true   |
            | setup.py, requirements.txt, src/ska_unit_tests_v2/main.py | blob, blob, blob | true   |

        Examples: Invalid Python Repos
            | tree_items                                         | tree_types       | result |
            | pyproject.toml,src/unit_tests/main.py              | blob, blob       | false  |
            | setup.py, requirements.txt ,src/unit_tests/main.py | blob, blob, blob | false  |

        Examples: Non-Python Repos
            | tree_items                                 | tree_types | result |
            # note: we don't actually check if there are python files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | pyproject.toml,app/main.py                 | blob, blob | true   |
            | requirement.txt,src/ska_unit_tests/main.py | blob, blob | true   |
            | a,b                                        | blob, blob | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | pyproject.toml,src/ska_unit_tests/main.py  | tree, blob | true   |
            | setup.py,src/ska_unit_tests/main.py        | blob, tree | true   |

        Examples: Valid CPP Repos
            | tree_items                               | tree_types | result |
            | conanfile.py,src/ska_unit_tests/main.cpp | blob, blob | true   |

        Examples: Invalid CPP Repos
            | tree_items                           | tree_types | result |
            | conanfile.py,src/unit_tests/main.cpp | blob, blob | false  |

        Examples: Non-CPP Repos
            | tree_items                               | tree_types | result |
            # note: we don't actually check if there are files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | conanfile.py,app/main.cpp                | blob, blob | true   |
            | cmake.txt,src/ska_unit_tests/main.cpp    | blob, blob | true   |
            | a,b                                      | blob, blob | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | conanfile.py,src/ska_unit_tests/main.cpp | tree, blob | true   |
            | conanfile.py,src/ska_unit_tests/main.cpp | blob, tree | true   |

        Examples: Valid HELM Repos
            | tree_items                        | tree_types | result |
            | charts/ska-unit-tests/Chart.yaml  | blob       | true   |
            | charts/ska-unit-tests/Chart.yml   | blob       | true   |
            | charts/ska-unit-tests-1/Chart.yml | blob       | true   |
            | charts/ska-tests/Chart.yml        | blob       | true   |

        Examples: Invalid HELM Repos
            | tree_items              | tree_types | result |
            | charts/chart/Chart.yaml | blob       | false  |
            | charts/chart/Chart.yml  | blob       | false  |

        Examples: Non-HELM Repos
            | tree_items                       | tree_types | result |
            # note: we don't actually check if there are files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | charts/chart/Charts.yml          | blob       | true   |
            | chart/chart-a/Charts.yml         | blob       | true   |
            | chart/chart-a/Chart.yml          | blob       | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | charts/ska-unit-tests/Chart.yaml | tree       | true   |

        Examples: Valid OCI Repos
            | tree_items                              | tree_types | result |
            | Dockerfile                              | blob       | true   |
            | images/ska-unit-tests-alpine/Dockerfile | blob       | true   |
            | images/ska-unit-tests/Dockerfile        | blob       | true   |

        Examples: Invalid OCI Repos
            | tree_items               | tree_types | result |
            | images/docker/Dockerfile | blob       | false  |

        Examples: Non-OCI Repos
            | tree_items        | tree_types | result |
            # note: we don't actually check if there are files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | docker/Dockerfile | blob       | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | Dockerfile        | tree       | true   |

        Examples: Valid RAW Repos
            | tree_items                      | tree_types | result |
            | raw, raw/ska-unit-tests-a/a.txt | tree, blob | true   |
            | raw, raw/ska-unit-tests/a.txt   | tree, blob | true   |

        Examples: Invalid RAW Repos
            | tree_items                | tree_types | result |
            | raw, raw/unit-tests/a.txt | tree, blob | false  |
            | raw, raw/a.txt            | tree, blob | false  |

        Examples: Non-RAW Repos
            | tree_items    | tree_types | result |
            # note: we don't actually check if there are files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | raw_artefacts | tree       | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | raws          | tree       | true   |

        Examples: Valid ANSIBLE Repos
            | tree_items      | tree_types | result |
            | galaxy.yaml     | blob       | true   |
            | galaxy.yml      | blob       | true   |
            | role/galaxy.yml | blob       | true   |

        Examples: Invalid ANSIBLE Repos
            | tree_items                                 | tree_types | result |
            | src/galaxy.yml                             | blob       | false  |
            | scripts/galaxy.yml                         | blob       | false  |
            | resources/galaxy.yml                       | blob       | false  |
            | charts/galaxy.yml                          | blob       | false  |
            | images/galaxy.yml                          | blob       | false  |
            | docs/galaxy.yml                            | blob       | false  |
            | tests/galaxy.yml                           | blob       | false  |
            | ansible-collections/collections/galaxy.yml | blob       | false  |

        Examples: Non-ANSIBLE Repos
            | tree_items          | tree_types | result |
            # note: we don't actually check if there are files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | ansible.galaxy.yaml | blob       | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | galaxy.yaml         | tree       | true   |
            | galaxy.yml          | tree       | true   |

        Examples: Valid DOCS Repos
            | tree_items                                                     | tree_types            | result |
            | docs, docs/src/conf.py, docs/src/index.rst, docs/src/README.md | tree,blob, blob, blob | true   |

        Examples: Invalid DOCS Repos
            | tree_items                             | tree_types      | result |
            | docs, docs/conf.py                     | tree,blob       | false  |
            | docs, docs/src/conf.py                 | tree,blob       | false  |
            | docs, docs/conf.py, docs/src/README.md | tree,blob, blob | false  |

        Examples: Non-DOCS Repos
            | tree_items   | tree_types | result |
            # note: we don't actually check if there are files anywhere else other than src/ when deciding repo type (hence the below test returns true)
            | rtd          | tree       | true   |
            # note: if an expected file is a folder (instead of a file), it again does not fail the check since it's not considered a repo
            | docs/conf.py | tree       | true   |

        Examples: Valid NOTEBOOKS Repos
            | tree_items                            | tree_types    | result |
            | notebooks/example.ipynb               | blob          | true   |
            # note: if an expected file is in a folder we don't check, it again does not fail the check since it's not considered a repo
            | src/example.ipynb                     | tree,blob     | true   |

        Examples: Invalid NOTEBOOKS Repos
            | tree_items                        | tree_types    | result |
            | notebooks, src/example.ipynb      | tree,blob     | false  |
            | notebooks, example.ipynb          | tree,blob     | false  |
            | notebooks                         | tree,blob     | false  |
            | example.ipynb                     | blob          | false  |

    Scenario: Check Charts with deprecated dependencies in Nexus
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of version with 2.0.0 for get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_chart_deprecated_dependencies_nexus.yml for get_file_from_repository method in gitlab api
        Then check_chart_dependencies should return false

    Scenario: Check Charts with deprecated dependencies in Nexus using dev tag
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of version with 2.0.0 for get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_chart_deprecated_dependencies_nexus_dev_tag.yml for get_file_from_repository method in gitlab api
        Then check_chart_dependencies should return false

    Scenario: Check Charts without deprecated dependencies in Nexus
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of version with 0.0.0 for get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_chart_deprecated_dependencies_nexus.yml for get_file_from_repository method in gitlab api
        Then check_chart_dependencies should return true

    Scenario: Check multiple Charts without deprecated dependencies in Nexus
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}, {"name":"Chart.yaml", "path":"charts/ska-test2/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of version with 0.0.0 for get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_chart_deprecated_dependencies_nexus.yml for get_file_from_repository method in gitlab api
        Then check_chart_dependencies should return true

    Scenario: Check multiple Charts with deprecated dependencies in Nexus
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}, {"name":"Chart.yaml", "path":"charts/ska-test2/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of version with 2.0.0 for get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_chart_deprecated_dependencies_nexus.yml for get_file_from_repository method in gitlab api
        Then check_chart_dependencies should return false

    Scenario: Check multiple Charts without deprecated dependencies in Nexus
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"message":"404 Tree Not Found"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of version with 2.0.0 for get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_chart_deprecated_dependencies_nexus.yml for get_file_from_repository method in gitlab api
        Then check_chart_dependencies should return true

    Scenario: Check Python Repo using Poetry
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"pyproject.toml", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return true

    Scenario: Check Python Repo not using Poetry
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return false

    Scenario: Check Non-Python Repo using Poetry
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"pyproject.toml", "path":"", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return true

    Scenario: Check Python Repo using requirements.txt
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"requirements.txt", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return false

    Scenario: Check Non-Python Repo using requirements.txt
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"requirements.txt", "path":"", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return true

    Scenario: Check Python Repo using setup.py
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"setup.py", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return false

    Scenario: Check Non-Python Repo using setup.py
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"setup.py", "path":"", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return true

    Scenario: Check Python Repo using Poetry and setup.py
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"pyproject.toml", "path":"", "type": "blob"},{"name":"setup.py", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return false

    Scenario: Check Python Repo using Poetry and requirements.txt
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"pyproject.toml", "path":"", "type": "blob"},{"name":"requirements.txt", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return false

    Scenario: Check Python Repo using setup.py and requirements.txt
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"setup.py", "path":"", "type": "blob"},{"name":"requirements.txt", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return false

    Scenario: Check Python Repo using Poetry and .binder/requirements.txt
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"pyproject.toml", "path":"", "type": "blob"},{"name":".binder/requirements.txt", "path":"", "type": "blob"},{"name":"main.py", "path":"src/ska_unit_tests/main.py", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And an MR Hook with field object_attributes.source_branch with pipeline-check
        And an MR Hook with field project.path_with_namespace with "ska-telescope/ska-unit-tests"
        And an MR Hook with field project.ci_config_path with ""
        Then check_poetry_usage should return true

    Scenario: Check Charts with deprecated dependencies
        Given a mock gitlab API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_deprecated_chart.yml for get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        Then check_chart_dependencies should return false

    Scenario: Check Charts with non deprecated dependencies
        Given a mock gitlab API
        And a mock nexus API
        And with get_repo_tree method in gitlab api
        And append [{"name":"Chart.yaml", "path":"charts/ska-test/Chart.yaml", "type": "blob"}] for get_repo_tree method in gitlab api
        And finally create generator for get_repo_tree method in gitlab api
        And with get_file_from_repository method in gitlab api
        And a mock nexus API
        And with get_latest_chart_version method in nexus api
        And append key-value pair of content with file tests/unit/gitlab_mr/files/check_not_deprecated_chart.yml for get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        Then check_chart_dependencies should return true

    Scenario: Check Make submodule with updated ref
        Given a mock gitlab API
        And with get_commits method in gitlab api
        And append [{"created_at":"2024-02-07T10:00:00.000+00:00"}] for get_commits method in gitlab api
        And finally create generator for get_commits method in gitlab api
        And with get_file_from_repository method in gitlab api
        And append {"blob_id":"99db9fc9c04e39ac229912b891232f89519d6185"} for get_file_from_repository method in gitlab api
        And finally create generator for get_file_from_repository method in gitlab api
        And with get_commit method in gitlab api
        And append {"created_at":"2024-02-07T10:00:00.000+00:00"} for get_commit method in gitlab api
        And finally create generator for get_commit method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        Then check_make_submodule_version should return true

    Scenario: Check Make submodule with oudated ref
        Given a mock gitlab API
        And with get_commits method in gitlab api
        And append [{"created_at":"2024-02-07T10:00:00.000+00:00"}] for get_commits method in gitlab api
        And finally create generator for get_commits method in gitlab api
        And with get_file_from_repository method in gitlab api
        And append key-value pair of blob_id with 99db9fc9c04e39ac229912b891232f89519d6185 for get_file_from_repository method in gitlab api
        And with get_commit method in gitlab api
        And append key-value pair of created_at with 2024-02-07T09:00:00.000+00:00 for get_commit method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        Then check_make_submodule_version should return false

    Scenario: Check Make submodule with ref ahead of master
        Given a mock gitlab API
        And with get_commits method in gitlab api
        And append [{"created_at":"2024-02-07T10:00:00.000+00:00"}] for get_commits method in gitlab api
        And finally create generator for get_commits method in gitlab api
        And with get_file_from_repository method in gitlab api
        And append key-value pair of blob_id with 99db9fc9c04e39ac229912b891232f89519d6185 for get_file_from_repository method in gitlab api
        And with get_commit method in gitlab api
        And append key-value pair of created_at with 2024-02-07T11:00:00.000+00:00 for get_commit method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        Then check_make_submodule_version should return true

    Scenario: Check Make submodule with no make submodule
        Given a mock gitlab API
        And with get_commits method in gitlab api
        And append [{"created_at":"2024-02-07T10:00:00.000+00:00"}] for get_commits method in gitlab api
        And finally create generator for get_commits method in gitlab api
        And with get_file_from_repository method in gitlab api
        And append {} for get_file_from_repository method in gitlab api
        And an MR Hook with field object_attributes.source_branch with unit-tests
        Then check_make_submodule_version should return true

    Scenario: Check Branch name with valid jira ticket with numbers
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "AT2-679 Valid message"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        And an MR Hook with field object_attributes.source_branch with at2-679
        And an MR Hook with field object_attributes.title with AT2-679
        And a mock jira API with get_issue method
        And append object with attribute key with at2-679 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check Branch name with invalid jira ticket
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "ST-600 Valid message"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        Given an MR Hook with field object_attributes.source_branch with st-9999
        And an MR Hook with field object_attributes.title with ST-9999
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_jira_id should return false

    Scenario: Check Branch name with valid jira ticket
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "ST-600 Valid message"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with ST-600
        And a mock jira API with get_issue method
        And append object with attribute key with st-600 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check Branch name without jira ticket
        Given a mock gitlab API with get_merge_request_commits method
        And append [{"short_id": "FAKEID", "message": "ST-600 Valid message"},{"short_id": "FAKEID2", "message": "ST-600 Valid message 2"}] for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        Given an MR Hook with field object_attributes.source_branch with no-jira
        And an MR Hook with field object_attributes.title with no-jira
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_jira_id should return false

    Scenario Outline: Check Commit message with jira tickets
        Given a mock gitlab API with get_merge_request_commits method
        And append commits <commit_message> for get_merge_request_commits method in gitlab api
        And finally create generator for get_merge_request_commits method in gitlab api
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with ST-600
        And a mock jira API with get_issue method
        And append object with attribute key with st-600 for get_issue method in jira api
        Then check_jira_id should return <result>

        Examples: Valid Commit Messages
            | commit_message                                                 | result   |
            | "ABC-123 Fix login bug"                                        | true     |
            | "[DEF-456] Implement new feature"                              | true     |
            | "GHI-789: Refactor database schema"                            | true     |
            | "ABC-123 DEF-456: Multiple tickets"                            | true     |
            | "ABC-123:"                                                     | true     |
            | "[ABC-123]"                                                    | true     |
            | "(XYZ-789)"                                                    | true     |
            | "ABC-123 DEF-456 GHI-789: Multiple tickets with message"       | true     |
            | "ABC-123:"                                                     | true     |
            | "PROJ-1 Initial commit"                                        | true     |
            | "ABC-123 DEF-456    Multiple spaces after tickets"             | true     |
            | "Merge branch 'feature/ABC-123' into 'main'"                   | true     |
            | "Merge pull request #123 from user/feature"                    | true     |
            | "Merge remote-tracking branch 'origin/feature' into develop"   | true     |
            | "Revert \"ABC-123: Add new feature\""                          | true     |
            | "   ABC-123: Leading spaces"                                   | true     |
            | "Merge: ABC-123 Fix merge issues"                              | true     |
            | "Merge branch 'hotfix/ABC-123' into 'release'"                 | true     |
            | "Revert commit 'ABC-123': Reverting a change"                  | true     |
            | "Revert ABC-123: Fix login bug"                                | true     |
            | "Merge feature-ABC-123 into main"                              | true     |
            | "Merge branch 'release/1.0.0'"                                 | true     |
            | "Merge pull request #ABC-123 from branch"                      | true     |
            | "Merge pull request #123 from user/feature-ABC-123"            | true     |
            | "Revert \"Merge pull request #123 from user/feature-ABC-123\"" | true     |
            | "Merge 'release/ABC-123' into 'main'"                          | true     |
            | "ABC-123! Special character at end" | true |

        Examples: Invalid Commit Messages
            | commit_message                           | result  |
            | "(JKL-012) Update documentation"         | false   |
            | "abc-123 Lowercase ticket"               | false   |
            | "ABC-DEF No numbers in ticket"           | false   |
            | "Update README"                          | false   |
            | "fix: Quick bugfix"                      | false   |
            | "123-ABC Incorrect order"                | false   |
            | "ABC-123ABC Invalid format after hyphen" | false   |
            | "ABC_123 Underscore instead of hyphen"   | false   |
            | "ABC- Fix missing numbers"               | false   |
            | "[] Empty brackets"                      | false   |
            | "() Empty parentheses"                   | false   |
            | "abc_123 Invalid separator"              | false   |

    Scenario: Check MR Title with valid JIRA Id
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with ST-600
        And a mock jira API with get_issue method
        And append object with attribute key with ST-600 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check MR Title with valid JIRA Id with brackets
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with [ST-600]
        And a mock jira API with get_issue method
        And append object with attribute key with ST-600 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check MR Title with Draft before ID
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with Draft: ST-600
        And a mock jira API with get_issue method
        And append object with attribute key with ST-600 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check MR Title with Draft before ID and brackets
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with Draft: [ST-600]
        And a mock jira API with get_issue method
        And append object with attribute key with ST-600 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check MR Title with valid JIRA Id with numbers
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with at2-679
        And an MR Hook with field object_attributes.title with AT2-679
        And a mock jira API with get_issue method
        And append object with attribute key with AT2-679 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check MR Title with invalid JIRA Id
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-9999
        And an MR Hook with field object_attributes.title with ST-9999
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_jira_id should return false

    Scenario: Check MR Title with valid JIRA Id with Draft Status
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with Draft: ST-600:
        And a mock jira API with get_issue method
        And append object with attribute key with ST-600 for get_issue method in jira api
        Then check_jira_id should return true

    Scenario: Check MR Title with no JIRA Id
        Given a mock gitlab API with get_merge_request_commits method
        Given an MR Hook with field object_attributes.source_branch with st-600
        And an MR Hook with field object_attributes.title with no-jira-id
        And a mock jira API with get_issue method
        And throw exception for get_issue method in jira api
        Then check_jira_id should return false

    Scenario: Check repository with CHANGELOG.md file
        Given a mock gitlab API
        And with get_search_results method in gitlab api
        And append [{"filename":"CHANGELOG.md"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And an MR Hook with field object_attributes.source_branch with st-856
        Then check_changelog_exist should return true

    Scenario: Check repository with CHANGELOG.rst file
        Given a mock gitlab API
        And with get_search_results method in gitlab api
        And append [{"filename":"docs/CHANGELOG.rst"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And an MR Hook with field object_attributes.source_branch with st-856
        Then check_changelog_exist should return true

    Scenario: Check repository with CHANGELOG.rst and CHANGELOG.md file
        Given a mock gitlab API
        And with get_search_results method in gitlab api
        And append [{"filename":"CHANGELOG.md"}, {"filename":"docs/CHANGELOG.rst"}] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And an MR Hook with field object_attributes.source_branch with st-856
        Then check_changelog_exist should return true

    Scenario: Check repository without CHANGELOG files
        Given a mock gitlab API
        And with get_search_results method in gitlab api
        And append [] for get_search_results method in gitlab api
        And finally create generator for get_search_results method in gitlab api
        And an MR Hook with field object_attributes.source_branch with st-856
        Then check_changelog_exist should return false