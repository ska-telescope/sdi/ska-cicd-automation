from unittest.mock import AsyncMock, patch

import pytest

from src.ska_cicd_automation.plugins.gitlab_mr.models.comment_executor import (
    CommentExecutor,
    GitLabApi,
)


@pytest.mark.asyncio
@patch(
    "src.ska_cicd_automation.plugins.gitlab_mr.models.comment_executor.logging"
)
async def test_comment_found(mock_logger):
    api_mock = AsyncMock(GitLabApi)
    logger_name = "test_logger"
    executor = CommentExecutor(api_mock, logger_name)

    api_mock.get_merge_request_comments.return_value.__aiter__.return_value = [
        {"id": 1, "body": "message_marker"}
    ]

    # Variables for the method
    proj_id = "123"
    mr_id = "456"
    comment_text = "Updated comment"
    message_marker = "message_marker"

    _ = await executor.action(proj_id, mr_id, comment_text, message_marker)

    api_mock.modify_comment.assert_awaited_once_with(
        proj_id, mr_id, 1, comment_text
    )
    api_mock.add_comment.assert_not_awaited()


@pytest.mark.asyncio
@patch(
    "src.ska_cicd_automation.plugins.gitlab_mr.models.comment_executor.logging"
)
async def test_comment_not_found(mock_logger):
    api_mock = AsyncMock(GitLabApi)
    logger_name = "test_logger"
    executor = CommentExecutor(api_mock, logger_name)

    api_mock.get_merge_request_comments.return_value.__aiter__.return_value = [
        {"id": 1, "body": ""}
    ]

    # Variables for the method
    proj_id = "123"
    mr_id = "456"
    comment_text = "Updated comment"
    message_marker = "message_marker"

    _ = await executor.action(proj_id, mr_id, comment_text, message_marker)

    api_mock.add_comment.assert_awaited_once_with(proj_id, mr_id, comment_text)
    api_mock.modify_comment.assert_not_awaited()
