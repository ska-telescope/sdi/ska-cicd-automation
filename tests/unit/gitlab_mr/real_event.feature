Feature: Trigger Actual Test

    @manual
    Scenario: Trigger an actual event against GitLab API for a real project
        Given Actual post event from json file tests/unit/gitlab_mr/files/event.json
        Then Return code 200
