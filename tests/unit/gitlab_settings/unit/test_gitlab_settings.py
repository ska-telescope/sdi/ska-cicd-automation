# flake8: noqa E501
# from inspect import signature
import logging
from unittest.mock import AsyncMock, patch

import pytest
from authlib.integrations.base_client.errors import OAuthError
from fastapi.testclient import TestClient
from gidgetlab.exceptions import GitLabException
from pytest_bdd import given, parsers, scenarios, then
from starlette.responses import Response

from ska_cicd_automation.main import app
from ska_cicd_automation.plugins.gitlab_settings.routers.fix_settings import (
    router,
)

# Used to Print
logger = logging.getLogger(__name__)
# Creating a Testclient with main app
client = TestClient(app)
# Creating a Testclient with router members
client_router = TestClient(router)


@given(parsers.parse("{isValid} token"))
def set_token(isValid: str):
    # ST-1030: Dynamic user access token
    if isValid == "valid":
        pytest.token = 1234
    else:
        pytest.token = None


@given(parsers.parse("user with ID: {user_id}"))
def set_user_id(user_id: str):
    # ST-1030: Dynamic user ID
    # Invalid User. User ID 0
    # throw Exception. User ID 999
    if int(user_id) != 0:
        pytest.user = {
            "id": int(user_id),
        }
    else:
        pytest.user = None


@given(parsers.parse("{isGood} modify setting request"))
def set_setting_request(isGood: str):
    # ST-1030: Dynamic Setting Request
    pytest.goodSettingRequest = isGood


# ST-1030: Oauth Redirect Test
@given(
    parsers.parse(
        "oauth http request to {target} with these params: <proj_id>, <mr_id> and <setting>"
    )  # NOQA: E501
)
def check_oauth_request(
    target: str,
    proj_id: str,
    mr_id: str,
    setting: str,
):

    params = {
        "proj_id": None if proj_id == "" else proj_id,
        "mr_id": None if mr_id == "" else mr_id,
        "setting": None if setting == "" else setting,
    }

    with patch(
        "ska_cicd_automation.plugins.gitlab_settings.routers.fix_settings.oauth.gitlab.authorize_redirect",  # NOQA: E501
    ) as request:

        # ST-1030: Mock Oauth Redirect Request
        request.return_value = Response(
            status_code=302,
            headers={"location": "https://gitlab-test.com/oauth/authorize"},
        )

        # ST-1030: allow_redirects must be set to False or the TestClient
        #  will return a 404 response code upon receiving an external URL
        #  for redirection (happens during OAuth testing)
        response = client.get(target, params=params, allow_redirects=False)

        pytest.redirect = response.headers.get("location")
        pytest.confirm = response.status_code


@given(
    parsers.parse(
        "http request to {target} with these params: <proj_id>, <mr_id> and <setting>"
    )
)  # NOQA: E501
def check_http_request(
    target: str,
    proj_id: str,
    mr_id: str,
    setting: str,
):

    proj_id = None if proj_id == "" else proj_id
    mr_id = None if mr_id == "" else mr_id
    setting = None if setting == "" else setting

    with patch(
        "ska_cicd_automation.plugins.gitlab_settings.routers.fix_settings.GitLabApi"
    ) as gitlab, patch(
        "ska_cicd_automation.plugins.gitlab_settings.routers.fix_settings.FixCommentGenerator"
    ) as commentor, patch(
        "ska_cicd_automation.plugins.gitlab_settings.routers.fix_settings.oauth.gitlab.authorize_access_token",  # NOQA: E501
    ) as gut:

        # ST-1030: mock get user token
        if pytest.token:
            gut.return_value = {"access_token": 1234}
        else:
            gut.side_effect = OAuthError("Error message")

        # ST-1030: Dynamic Gitlab API functions

        # ST-1030: Project Settings
        # ID 0 - Cannot Retrive Web Url
        if proj_id and int(proj_id) != 0:
            get_project_settings_info = AsyncMock(
                return_value={
                    "web_url": "https://gitlab.com/ska-telescope/test-repo",
                }
            )
        else:
            get_project_settings_info = AsyncMock(return_value={})

        # ST-1030: Merge Request Settings
        # MR ID 0 - None response
        # PR ID 1 - None response
        if (proj_id and int(proj_id) == 999) or (mr_id and int(mr_id) == 999):
            get_merge_request_info = AsyncMock(
                side_effect=GitLabException("Bad MR Request")
            )
        elif (proj_id and int(proj_id) != 1) and (mr_id and int(mr_id) != 0):
            get_merge_request_info = AsyncMock(
                return_value={
                    "assignees": [
                        {"id": 1},
                        {"id": 3},
                    ],
                }
            )
        else:
            get_merge_request_info = AsyncMock(return_value=None)

        # ST-1030: Request Settings
        if pytest.goodSettingRequest == "good":
            setting_response = {}
        else:
            setting_response = {"error": "Bad Setting Request"}

        # ST-1030: Request Get User
        if pytest.user and pytest.user.get("id") != 999:
            get_current_user = AsyncMock(return_value=pytest.user)
        elif not pytest.user:
            get_current_user = AsyncMock(return_value=None)
        else:
            get_current_user = AsyncMock(
                side_effect=GitLabException("Bad User Request")
            )

        # ST-1030: mock Gitlab API Class
        gitlab.return_value = AsyncMock(
            get_current_user=get_current_user,
            get_project_settings_info=get_project_settings_info,
            get_merge_request_info=get_merge_request_info,
            modify_merge_request=AsyncMock(
                return_value=setting_response,
            ),
            modify_project=AsyncMock(
                return_value=setting_response,
            ),
            get_project_merge_request_approvals_info=AsyncMock(
                return_value={
                    "disable_overriding_approvers_per_merge_request": True
                },
            ),
            modify_project_level_mr_approval_settings=AsyncMock(
                return_value=setting_response,
            ),
            modify_mr_level_mr_approval_settings=AsyncMock(
                return_value=setting_response,
            ),
            modify_job_token_access=AsyncMock(
                return_value=setting_response,
            ),
        )

        # ST-1030: Mock Gitlab Commentor
        commentor.return_value = AsyncMock(
            add_entry=AsyncMock(),
            execute=AsyncMock(),
        )

        params = {"proj_id": proj_id, "mr_id": mr_id, "setting": setting}

        # ST-1030: allow_redirects must be set to False or the TestClient
        #  will return a 404 response code upon receiving an external URL
        #  for redirection (happens during OAuth testing)
        response = client.get(target, params=params, allow_redirects=False)

        # ST-1030: Check if mock functions were called
        if setting and proj_id and mr_id:
            gut.assert_awaited_once()

            if pytest.token:
                gitlab.assert_called_once()
            else:
                gitlab.assert_not_called()

            # BUG: nested AsyncMock functions are not saving the call history
            # gitlab.get_current_user.assert_called_once()

        pytest.redirect = response.headers.get("location")
        pytest.confirm = response.status_code


@then(parsers.parse("return code <http_code>"))
def check_message(http_code):
    assert int(http_code) == pytest.confirm


@then(parsers.parse("redirects to <url>"))
def check_redirect(url):
    if url == "":
        url = None
    assert url == pytest.redirect


scenarios("./gitlab_settings.feature")
