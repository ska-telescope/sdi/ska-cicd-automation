Feature: Manual Gitlab Settings

    Scenario: Check for OAuth redirection
        Given oauth http request to gitlab/settings/manual_fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url                                     |
            |   9876    |   1212    |   force_remove_source_branch                      | 302       | https://gitlab-test.com/oauth/authorize |


    Scenario: Invalid HTTP Parameters
        Given valid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1111    |                                                   | 400       |       |
            |   9876    |           |   force_remove_source_branch                      | 422       |       |
            |           |   1111    |   force_remove_source_branch                      | 422       |       |


    Scenario: Invalid access token
        Given invalid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1111    | force_remove_source_branch                        | 401       |       |
    
    Scenario: Invalid User
        Given valid token
        And user with ID: 0
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1111    | force_remove_source_branch                        | 401       |       |    
            
    Scenario: Bad Get User Request
        Given valid token
        And user with ID: 999
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1111    | force_remove_source_branch                        | 401       |       |    
            
    Scenario: Bad Get Merge Request Info Request
        Given valid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   999     |   999     | force_remove_source_branch                        | 401       |       |
 
    
    Scenario: Valid User - Not a MR assignee
        Given valid token
        And user with ID: 2
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1212    | force_remove_source_branch                        | 401       |       |    
            
    Scenario: Bad Merge Request Info
        Given valid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   0       | force_remove_source_branch                        | 401       |       |
            |   1       |   1212    | force_remove_source_branch                        | 401       |       |

    Scenario: Bad Setting
        Given valid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>
    
        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1212    | force                                             | 400       |       |
            |   0       |   1212    | force                                             | 400       |       |

    
    Scenario: Checking Redirect URL
        Given valid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

        Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url                                                            |
            |   9876    |   1212    |   force_remove_source_branch                      | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1111    |   force_remove_source_branch                      | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1111 |
            |   0       |   1111    |   force_remove_source_branch                      | 200       |                                                                |
            
    Scenario: Checking All Available Settings - Good Modify Setting Request
        Given valid token
        And user with ID: 1
        And good modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

            Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url                                                            |
            |   9876    |   1212    |   force_remove_source_branch                      | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   squash                                          | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   squash_option                                   | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   merge_method                                    | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   resolve_outdated_diff_discussions               | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   printing_merge_request_link_enabled             | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   remove_source_branch_after_merge                | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   only_allow_merge_if_pipeline_succeeds           | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   approvals_required                              | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   disable_overriding_approvers_per_merge_request  | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   reset_approvals_on_push                         | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   merge_requests_author_approval                  | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |    
            |   9876    |   1212    |   request_access_enabled                          | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   allow_merge_on_skipped_pipeline                 | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   auto_devops_enabled                             | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   build_git_strategy                              | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   ci_config_path                                  | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   keep_latest_artifact                            | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   job_token_scope                                 | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   security_and_compliance_access_level            | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   issues_access_level                             | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   auto_cancel_pending_pipelines                   | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |
            |   9876    |   1212    |   only_allow_merge_if_all_status_checks_passed    | 302       | https://gitlab.com/ska-telescope/test-repo/merge_requests/1212 |            
    
    Scenario: Checking All Available Settings - Bad Modify Setting Request
        Given valid token
        And user with ID: 1
        And bad modify setting request
        And http request to gitlab/settings/fix with these params: <proj_id>, <mr_id> and <setting>
        Then return code <http_code>
        And redirects to <url>

            Examples: Parameters & Returns
            | proj_id   | mr_id     | setting                                           | http_code | url   |
            |   9876    |   1212    |   force_remove_source_branch                      | 400       |       |
            |   9876    |   1212    |   squash                                          | 400       |       |
            |   9876    |   1212    |   squash_option                                   | 400       |       |            
            |   9876    |   1212    |   merge_method                                    | 400       |       |
            |   9876    |   1212    |   resolve_outdated_diff_discussions               | 400       |       |
            |   9876    |   1212    |   printing_merge_request_link_enabled             | 400       |       |
            |   9876    |   1212    |   remove_source_branch_after_merge                | 400       |       |
            |   9876    |   1212    |   only_allow_merge_if_pipeline_succeeds           | 400       |       |
            |   9876    |   1212    |   approvals_required                              | 400       |       |
            |   9876    |   1212    |   disable_overriding_approvers_per_merge_request  | 400       |       |
            |   9876    |   1212    |   reset_approvals_on_push                         | 400       |       |
            |   9876    |   1212    |   merge_requests_author_approval                  | 400       |       |
            |   9876    |   1212    |   request_access_enabled                          | 400       |       |      
            |   9876    |   1212    |   allow_merge_on_skipped_pipeline                 | 400       |       |
            |   9876    |   1212    |   auto_devops_enabled                             | 400       |       |
            |   9876    |   1212    |   build_git_strategy                              | 400       |       |
            |   9876    |   1212    |   ci_config_path                                  | 400       |       |
            |   9876    |   1212    |   keep_latest_artifact                            | 400       |       |
            |   9876    |   1212    |   job_token_scope                                 | 400       |       |
            |   9876    |   1212    |   security_and_compliance_access_level            | 400       |       |
            |   9876    |   1212    |   issues_access_level                             | 400       |       |
            |   9876    |   1212    |   auto_cancel_pending_pipelines                   | 400       |       |
            |   9876    |   1212    |   only_allow_merge_if_all_status_checks_passed    | 400       |       |
            