# flake8: noqa E501
from unittest.mock import patch

import pytest
from fastapi import HTTPException
from fastapi.exceptions import RequestValidationError
from fastapi.testclient import TestClient

from ska_cicd_automation.plugins.metrics_server.routers.metrics_server import (
    router,
)

client = TestClient(router)


@pytest.fixture
def mock_feature_toggler():
    with patch(
        "ska_cicd_automation.plugins.metrics_server.routers.metrics_server.feature_toggler"
    ) as mock:
        yield mock


@pytest.fixture
def mock_metrics_server():
    with patch(
        "ska_cicd_automation.plugins.metrics_server.routers.metrics_server.metrics_server"
    ) as mock:
        yield mock


def test_metrics_enabled(mock_feature_toggler, mock_metrics_server):
    mock_feature_toggler.is_enabled.return_value = True
    mock_metrics_server.metrics = "some_metrics"

    response = client.get("/metrics")
    assert response.status_code == 200
    assert response.content == b"some_metrics"


def test_metrics_disabled(mock_feature_toggler):
    mock_feature_toggler.is_enabled.return_value = False

    response = client.get("/metrics")
    assert response.status_code == 404
    assert response.json() == {"message": "METRICS EXPORTER disabled"}


def test_send_target_metadata_success(mock_metrics_server):
    data = {
        "CI_JOB_ID": "job_123",
        "MAKE_TARGET": "build",
        "CI_PIPELINE_ID": "pipeline_123",
        "CI_PROJECT_ID": "project_123",
        "CI_PROJECT_NAME": "project_name_123",
    }

    response = client.post("/target", json=data)
    assert response.status_code == 200
    assert response.json() == "Target metadata recieved, sent to Prometheus!"


def test_send_target_metadata_invalid_data(mock_metrics_server):
    data = {
        "CI_JOB_ID": "job_123",
        "MAKE_TARGET": "build"
        # Missing CI_PIPELINE_ID and CI_PROJECT_ID and CI_PROJECT_NAME
    }
    try:
        client.post("/target", json=data)
    except RequestValidationError as e:
        assert e.errors() == [
            {
                "loc": ("body", "CI_PIPELINE_ID"),
                "msg": "Field required",
                "type": "missing",
                "input": {"CI_JOB_ID": "job_123", "MAKE_TARGET": "build"},
            },
            {
                "loc": ("body", "CI_PROJECT_ID"),
                "msg": "Field required",
                "type": "missing",
                "input": {"CI_JOB_ID": "job_123", "MAKE_TARGET": "build"},
            },
            {
                "loc": ("body", "CI_PROJECT_NAME"),
                "msg": "Field required",
                "type": "missing",
                "input": {"CI_JOB_ID": "job_123", "MAKE_TARGET": "build"},
            },
        ]
        assert e.body == data
    else:
        pytest.fail("RequestValidationError not raised")
