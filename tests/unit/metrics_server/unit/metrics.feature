Feature: Test Metrics API

  Scenario: Sending target metadata
    Given POST request to /target with data <data> and store in JOB_COLL
    Then return code 200

      Examples: Parameters
        | data   |
        | {"CI_PIPELINE_ID": "pipeline_123", "CI_JOB_ID": "job_456", "MAKE_TARGET": "target_1"} |

  Scenario: Sending job metadata
    Given POST request to /job with data <data> and store in JOB_COLL
    Then return code 200

      Examples: Parameters
        | data   |
        | {"CI_PIPELINE_ID": "pipeline_789", "CI_JOB_ID": "job_999", "other_key": "value_xyz"} |

  Scenario: Sending pipeline metadata
    Given POST request to /pipeline with data <data> and store in PIPELINE_COLL
    Then return code 200

      Examples: Parameters
        | data   |
        | {"key_1": "value_123", "key_2": "value_456", "key_3": "value_789"} |