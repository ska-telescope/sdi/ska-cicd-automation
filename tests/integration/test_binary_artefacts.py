import logging
import os
import zipfile

import pytest
import requests

from ska_cicd_automation.plugins.binary_artefacts.apis.oras import (
    harbor_delete_repository,
    oras_push,
)

LOGGER = logging.getLogger(__name__)
PREFIX_URL = "/binary_artefacts/api/v1"


@pytest.mark.post_deployment
@pytest.fixture
def setup_teardown_repo():
    """
    This ensures that the pipeline repository
    on harbor alwas as the same state.
    So integration tests are consistent.
    For every test:
        - A repository called pipeline should exist.
        - Inside pipeline repository an artefact with:
            - digest: sha256:d234d9a4129e57c845bc95f79973fb02608be45d98617afdb11c479a2f795e63 # noqa E501
            - tag: v1
    """
    logging.info("Teardown pipeline repository if it exists")
    harbor_delete_repository("pipeline")

    logging.info("Setup pipeline repository")
    file_path = "tests/upload.txt"
    manifest_annotations = {"included-files": "upload.txt"}
    oras_push(
        push_files=[file_path],
        name="pipeline",
        tag="v1",
        manifest_annnotations=manifest_annotations,
    )
    yield
    harbor_delete_repository("pipeline")


@pytest.mark.post_deployment
def test_list_artefacts(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline?page=1&page_size=10"  # noqa E501
    )

    response = requests.get(url, headers=headers)
    assert response.status_code == 200
    assert response.json()[0]["tags"][0]["name"] == "v1"
    assert (
        response.json()[0]["digest"]
        == "sha256:d234d9a4129e57c845bc95f79973fb02608be45d98617afdb11c479a2f795e63"  # noqa E501
    )


@pytest.mark.post_deployment
def test_list_artefact_by_tag(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v1"
    )

    response = requests.get(url, headers=headers)
    assert response.status_code == 200
    assert response.json()["tags"][0]["name"] == "v1"
    assert (
        response.json()["digest"]
        == "sha256:d234d9a4129e57c845bc95f79973fb02608be45d98617afdb11c479a2f795e63"  # noqa E501
    )


@pytest.mark.post_deployment
def test_push_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v2"
    )

    file_path = "tests/upload.txt"
    files = {"files": open(file_path, "rb")}

    response = requests.post(url, files=files, headers=headers)
    assert response.status_code == 201


@pytest.mark.post_deployment
def test_push_artefact_already_exists(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v1"
    )

    file_path = "tests/upload.txt"
    files = {"files": open(file_path, "rb")}

    response = requests.post(url, files=files, headers=headers)
    assert response.status_code == 409


@pytest.mark.post_deployment
def test_overwrite_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v1"
    )

    file_path = "tests/upload.txt"
    files = {"files": open(file_path, "rb")}

    response = requests.put(url, files=files, headers=headers)
    assert response.status_code == 201


@pytest.mark.post_deployment
def test_overwrite_nonexistent_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v2"
    )

    file_path = "tests/upload.txt"
    files = {"files": open(file_path, "rb")}

    response = requests.put(url, files=files, headers=headers)
    assert response.status_code == 404


@pytest.mark.post_deployment
def test_delete_artefact_by_tag(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v1"
    )

    response = requests.delete(url, headers=headers)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_delete_nonexistent_artefact_by_tag(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v2"
    )

    response = requests.delete(url, headers=headers)
    assert response.status_code == 404


@pytest.mark.post_deployment
def test_delete_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    artefact_sha = "d234d9a4129e57c845bc95f79973fb02608be45d98617afdb11c479a2f795e63"  # noqa E501
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/sha/"
        + artefact_sha
    )

    response = requests.delete(url, headers=headers)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_delete_nonexistent_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    nonexistent_artefact_sha = "sha256:11111"
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/sha/"
        + nonexistent_artefact_sha
    )

    response = requests.delete(url, headers=headers)
    assert response.status_code == 404


@pytest.mark.post_deployment
def test_download_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v1?format=zip"
    )

    response = requests.get(url, headers=headers)

    with open("downloaded.zip", "wb") as file:
        file.write(response.content)

    with zipfile.ZipFile("downloaded.zip") as zf:
        with zf.open("upload.txt") as f:
            line = f.readline()

    assert response.status_code == 200
    assert line == b"test file\n"


@pytest.mark.post_deployment
def test_download_nonexistent_artefact(setup_teardown_repo):
    token_header = os.getenv("CAR_HEADER")
    token = os.getenv("CAR_TOKEN")

    headers = {token_header: token}
    url = (
        os.getenv("SKAO_GITLAB_URL")
        + f"{PREFIX_URL}/artefacts/pipeline/tags/v2?format=zip"
    )

    response = requests.get(url, headers=headers)

    assert response.status_code == 404
