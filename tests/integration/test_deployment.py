import json
import os

import pytest
import requests


@pytest.mark.post_deployment
def test_get():
    url = os.getenv("SKAO_GITLAB_URL")
    response = requests.get(url)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_post_events():
    url = os.getenv("SKAO_GITLAB_URL") + "/gitlab/mr/events/"

    with open("tests/integration/event.json", "r", encoding="utf-8") as myfile:
        data = myfile.read()

    obj = json.loads(data)

    token_header = os.getenv("GITLAB_HEADER")
    token = os.getenv("GITLAB_TOKEN")

    headers = {token_header: token}

    response = requests.post(url, json=obj, headers=headers)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_post_pipeline_events():
    url = os.getenv("SKAO_GITLAB_URL") + "/gitlab/mr/pipeline/"

    with open(
        "tests/integration/event-pipeline.json", "r", encoding="utf-8"
    ) as myfile:
        data = myfile.read()

    obj = json.loads(data)

    token_header = os.getenv("GITLAB_HEADER")
    token = os.getenv("GITLAB_TOKEN")

    headers = {token_header: token}

    response = requests.post(url, json=obj, headers=headers)
    assert response.status_code == 200


@pytest.mark.post_deployment
def test_gitlab_mr_metrics_endpoint():
    test_post_events()

    url = os.getenv("SKAO_GITLAB_URL") + "/gitlab/mr/metrics"

    response = requests.get(url)
    assert response.status_code == 200


@pytest.mark.skip(reason="MongoDB not available anymore")
def test_metrics_server_api():
    url = os.getenv("SKAO_GITLAB_URL") + "/metrics/pipeline"

    with open(
        "tests/integration/metrics_server_payload.json", "r", encoding="utf-8"
    ) as myfile:
        data = myfile.read()

    obj = json.loads(data)

    response = requests.post(url, json=obj)
    assert response.status_code == 200
