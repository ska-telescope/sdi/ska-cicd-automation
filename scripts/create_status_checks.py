import gitlab
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

GITLAB_URL = "https://gitlab.com"
PRIVATE_TOKEN = "<YOUR-GITLAB-TOKEN>"

gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)

status_check_endpoint = "/projects/{project_id}/external_status_checks"
status_check_data = {
    "name": "Marvin",
    "external_url": "https://please-look-at-failures-in-marvin-table-below.dummy-url.status",  # This is a mock endpoint
}

exclusions = [] # Add project IDs as strings to skip enabling the status check

def enable_status_check_setting(project_id):
    if str(project_id) in exclusions:
        logger.info("    Skipped project ID: %s", project_id)
        return
    try:
        project = gl.projects.get(project_id)
        project.only_allow_merge_if_all_status_checks_passed = True
        project.save()
        logger.info("    Enabled status check setting for project ID: %s", project_id)
    except Exception as e:
        logger.error("    Failed to enable setting for project ID: %s, error: %s", project_id, e)


def check_status_check_exists(project_id):
    try:
        response = gl.http_get(status_check_endpoint.format(project_id=project_id))
    except Exception as e:
        logger.error("    Failed to get status checks for project ID: %s, error: %s", project_id, e)
        return False, None

    for status_check in response:
        if status_check.get("name") == status_check_data.get("name"):
            return True, status_check.get("id")
    
    return False, None


def update_status_check(project_id, status_check_id):
    status_check_data["id"] = status_check_id
    url = status_check_endpoint.format(project_id=project_id) + f"/{status_check_id}"
    try:
        gl.http_put(url, query_data=status_check_data)
        logger.info("    Updated status check for project ID: %s", project_id)
    except Exception as e:
        logger.error("    Failed to update status check for project ID: %s, error: %s", project_id, e)


def create_status_check(project_id):
    try:
        gl.http_post(status_check_endpoint.format(project_id=project_id), query_data=status_check_data)
        logger.info("    Created status check for project ID: %s", project_id)
    except Exception as e:
        logger.error("    Failed to create status check for project ID: %s, error: %s", project_id, e)


def ensure_status_check(project_id):
    status_check_exists, status_check_id = check_status_check_exists(project_id)
    
    if status_check_exists:
        update_status_check(project_id, status_check_id)
    else:
        create_status_check(project_id)


def get_all_subgroups(group_id):
    group = gl.groups.get(group_id)
    subgroups = group.subgroups.list(all=True)
    all_subgroups = subgroups.copy()

    for subgroup in subgroups:
        all_subgroups.extend(get_all_subgroups(subgroup.id))

    return all_subgroups


def get_all_projects(group_id):
    all_projects = []
    group = gl.groups.get(group_id)
    projects = group.projects.list(all=True)
    all_projects.extend(projects)

    subgroups = get_all_subgroups(group_id)
    for subgroup in subgroups:
        subgroup_projects = gl.groups.get(subgroup.id).projects.list(all=True)
        all_projects.extend(subgroup_projects)

    return all_projects


def iterate_groups_and_projects(group_id, indent=0):
    group = gl.groups.get(group_id)
    logger.info("Group: %s (ID: %s)", group.name, group.id)

    projects = group.projects.list(all=True)
    for project in projects:
        logger.info("  " * (indent + 1) + "Project: %s (ID: %s)", project.name, project.id)
        ensure_status_check(project.id)
        enable_status_check_setting(project.id)

    subgroups = get_all_subgroups(group_id)
    for subgroup in subgroups:
        iterate_groups_and_projects(subgroup.id, indent + 1)


if __name__ == "__main__":
    TOP_GROUP_ID = 3180705  # SKAO
    iterate_groups_and_projects(TOP_GROUP_ID)
