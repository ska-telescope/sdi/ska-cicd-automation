.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

  CHANGELOG

.. README =============================================================

.. This project most likely has its own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme

   README

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 2
  :caption: Plugins
  :hidden:

  GITLAB_MR_README
  JIRA_README
  GITLAB_SETTINGS_README
  BINARY_ARTEFACTS_README


Merge Request Checks
====================

These are all the packages, functions and scripts that form part of the project.

- :doc:`GITLAB_MR_README`
- :doc:`JIRA_README`
- :doc:`GITLAB_SETTINGS_README`
- :doc:`BINARY_ARTEFACTS_README`
