import logging
from enum import Enum

import prettytable


class MessageType(Enum):
    FAILURE = ":no_entry_sign:"
    WARNING = ":warning:"
    INFO = ":book:"

    def order(self):
        order = {"FAILURE": 3, "WARNING": 2, "INFO": 1}
        return order[self.name]


def get_message_type(enum_value: str):
    for enum in MessageType:
        if enum.value == enum_value:
            return enum

    return None


class MessageGenerator:
    message_marker = "ska-devsecops-mr-service-id"

    def __init__(self, logger_name):
        self.logger = logging.getLogger(logger_name)
        self.pretext = ""
        self.table = prettytable.PrettyTable()
        self.table.align = "c"
        self.table.field_names = ["Type", "Description", "Mitigation Strategy"]
        self.posttext = ""

    async def add_entry(self, type: MessageType, description, mitigation):
        self.table.add_row([type.value, description, mitigation])

    async def generate_metadata(self):
        metadata = [
            "<!--",
            f"MRServiceID: {MessageGenerator.message_marker}",
            "-->",
        ]
        return "\n".join(metadata)

    async def add_pretext(self, pretext):
        self.pretext = pretext

    async def add_posttext(self, posttext):
        self.posttext = posttext

    async def get_message(self):
        metadata = await self.generate_metadata()
        return "\n\n".join(
            [
                metadata,
                self.pretext,
                self.table.get_html_string(
                    sort_key=lambda x: get_message_type(x[0]).order()
                    if get_message_type(x[0]) is not None
                    else x,
                    sortby="Type",
                    reversesort=True,
                ),
                self.posttext,
            ]
        )

    async def get_well_done_message(self):
        self.table.clear_rows()
        self.table.format = True
        self.pretext = (
            ":tada: Well Done!"
            " All the merge request quality checks have passed!"
        )
        self.table.add_row(
            [":checkered_flag:", "All checks passed!", ":coffee:"]
        )
        return await self.get_message()

    async def find_comment(self, mr_comments, comment_description):

        marvin_comment = None

        async for comment in mr_comments:
            if MessageGenerator.message_marker in comment["body"]:
                if comment_description not in comment:
                    marvin_comment = comment

        return marvin_comment

    async def add_entry_to_comment(
        self,
        comment: str,
    ):

        new_comment = await self.get_message()

        marvin_comment = comment["body"]

        start = "<tbody>\n"
        end = "</tbody>"
        # get only body from comment
        marvin_comment = (marvin_comment.split(start))[1].split(end)[0]
        # add existing table to new one
        index = new_comment.find(end)

        new_comment = (
            new_comment[:index] + marvin_comment + new_comment[index:]
        )

        return new_comment
