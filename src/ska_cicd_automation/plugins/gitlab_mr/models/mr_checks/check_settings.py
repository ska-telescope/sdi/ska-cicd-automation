# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.fix_comment_generator import (
    ChangeType,
    FixCommentGenerator,
)
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook


# NOTE: This check's feature-flag is disabled. Ask Ugur why.
class CheckSettings(Check):
    feature_toggle = "check-settings"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.failing_maintainer_checks = []
        self.failing_mr_checks = []

    async def fix_required_settings(self, proj_id: int, mr_id: int):
        # ST-1030: Get current, pre-fix settings
        proj_settings = await self.api.get_project_settings_info(proj_id)
        proj_mr_approvals = (
            await self.api.get_project_merge_request_approvals_info(proj_id)
        )
        mr_settings = await self.api.get_merge_request_info(proj_id, mr_id)
        job_token_scope = await self.api.get_job_token_scope(proj_id)
        self.logger.debug("Pre-Fix Proj Settings: %s", proj_settings)
        self.logger.debug("Pre-Fix Proj MR Approvals: %s", proj_mr_approvals)
        self.logger.debug("Pre-Fix MR Settings: %s", mr_settings)
        self.logger.debug(
            "Pre-Fix Job Token Access Settings: %s", job_token_scope
        )

        # ST-1030: Instantiate a comment generator to display the changes made
        commentor = FixCommentGenerator(__name__, self.api)

        changeType = ChangeType.SUCCESS
        # ST-1030: Fix Merge Request Settings
        if (
            not mr_settings["force_remove_source_branch"]
            or mr_settings["squash"]
        ):
            result = await self.api.modify_merge_request(
                proj_id,
                mr_id,
                remove_source_branch=True,
                squash=False,
            )
            if not result or "error" in result:
                changeType = ChangeType.FAILURE

        # ST-1030: Add the relevant information regarding the changes
        if not mr_settings["force_remove_source_branch"]:
            await commentor.add_entry(
                changeType,
                "Check 'Delete source branch when MR is accepted'.",
                "Successful",
                "Check Failed",
            )

        if mr_settings["squash"]:
            await commentor.add_entry(
                changeType,
                "Check 'Squash commits when MR is accepted is unabled'.",
                "Successful",
                "Check Failed",
            )

        changeType = ChangeType.SUCCESS
        # ST-1030: Fix Project-level Merge Request Approval Rules
        if not proj_mr_approvals["reset_approvals_on_push"]:
            result = await self.api.modify_project_level_mr_approval_settings(
                proj_id,
                reset_approvals_on_push=True,
            )
            if not result or "error" in result:
                changeType = ChangeType.FAILURE

        # ST-1030: Add the relevant information regarding the changes
        if not proj_mr_approvals["reset_approvals_on_push"]:
            await commentor.add_entry(
                changeType,
                (
                    "Check 'Remove all approvals when commits are added "
                    "to the source branch'."
                ),
                "Successful",
                "Check Failed",
            )
        # ST-2023: Fix job token scope
        if not job_token_scope["inbound_enabled"]:
            result = await self.api.modify_job_token_access(
                proj_id,
                enabled=True,
            )
            if not result or "error" in result:
                changeType = ChangeType.FAILURE
        if not job_token_scope["inbound_enabled"]:
            await commentor.add_entry(
                changeType,
                "Check 'Job Token Scope is 'Only this project and any groups and projects in the allowlist'.",
                "Successful",
                "Check Failed",
            )

        # ST-1030: Fix Project Settings
        if (
            proj_settings["allow_merge_on_skipped_pipeline"]
            or proj_settings["request_access_enabled"]
            or proj_settings["auto_devops_enabled"]
            or proj_settings["issues_enabled"]
            or proj_settings["ci_config_path"] not in (None, "")
            or not proj_settings["merge_method"] == "merge"
            or not proj_settings["resolve_outdated_diff_discussions"]
            or not proj_settings["only_allow_merge_if_pipeline_succeeds"]
            or not proj_settings["remove_source_branch_after_merge"]
            or not proj_settings["printing_merge_request_link_enabled"]
            or not proj_settings["security_and_compliance_access_level"]
            == "private"
            or not proj_settings["auto_cancel_pending_pipelines"] == "enabled"
            or not proj_settings["build_git_strategy"] == "fetch"
            or not proj_settings["squash_option"] == "never"
            or not proj_settings["keep_latest_artifact"]
            or not proj_settings[
                "only_allow_merge_if_all_status_checks_passed"
            ]
        ):
            result = await self.api.modify_project(
                proj_id,
                merge_method="merge",
                resolve_outdated_diff_discussions=True,
                only_allow_merge_if_pipeline_succeeds=True,
                remove_source_branch_after_merge=True,
                printing_merge_request_link_enabled=True,
                request_access_enabled=False,
                security_and_compliance_access_level="private",
                issues_access_level="disabled",
                auto_cancel_pending_pipelines="enabled",
                allow_merge_on_skipped_pipeline=False,
                auto_devops_enabled=False,
                build_git_strategy="fetch",
                ci_config_path="",
                squash_option="never",
                keep_latest_artifact=True,
                only_allow_merge_if_all_status_checks_passed=True,
            )
            if not result or "error" in result:
                changeType = ChangeType.FAILURE
        # ST-1030: Add the relevant information regarding the changes
        if not proj_settings["merge_method"] == "merge":
            await commentor.add_entry(
                changeType,
                "Check 'Merge method should be 'Merge Commit'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["resolve_outdated_diff_discussions"]:
            await commentor.add_entry(
                changeType,
                "Check 'Automatically resolve MR diff discussions'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["only_allow_merge_if_pipeline_succeeds"]:
            await commentor.add_entry(
                changeType,
                "Check 'Pipelines must succeed'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["remove_source_branch_after_merge"]:
            await commentor.add_entry(
                changeType,
                "Check 'Enable delete source branch option by default'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["printing_merge_request_link_enabled"]:
            await commentor.add_entry(
                changeType,
                "Check 'Show link to create/view MR when pushing is enabled.",
                "Successful",
                "Check Failed",
            )

        if proj_settings["request_access_enabled"]:
            await commentor.add_entry(
                changeType,
                "Check 'Request member access not allowed'.",
                "Successful",
                "Check Failed",
            )

        if (
            not proj_settings["security_and_compliance_access_level"]
            == "private"
        ):
            await commentor.add_entry(
                changeType,
                "Check 'Visibility of security and compliance must be private'.",
                "Successful",
                "Check Failed",
            )

        if proj_settings["issues_enabled"]:
            await commentor.add_entry(
                changeType,
                "Check 'Issues is disabled'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["auto_cancel_pending_pipelines"] == "enabled":
            await commentor.add_entry(
                changeType,
                "Check 'Auto-cancel pending pipelines is enabled'.",
                "Successful",
                "Check Failed",
            )

        if proj_settings["allow_merge_on_skipped_pipeline"]:
            await commentor.add_entry(
                changeType,
                "Check 'MRs cannot be merged with skipped jobs'.",
                "Successful",
                "Check Failed",
            )

        if proj_settings["auto_devops_enabled"]:
            await commentor.add_entry(
                changeType,
                "Check 'Auto DevOps is disabled'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["build_git_strategy"] == "fetch":
            await commentor.add_entry(
                changeType,
                "Check 'Git strategy is fetch'.",
                "Successful",
                "Check Failed",
            )

        if proj_settings["ci_config_path"] not in (None, ""):
            await commentor.add_entry(
                changeType,
                "Check 'Path to CI configuration file is default'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["squash_option"] == "never":
            await commentor.add_entry(
                changeType,
                "Check 'Squash option should be never'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["keep_latest_artifact"]:
            await commentor.add_entry(
                changeType,
                "Check 'Keep the latest artifact is enabled'.",
                "Successful",
                "Check Failed",
            )

        if not proj_settings["only_allow_merge_if_all_status_checks_passed"]:
            await commentor.add_entry(
                changeType,
                "Check 'Status checks must succeed'.",
                "Successful",
                "Check Failed",
            )
        # ST-1030: Add a comment (only when an entry was added)
        await commentor.execute(proj_id, mr_id)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        # ST-1030: Fix required settings automatically if possible
        await self.fix_required_settings(proj_id, mr_id)
        # ST-1030: Get current, post-fix settings
        proj_settings = await self.api.get_project_settings_info(proj_id)
        proj_mr_approvals = (
            await self.api.get_project_merge_request_approvals_info(proj_id)
        )
        mr_approvals = await self.api.get_merge_request_approvals_info(
            proj_id, mr_id
        )
        mr_settings = await self.api.get_merge_request_info(proj_id, mr_id)
        job_token_scope = await self.api.get_job_token_scope(proj_id)

        self.logger.debug("Post-Fix Proj Settings: %s", proj_settings)
        self.logger.debug("Post-Fix Proj MR Approvals: %s", proj_mr_approvals)
        self.logger.debug("Post-Fix MR Approvals: %s", mr_approvals)
        self.logger.debug("Post-Fix MR Settings: %s", mr_settings)
        self.logger.debug(
            "Post-Fix Job Token Access Settings: %s", job_token_scope
        )

        if not mr_settings["reviewers"]:
            self.failing_mr_checks.append(
                "Please appoint one or more people as reviewer(s)"
            )
        if not mr_settings["force_remove_source_branch"]:
            self.failing_mr_checks.append(
                (
                    "Please check Delete source branch when MR is accepted. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=force_remove_source_branch)"  # NOQA: E501
                )
            )
        if mr_settings["squash"]:
            self.failing_mr_checks.append(
                (
                    "Please uncheck Squash commits when MR is accepted. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=squash)"  # NOQA: E501
                )
            )
        if not proj_settings["squash_option"] == "never":
            self.failing_mr_checks.append(
                (
                    "Please Squash option must be never. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=squash_option)"  # NOQA: E501
                )
            )
        if not proj_settings["merge_method"] == "merge":
            self.failing_maintainer_checks.append(
                (
                    "Merge Method should be Merge Commit. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=merge_method)"  # NOQA: E501
                )
            )
        if not proj_settings["resolve_outdated_diff_discussions"]:
            self.failing_maintainer_checks.append(
                (
                    "Automatically resolve MR diff discussions "
                    "should be checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=resolve_outdated_diff_discussions)"  # NOQA: E501
                )
            )
        if not proj_settings["printing_merge_request_link_enabled"]:
            self.failing_maintainer_checks.append(
                (
                    "Show link to create/view MR when pushing from "
                    "the command line should be checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=printing_merge_request_link_enabled)"  # NOQA: E501
                )
            )
        if not proj_settings["remove_source_branch_after_merge"]:
            self.failing_maintainer_checks.append(
                (
                    "Enable Delete source branch option by default should be "
                    "checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=remove_source_branch_after_merge)"  # NOQA: E501
                )
            )
        if not proj_settings["only_allow_merge_if_pipeline_succeeds"]:
            self.failing_maintainer_checks.append(
                (
                    "Pipelines must succeed should be checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=only_allow_merge_if_pipeline_succeeds)"  # NOQA: E501
                )
            )

        if proj_settings["request_access_enabled"]:
            self.failing_maintainer_checks.append(
                (
                    "Request member access should not be allowed. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=request_access_enabled)"  # NOQA: E501
                )
            )

        if (
            not proj_settings["security_and_compliance_access_level"]
            == "private"
        ):
            self.failing_maintainer_checks.append(
                (
                    "Security and compliance access level should be private. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=security_and_compliance_access_level)"  # NOQA: E501
                )
            )

        if proj_settings["issues_enabled"]:
            self.failing_maintainer_checks.append(
                (
                    "Issues should be disabled. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=issues_access_level)"  # NOQA: E501
                )
            )

        if not proj_settings["auto_cancel_pending_pipelines"] == "enabled":
            self.failing_maintainer_checks.append(
                (
                    "Auto-cancel pending pipelines should be enabled. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=auto_cancel_pending_pipelines)"  # NOQA: E501
                )
            )

        if proj_settings["allow_merge_on_skipped_pipeline"]:
            self.failing_maintainer_checks.append(
                (
                    "Merging on skipped pipelines should not be allowed. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=allow_merge_on_skipped_pipeline)"  # NOQA: E501
                )
            )

        if proj_settings["auto_devops_enabled"]:
            self.failing_maintainer_checks.append(
                (
                    "Auto DevOps should be disabled. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=auto_devops_enabled)"  # NOQA: E501
                )
            )

        if not proj_settings["build_git_strategy"] == "fetch":
            self.failing_maintainer_checks.append(
                (
                    "Build Git strategy should be 'fetch'. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=build_git_strategy)"  # NOQA: E501
                )
            )

        if proj_settings["ci_config_path"] not in (None, ""):
            self.failing_maintainer_checks.append(
                (
                    "CI configuration path should be default (empty). "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=ci_config_path)"  # NOQA: E501
                )
            )

        if not proj_settings["keep_latest_artifact"]:
            self.failing_maintainer_checks.append(
                (
                    "Keep the latest artifact should be enabled. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=keep_latest_artifact)"  # NOQA: E501
                )
            )

        if not proj_settings["only_allow_merge_if_all_status_checks_passed"]:
            self.failing_maintainer_checks.append(
                (
                    "Status checks must succeed. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=only_allow_merge_if_all_status_checks_passed)"  # NOQA: E501
                )
            )
        # ST-1030: This fix will fail if the project-level approval rule
        # disable_overriding_approvers_per_merge_request is set to True.
        # This is also why this check was moved before the project-level fixes.
        if mr_approvals["approvals_required"] == 0:
            self.failing_mr_checks.append(
                "Please try to set at least 1 approval rule to ensure merge "
                "request is reviewed and approved"
            )
        if not proj_mr_approvals[
            "disable_overriding_approvers_per_merge_request"
        ]:
            self.failing_maintainer_checks.append(
                (
                    "'Prevent editing approval rules in merge requests'"
                    " should be checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=disable_overriding_approvers_per_merge_request)"  # NOQA: E501
                )
            )
        if not proj_mr_approvals["reset_approvals_on_push"]:
            self.failing_maintainer_checks.append(
                (
                    "'Remove all approvals when commits are added"
                    " to the source branch'"
                    " should be checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=reset_approvals_on_push)"  # NOQA: E501
                )
            )
        if proj_mr_approvals["merge_requests_author_approval"]:
            self.failing_maintainer_checks.append(
                (
                    "'Prevent approval by author' should be checked. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=merge_requests_author_approval)"  # NOQA: E501
                )
            )
        if not job_token_scope["inbound_enabled"]:
            self.failing_maintainer_checks.append(
                (
                    "'Set the job token scope to 'Only this project and any groups and projects in the allowlist'. "
                    f"[Fix](https://webhooks.api.skatelescope.org/gitlab/settings/manual_fix?proj_id={proj_id}&mr_id={mr_id}&setting=job_token_scope)"  # NOQA: E501
                )
            )
        return (
            not self.failing_maintainer_checks and not self.failing_mr_checks
        )

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Adjust Settings"

    async def mitigation_strategy(self) -> str:
        init_msg = (
            "Reconfigure Gitlab Settings according to "
            "[the guidelines](https://developer.skatelescope.org/en/latest/tools/ci-cd/gitlab-settings.html)."  # NOQA: E501
        )
        mr_checks_mitigation = ""
        proj_mr_checks_mitigation = ""
        if self.failing_mr_checks:
            mr_checks = "\n".join(
                f" - {check}" for check in self.failing_mr_checks
            )
            mr_checks_mitigation = (
                f"\n\nChange these settings on your MR:\n{mr_checks}"
            )
        if self.failing_maintainer_checks:
            proj_mr_checks = "\n".join(
                f" - {check}" for check in self.failing_maintainer_checks
            )

            proj_mr_checks_mitigation = (
                f"\n\nChange these settings on the project level"
                " (You may need Maintainer rights to change these):"
                f"\n{proj_mr_checks}"
            )

        return f"{init_msg}{mr_checks_mitigation}{proj_mr_checks_mitigation}"
