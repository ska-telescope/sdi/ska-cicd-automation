# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook


class CheckCodeownersExist(Check):
    feature_toggle = "check-codeowners-exist"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        source_branch = mr_event.object_attributes.source_branch

        try:
            response = await self.api.get_file_from_repository(
                proj_id, "CODEOWNERS", source_branch
            )
        except Exception:
            self.logger.debug("CODEOWNERS file not found.")
            return False

        if response.get("content"):
            return True

        return False

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Missing CODEOWNERS file"

    async def mitigation_strategy(self) -> str:
        return "Please add a [CODEOWNERS](https://docs.gitlab.com/ee/user/project/codeowners/) file to the root folder."  # NOQA: E501
