# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook
from ska_cicd_automation.plugins.gitlab_mr.models.repo_tree_agent import (
    RepoTreeAgent,
)


class CheckDirectoryCompliance(Check):
    feature_toggle = "check-directory-compliance"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.missing_compliance_rules: dict[str, set] = {}

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        repo_tree_agent = RepoTreeAgent(self.api, proj_id, mr_event)
        await repo_tree_agent.process_repo_tree()
        repo_types = repo_tree_agent.get_repo_types()
        repo_compliances = repo_tree_agent.get_repo_compliances()
        self.logger.debug("Repo Types: %s", repo_types)
        self.logger.debug("Compliances: %s", repo_compliances)

        # Get Compliance rules for every repo
        # if the repo is a valid repo type but fails compliancy
        for repo_type, report in repo_tree_agent.reports.items():
            if report.is_repo and not report.is_compliant:
                self.missing_compliance_rules[repo_type] = set(
                    report.failed_compliance_rules
                )
        self.logger.info("Missing Rules: %s", self.missing_compliance_rules)
        return len(self.missing_compliance_rules.keys()) == 0

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return (
            "Repository Structure is not following standardised "
            "[Project Structure]"
            "(https://confluence.skatelescope.org/display/SE/Standardising+Project+Structure+and+Content)"  # NOQA: E501
        )

    async def mitigation_strategy(self) -> str:
        missing_rules = []
        for repo_key, rules in self.missing_compliance_rules.items():
            missing_rules.extend(
                [f"- {repo_key.value.capitalize()}: {rule}" for rule in rules]
            )
        if missing_rules:
            return (
                "Following rules failed for the repository structure:\n"
                + "\n".join(missing_rules)
            )
