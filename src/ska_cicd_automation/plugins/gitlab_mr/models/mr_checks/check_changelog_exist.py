# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

search_query = "changelog"
changelog_files = ["changelog.md", "changelog.rst"]


class CheckChangelogExist(Check):
    feature_toggle = "check-changelog-exist"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.file_found = False

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        source_branch = mr_event.object_attributes.source_branch

        try:
            response = await self.api.get_search_results(
                proj_id, "blobs", search_query, ref=source_branch
            )
            async for item in response:
                item_filepath = item.get("filename", "").lower()
                if any(
                    file_name in item_filepath for file_name in changelog_files
                ):
                    self.file_found = True

        except Exception as e:
            self.logger.error(
                'Search query "%s" provided no results. %s',
                search_query,
                str(e),
            )

        return self.file_found

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Missing CHANGELOG file"

    async def mitigation_strategy(self) -> str:
        return "Please add a [CHANGELOG.md](https://developer.skao.int/en/latest/tutorial/release-management/automate-release-process.html#how-to-add-the-changelog) file to the root folder."  # NOQA: E501
