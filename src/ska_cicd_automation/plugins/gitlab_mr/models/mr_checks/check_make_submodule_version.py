import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

MAKE_PROJECT_ID = 29572088


class CheckMakeSubmoduleVersion(Check):
    feature_toggle = "check_make_submodule_version"

    def __init__(self, gitlab_api: GitLabApi, logger_name: str):
        self.gitlab_api = gitlab_api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        latest_make_commit_info = await self.gitlab_api.get_commits(
            MAKE_PROJECT_ID, "master"
        )

        async for commit in latest_make_commit_info:
            latest_make_commit_date = commit.get("created_at")
            break

        try:
            project_make_commit_blob = (
                await self.gitlab_api.get_file_from_repository(
                    proj_id, ".make", mr_event.object_attributes.source_branch
                )
            )

            project_make_commit_ref = project_make_commit_blob.get("blob_id")
            project_make_commit_info = await self.gitlab_api.get_commit(
                MAKE_PROJECT_ID, project_make_commit_ref
            )

            project_make_commit_date = project_make_commit_info.get(
                "created_at"
            )
        except Exception:
            self.logger.error("Repository does not contain .make submodule.")
            return True

        if latest_make_commit_date > project_make_commit_date:
            return False

        return True

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Makefile submodule outdated"

    async def mitigation_strategy(self) -> str:
        return (
            "The .make submodule used in this project is outdated.\n"
            "Please consider running 'make make' to update the reference."
        )
