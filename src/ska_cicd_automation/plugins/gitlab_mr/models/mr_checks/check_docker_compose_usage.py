# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

FILE_NAMES = ["docker-compose.yml", "docker-compose.yaml"]


class CheckDockerComposeUsage(Check):
    feature_toggle = "check-docker-compose-usage"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.file_list = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):

        # NOTE: Maybe move this to mrevents to skip all checks for certain repos
        if proj_id == 9070656:  # project devportal doesn't need to be checked
            return not self.file_list

        source_branch = mr_event.object_attributes.source_branch
        mr_search = await self.api.get_search_results(
            proj_id, "blobs", "docker-compose", ref=source_branch
        )

        async for search_result in mr_search:
            if search_result["filename"] in FILE_NAMES:
                self.file_list.append(
                    (search_result["path"], search_result["startline"])
                )

        self.logger.debug("Search API result: %s", self.file_list)

        return not self.file_list

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Docker Compose Usage"

    async def mitigation_strategy(self) -> str:
        init_msg = "Please remove docker-compose from following files."
        files = "".join(
            f" - {file[0]} on line {file[1]}\n" for file in self.file_list
        )
        return f"{init_msg}\n{files}"
