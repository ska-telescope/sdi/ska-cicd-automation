# flake8: noqa E501
import logging
import re

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook


class CheckProjSlug(Check):
    feature_toggle = "check-proj-slug"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):

        path_with_namespace = mr_event.project.path_with_namespace
        slug_name = path_with_namespace.split("/")[-1]

        complaint_slug_match = re.search(r"(ska-[a-z0-9])", slug_name)

        return complaint_slug_match is not None

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Non-Complaint Project Slug Name"

    async def mitigation_strategy(self) -> str:
        return (
            "Project Slug should start with ska-. \n\n"
            "To change the slug go into: Settings->Advanced->Change Path\n\n"
            "If you don't have access to this settings, please contact the "
            "system team at #team-system-support slack channel"
        )
