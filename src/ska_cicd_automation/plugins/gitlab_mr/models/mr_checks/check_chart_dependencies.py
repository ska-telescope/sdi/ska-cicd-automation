# flake8: noqa E501
import base64
import logging
import os

import yaml
from packaging import version
from packaging.specifiers import SpecifierSet
from packaging.version import InvalidVersion, Version
from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.apis.nexus import NexusApi
from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

FILE_NAMES = ["Chart.yaml", "Chart.yml"]
CHARTS_FOLDER_PATH = "charts"
DEPRECATED_CHARTS_FILE = os.path.join(
    os.path.dirname(__file__), "..", "rules", "deprecated_charts.yaml"
)


class CheckChartDependencies(Check):
    feature_toggle = "check-chart-dependencies"

    def __init__(self, api: GitLabApi, nexus: NexusApi, logger_name):
        self.api = api
        self.nexus = nexus
        self.logger = logging.getLogger(logger_name)
        self.deprecated_charts_reference = {}
        self.deprecated_charts = {}
        self.outdated_charts = {}
        self.invalid_versions = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        response = await self.api.get_repo_tree(
            proj_id=proj_id,
            ref=mr_event.object_attributes.source_branch,
            recursive=True,
        )
        repo_tree = [dict(tree_item) async for tree_item in response]

        with open(DEPRECATED_CHARTS_FILE, encoding="utf-8") as file:
            deprecated_charts_data = yaml.safe_load(file)

        for chart in deprecated_charts_data:
            self.deprecated_charts_reference[chart.get("name")] = chart.get(
                "version"
            )

        chart_files = []
        for item in repo_tree:
            if item.get("message") == "404 File Not Found":
                self.logger.error("Error fetching repository tree.")
                return True

            if item.get("name") in FILE_NAMES:
                chart_files.append(
                    {"path": item.get("path"), "chart": item.get("name")}
                )

        for chart in chart_files:
            try:
                file_decoded = await self._get_file_decoded_from_repo(
                    mr_event, proj_id, chart.get("path").replace("/", "%2F")
                )
            except Exception as e:
                self.logger.error(
                    "Error fetching file from repository: %s", str(e)
                )
                return True

            try:
                yaml_content = yaml.safe_load(file_decoded)
            except yaml.YAMLError as e:
                self.logger.error("Error loading YAML content: %s", str(e))
                return True

            dependencies = yaml_content.get("dependencies", [])
            for dependency in dependencies:
                chart_name = dependency.get("name")
                chart_version = (
                    dependency.get("version").split("-dev")[0].replace("~", "")
                )
                chart_version_nexus = (
                    await self.nexus.get_latest_chart_version(chart_name)
                )

                try:
                    version.parse(chart_version)
                except InvalidVersion:
                    self.logger.error(
                        "Chart version %s is unsupported. Please notify System Team!",
                        chart_version,
                    )
                    self.invalid_versions.append(
                        f"Chart version {chart_version} is unsupported. Please notify System Team!"
                    )
                    continue

                if self._check_if_deprecated(chart_name, chart_version):
                    deprecated_chart = {
                        "name": chart_name,
                        "version": chart_version,
                    }
                    self.deprecated_charts.setdefault(
                        chart.get("path"), []
                    ).append(deprecated_chart)
                    continue

                if chart_version_nexus.get(
                    "version"
                ) is not None and version.parse(chart_version) < version.parse(
                    chart_version_nexus.get("version")
                ):
                    outdated_chart = {
                        "name": chart_name,
                        "version": chart_version,
                        "nexus_version": chart_version_nexus.get("version"),
                    }
                    self.outdated_charts.setdefault(
                        chart.get("path"), []
                    ).append(outdated_chart)

        return (
            not self.outdated_charts.keys()
            and not self.deprecated_charts.keys()
        )

    async def type(self) -> MessageType:
        if self.deprecated_charts.keys():
            return MessageType.FAILURE
        return MessageType.WARNING

    async def description(self) -> str:
        return "Outdated Helm Charts"

    async def mitigation_strategy(self) -> str:
        header = "The repository contains helm charts with outdated dependencies, please update the following: \n"
        messages = []
        for file in self.outdated_charts.keys():
            messages.append(f"\nChart: {file}\n")
            for chart in self.outdated_charts[file]:
                messages.append(
                    f" - {chart['name']}: using {chart['version']}, {chart['nexus_version']} available.\n"
                )

        for file in self.deprecated_charts.keys():
            messages.append(f"\nChart: {file}\n")
            for chart in self.deprecated_charts[file]:
                version_range = self.deprecated_charts_reference[chart["name"]]
                messages.append(
                    f" - {chart['name']}: {chart['version']} is deprecated! Please use a version outside the range {version_range}.\n"
                )

        for invalid_version_message in self.invalid_versions:
            messages.append(f" - {invalid_version_message}\n")

        return header + "".join(messages)

    def _check_if_deprecated(self, chart_name, chart_version):
        version_range = self.deprecated_charts_reference.get(chart_name)
        if version_range is None:
            return False

        spec = SpecifierSet(version_range)
        return Version(chart_version) in spec

    async def _get_file_decoded_from_repo(self, mr_event, proj_id, file_path):
        source_branch = mr_event.object_attributes.source_branch
        response = await self.api.get_file_from_repository(
            proj_id, file_path, source_branch
        )
        if response.get("message") == "404 File Not Found":
            raise FileNotFoundError("File not found: %s" % str(file_path))

        try:
            base64_bytes = response["content"].encode()
            message_bytes = base64.b64decode(base64_bytes)
            file_decoded = message_bytes.decode()
        except UnicodeDecodeError as e:
            raise UnicodeDecodeError(
                "Error decoding Unicode: %s" % str(e)
            ) from e
        except AttributeError as e:
            raise AttributeError("AttributeError: %s" % str(e)) from e
        except base64.binascii.Error as e:
            raise base64.binascii.Error(
                "Base64 decoding error: %s" % str(e)
            ) from e
        return file_decoded
