# flake8: noqa E501
import base64
import logging
import os

import yaml
from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook
from ska_cicd_automation.plugins.gitlab_mr.models.repo_tree_agent import (
    RepoTreeAgent,
)

TEMPLATE_REPOSITORY = "ska-telescope/templates-repository"
PIPELINE_RULES_FILE_PATH = os.path.join(
    os.path.dirname(__file__), "..", "rules", "pipeline_rules.yaml"
)


class CheckPipelineJobs(Check):
    feature_toggle = "check-pipeline-jobs"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.missing_includes = []
        self.partial_includes = []
        self.cicd_file_exists = True
        self.file_checks = self._load_rules_from_yaml()
        self.project_template = "ska-telescope/templates-repository"

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        repo_tree_agent = RepoTreeAgent(self.api, proj_id, mr_event)
        await repo_tree_agent.process_repo_tree()
        repo_types = repo_tree_agent.get_repo_types()

        ci_cd_file_path = mr_event.project.ci_config_path
        if ci_cd_file_path == "" or ci_cd_file_path is None:
            ci_cd_file_path = ".gitlab-ci.yml"

        ci_cd_file_path = ci_cd_file_path.replace("/", "%2F")

        try:
            file_decoded = await self._get_file_decoded_from_repo(
                mr_event, proj_id, ci_cd_file_path
            )
            file_data = yaml.safe_load(file_decoded)
        except Exception as ex:
            self.logger.error("Exception occurred: %s", str(ex))
            self.cicd_file_exists = False
            return False

        # This is to handle different yaml structures (See STS-314)
        included_templates = []
        for included_template in file_data.get("include", []):
            if (
                isinstance(included_template, dict)
                and "project" in included_template
                and "file" in included_template
            ):
                project = included_template.get("project")
                included_file = included_template.get("file")
                if isinstance(included_file, list):
                    for file in included_file:
                        included_templates.append(
                            {"project": project, "file": file}
                        )
                else:
                    included_templates.append(
                        {"project": project, "file": included_file}
                    )

        identified_repo_types = [
            repo_type
            for repo_type, repo_is_type in repo_types.items()
            if repo_is_type
        ]
        identified_repo_types.append(
            "all"
        )  # Some templates are common to all repo types
        files_to_check = [
            file_check
            for file_check in self.file_checks
            if file_check.get("repo_type") in identified_repo_types
        ]

        for file_check in files_to_check:
            operator = file_check.get("op")
            expected_files = file_check.get("files")

            results = []
            missing_includes = []
            partial_includes = []
            for expected_file in expected_files:
                file_full = expected_file.get("file")
                file_partial = expected_file.get("partial_include")
                file_description = expected_file.get("description")

                found = False
                partial = False
                for included_template in included_templates:
                    if file_full == included_template.get("file"):
                        found = True
                        break
                    elif file_partial == included_template.get("file"):
                        partial = True
                        break

                if found:
                    results.append(True)
                elif partial:
                    partial_includes.append((file_full, file_description))
                    results.append(False)
                else:
                    missing_includes.append((file_full, file_description))
                    results.append(False)

            if operator == "or":
                result = any(results)
            elif operator == "and":
                result = all(results)
            else:
                result = any(results)

            if not result:
                self.missing_includes.extend(missing_includes)
                self.partial_includes.extend(partial_includes)

        return (
            len(self.missing_includes) == 0 and len(self.partial_includes) == 0
        )

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Pipeline Templates Usage"

    async def mitigation_strategy(self) -> str:
        if not self.cicd_file_exists:
            return (
                "Please create a "
                "[pipeline](https://developer.skatelescope.org/en/latest/tools/ci-cd.html) "  # NOQA: E501
                "for this MR"
            )

        message = "The repository is not using any of the CI/CD Templates, Please include the following templates:"

        if len(self.missing_includes) or len(self.partial_includes):
            message = (
                "The repository is only using a subset of the templates "
                "that are available. \n"
                "Please include the rest of the templates "
                "to cover the whole software lifecycle or contact "
                "#team-system-support if there's a reason you can't use it.\n"
                "The missing templates are: "
            )

        missing_includes = "\n".join(
            f" - file: {file} # {description}"
            for file, description in self.missing_includes
        )
        missing_partial_includes = "\n".join(
            f" - file: {file} # {description}"
            for file, description in self.partial_includes
        )
        missing_jobs_mitigation = (
            f"{message}"
            f"\n{missing_includes}"
            f"\n{missing_partial_includes}"
        )

        return missing_jobs_mitigation

    def _load_rules_from_yaml(self):
        with open(PIPELINE_RULES_FILE_PATH, "r", encoding="utf-8") as f:
            file_content = f.read()
            rules_data = yaml.safe_load(file_content)
            rules = rules_data.get("pipeline_rules", [])

        return rules

    async def _get_file_decoded_from_repo(self, mr_event, proj_id, file_path):
        source_branch = mr_event.object_attributes.source_branch
        response = await self.api.get_file_from_repository(
            proj_id, file_path, source_branch
        )
        if response.get("message") == "404 File Not Found":
            raise FileNotFoundError("File not found: %s" % str(file_path))

        try:
            base64_bytes = response["content"].encode()
            message_bytes = base64.b64decode(base64_bytes)
            file_decoded = message_bytes.decode()
        except UnicodeDecodeError as e:
            raise UnicodeDecodeError(
                "Error decoding Unicode: %s" % str(e)
            ) from e
        except AttributeError as e:
            raise AttributeError("AttributeError: %s" % str(e)) from e
        except base64.binascii.Error as e:
            raise base64.binascii.Error(
                "Base64 decoding error: %s" % str(e)
            ) from e
        return file_decoded
