# flake8: noqa E501
import base64
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook
from ska_cicd_automation.plugins.gitlab_mr.models.repo_tree_agent import (
    RepoTreeAgent,
)


class CheckMakefileIncludes(Check):
    feature_toggle = "check-makefile-includes"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.missing_files = []
        self.all_included = ".make/*.mk"
        self.base_included = {
            "file": ".make/base.mk",
            "description": "Core make support",
        }
        self.file_checks = [
            {
                "file": ".make/oci.mk",
                "description": "OCI Images support",
                "tags": ["oci"],
            },
            {
                "file": ".make/python.mk",
                "description": "Python support",
                "tags": ["python"],
            },
            {
                "file": ".make/raw.mk",
                "description": "Raw support",
                "tags": ["raw"],
            },
            {
                "file": ".make/helm.mk",
                "description": "Helm chart support",
                "tags": ["helm"],
            },
        ]

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        repo_tree_agent = RepoTreeAgent(self.api, proj_id, mr_event)
        await repo_tree_agent.process_repo_tree()
        repo_types = repo_tree_agent.get_repo_types()

        self.logger.info(
            "is python repo: %s, has helm chart: %s, has dockerfile: %s, has raw folder: %s",
            repo_types["python"],
            repo_types["helm"],
            repo_types["oci"],
            repo_types["raw"],
        )

        try:
            file_decoded = await self._get_file_decoded_from_repo(
                mr_event, proj_id, "Makefile"
            )
        except Exception as ex:
            self.logger.error("Exception occurred: %s", str(ex))
            return True

        if self.all_included in file_decoded:
            return True

        if self.base_included["file"] not in file_decoded:
            self.missing_files.append(
                (
                    self.base_included["file"],
                    self.base_included["description"],
                )
            )

        for repo_type, repo_is_type in repo_types.items():
            if not repo_is_type:
                continue

            for file_check in self.file_checks:
                if repo_type in file_check["tags"]:
                    if not file_check["file"] in file_decoded:
                        self.missing_files.append(
                            (
                                file_check["file"],
                                file_check["description"],
                            )
                        )

        return len(self.missing_files) == 0

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Makefile Templates Usage"

    async def mitigation_strategy(self) -> str:
        message = "The repository is not using any of the CI/CD Makefile, please include the following templates: "
        if len(self.missing_files) != len(self.file_checks):
            message = (
                "The repository is only using a subset of the templates "
                "that're available. Please include the main template to "
                "cover the whole software lifecycle or contact "
                "#team-system-support if there's a reason you can't use it.\n"
                "Please include the following templates:\n"
            )
        missing_includes = "".join(
            f"- {file} (for {description})\n"
            for file, description in self.missing_files
        )
        return f"{message}" f"{missing_includes}"

    async def _get_file_decoded_from_repo(self, mr_event, proj_id, file_path):
        source_branch = mr_event.object_attributes.source_branch
        response = await self.api.get_file_from_repository(
            proj_id, file_path, source_branch
        )
        if response.get("message") == "404 File Not Found":
            raise FileNotFoundError("File not found: %s" % str(file_path))

        try:
            base64_bytes = response["content"].encode()
            message_bytes = base64.b64decode(base64_bytes)
            file_decoded = message_bytes.decode()
        except UnicodeDecodeError as e:
            raise UnicodeDecodeError(
                "Error decoding Unicode: %s" % str(e)
            ) from e
        except AttributeError as e:
            raise AttributeError("AttributeError: %s" % str(e)) from e
        except base64.binascii.Error as e:
            raise base64.binascii.Error(
                "Base64 decoding error: %s" % str(e)
            ) from e
        return file_decoded
