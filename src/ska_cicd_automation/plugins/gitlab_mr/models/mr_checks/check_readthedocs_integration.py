# flake8: noqa E501
import logging
import re

from ska_cicd_services_api.gitlab_api import GitLabApi
from ska_cicd_services_api.readthedocs_api import HTTPException, ReadTheDocsApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook
from ska_cicd_automation.plugins.gitlab_mr.models.repo_tree_agent import (
    RepoTreeAgent,
)

DEVPORTAL_SLUG = "developerskatelescopeorg"


class CheckReadthedocsIntegration(Check):
    feature_toggle = "check-readthedocs-integration"

    def __init__(self, api: GitLabApi, rtd_api: ReadTheDocsApi, logger_name):
        self.api = api
        self.rtd_api = rtd_api
        self.logger = logging.getLogger(logger_name)
        self.failing_checks = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        if proj_id == 9070656:
            return True

        repo_tree_agent = RepoTreeAgent(self.api, proj_id, mr_event)
        await repo_tree_agent.process_repo_tree()
        repo_types = repo_tree_agent.get_repo_types()

        self._check_docs_folder(repo_types)
        await self._check_project_on_rtd(mr_event)
        await self._check_rtd_import(mr_event, proj_id)

        return not self.failing_checks

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "ReadTheDocs Integration"

    async def mitigation_strategy(self) -> str:
        init_msg = (
            "Please integrate this project with Read the Docs following "
            "[the guidelines](https://developer.skatelescope.org/en/latest/tools/documentation.html#how-to-document)."  # NOQA: E501
        )
        docs_checks_mitigation = ""
        if self.failing_checks:
            docs_checks = "\n".join(
                f" - {check}" for check in self.failing_checks
            )
            docs_checks_mitigation = (
                f"\n\nChange these settings on your MR:\n{docs_checks}"
            )

        return f"{init_msg}{docs_checks_mitigation}"

    def _check_docs_folder(self, repo_types):
        if repo_types.get("docs") == False:
            self.failing_checks.append(
                "Please set up docs/ folder for sphinx documentation build,"
                " following [the guidelines].(https://developer.skatelescope.org/en/latest/tools/documentation.html#documentation-on-git)"  # NOQA: E501
            )

    async def _check_project_on_rtd(self, mr_event):
        slug_alias = mr_event.project.path_with_namespace
        slug_alias = slug_alias.split("/")[-1]

        try:
            await self.rtd_api.get_subproject_detail(
                DEVPORTAL_SLUG, slug_alias
            )
        except HTTPException as ex:
            self.logger.error("Project not Found!: %s", str(ex))
            self.failing_checks.append(
                "Please add this project as a subproject on Read the Docs "
                "following [the guidelines](https://developer.skatelescope.org/en/latest/tools/documentation.html#add-project-as-a-sub-project-on-readthedocs)"  # NOQA: E501
            )

    async def _check_rtd_import(self, mr_event, proj_id):
        slug_alias = mr_event.project.path_with_namespace
        slug_alias = slug_alias.split("/")[-1]

        # Check webhook
        slug = f"https://readthedocs.org/api/.+/webhook/ska-telescope-{slug_alias}"  # NOQA: E501
        reg = re.compile(slug)
        hook_data = await self.api.get_project_hooks(proj_id)

        have_hooks = False
        async for regis in hook_data:
            url = regis["url"]
            self.logger.debug("Checking Hook: %s", url)
            if bool(re.match(reg, url)):
                have_hooks = True
                break  # Break the for loop to deal with multiple hooks

        if not have_hooks:
            self.failing_checks.append(
                "Please "
                "[import](https://developer.skatelescope.org/en/latest/tools/documentation.html#import-project-to-readthedocs) "  # NOQA: E501
                "your project into Read the Docs"
            )
