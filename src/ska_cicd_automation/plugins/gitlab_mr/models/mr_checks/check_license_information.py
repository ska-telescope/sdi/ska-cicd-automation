# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

LICENSES = ["bsd-3-clause"]


class CheckLicenseInformation(Check):
    feature_toggle = "check-license-information"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        mr_license = await self.api.get_project_settings_info(
            proj_id, license=True
        )
        self.logger.debug("MR License: %s", mr_license)
        # License keys are extracted from here: https://choosealicense.com/appendix/ # NOQA: E501

        license = mr_license.get("license")

        if license is not None and license.get("key") in LICENSES:
            return True

        return False

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Non-compliant License Information"

    async def mitigation_strategy(self) -> str:
        return (
            "All projects should use a BSD-3 licence as per the "
            "[SKAO Licence Template](https://gitlab.com/ska-telescope/templates-repository/-/blob/master/LICENSE/skao_license.txt)."  # NOQA: E501
            "If you have a requirement to use a different licence please consult with the Architecture "  # NOQA: E501
            "Team before adding the licence. Please update the license information according to "  # NOQA: E501
            "[developer portal guidelines](https://developer.skatelescope.org/en/latest/getting-started/projects/licensing.html?highlight=license)"  # NOQA: E501
            "This check can only examine the default branch, so don't worry if this message appears on a merge "  # NOQA: E501
            "request that adds a compliant LICENSE file."
        )
