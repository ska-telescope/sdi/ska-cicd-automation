import logging
import re

from ska_cicd_services_api.gitlab_api import GitLabApi
from ska_cicd_services_api.jira_api import JiraApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook


class CheckJiraId(Check):
    feature_toggle = "check-jira-id"

    def __init__(
        self, gitlab_api: GitLabApi, jira_api: JiraApi, logger_name: str
    ):
        self.gitlab_api = gitlab_api
        self.jira_api = jira_api
        self.logger = logging.getLogger(logger_name)
        self.bad_commits = []
        self.jira_issues = []
        self.mitigation_messages = []
        self.warning = False

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        await self.jira_api.authenticate()

        branch_name_valid = await self.check_branch_name(mr_event)
        mr_commits_valid = await self.check_mr_commits(proj_id, mr_id)
        mr_title_valid = await self.check_mr_title(mr_event)
        ids_match = len(set(self.jira_issues)) == 1

        if not branch_name_valid:
            self.mitigation_messages.append(
                "• Branch name should start with a lowercase Jira"
                " ticket id (i.e. st-0000-id-checks)."
                " Please close this MR, rename your branch and"
                " create a new MR.\n"
            )

        if not mr_commits_valid:
            init_msg = (
                "• Following commit messages violate "
                "[the formatting standards](https://developer.skatelescope.org/en/latest/tools/git.html#committing-code)."  # NOQA: E501
            )
            commits = "".join(
                f" - At commit: {commit}\n" for commit in self.bad_commits
            )
            self.mitigation_messages.append(f"{init_msg}\n{commits}")

        if not mr_title_valid:
            self.mitigation_messages.append(
                "• Please change the MR title to include"
                " a valid Jira ticket id in uppercase "
                "(i.e. ST-0000: id-checks).\n"
            )

        if not ids_match:
            self.mitigation_messages.append(
                "• The Jira ticket ids used in your branch name, "
                "MR title or commits are different from each other."
                " Please make sure this is intended, "
                "otherwise change them to match.\n"
            )

        # Change the message type to warning if only the mismatched IDs check fails  # NOQA: E501
        if (
            branch_name_valid
            and mr_commits_valid
            and mr_title_valid
            and not ids_match
        ):
            self.warning = True

        return (
            mr_commits_valid
            and mr_commits_valid
            and mr_title_valid
            and ids_match
        )

    async def type(self) -> MessageType:
        if self.warning:
            return MessageType.WARNING

        return MessageType.FAILURE

    async def description(self) -> str:
        return "Jira ticket ID"

    async def mitigation_strategy(self) -> str:
        return "\n".join([message for message in self.mitigation_messages])

    async def check_branch_name(self, mr_event: MRHook):
        source_branch = mr_event.object_attributes.source_branch
        issue_key_match = re.search(r"^([a-z][a-z0-9_]+-)\d+", source_branch)

        if not issue_key_match:
            self.logger.debug(
                "Regular expression failed to get a Jira Issue"
                " in the MR Title string"
            )
            return False
        else:
            possible_issue_key = issue_key_match.group(0)
            self.logger.debug("Possible JIRA Key: %s", possible_issue_key)

        try:
            jira_issue = await self.jira_api.get_issue(possible_issue_key)
            self.logger.debug("Returned JIRA Issue: %s", jira_issue)
            self.jira_issues.append(jira_issue.key)
            return True
        except Exception as ex:
            self.logger.error("Issue does not exist!: %s", ex)

        return False

    async def check_mr_commits(self, proj_id: int, mr_id: int):
        mr_commits = await self.gitlab_api.get_merge_request_commits(
            proj_id, mr_id
        )

        async for commit in mr_commits:
            issue_key_match = re.search(
                r"(\b[A-Z][A-Z0-9_]+-[1-9][0-9]*)\b|^Merge\b|^Revert\b",
                commit["message"],
            )
            if issue_key_match is None:
                self.bad_commits.append(commit["short_id"])
                continue

            possible_issue_key = issue_key_match.group(0)
            if possible_issue_key == "Merge" or possible_issue_key == "Revert":
                continue

            try:
                jira_issue = await self.jira_api.get_issue(possible_issue_key)
                self.logger.debug(
                    "Commit-%s, Returned JIRA Key: %s",
                    commit["short_id"],
                    jira_issue,
                )
                self.jira_issues.append(jira_issue.key)
            except Exception as ex:
                self.logger.error("Issue does not exist!: %s", ex)
                self.bad_commits.append(commit["short_id"])

        return not self.bad_commits

    async def check_mr_title(self, mr_event: MRHook):
        mr_title = mr_event.object_attributes.title
        self.logger.debug("MR Title: %s", mr_title)

        issue_key_match = re.search(
            r"^(Draft:\s+)?(\[?(\w+-\d+)\]?:?.*)$", mr_title
        )

        if not issue_key_match:
            self.logger.error("Invalid MR title: %s", mr_title)
            return False

        issue_key = (
            issue_key_match.group(3)
            if issue_key_match.group(3)
            else "Merge operation"
        )

        self.logger.info("Extracted issue key or operation: %s", issue_key)

        if "Merge operation" in issue_key:
            self.logger.info(
                "No JIRA issue check needed for merge operations."
            )
            return True

        try:
            jira_issue = await self.jira_api.get_issue(issue_key)
            self.logger.debug("Returned issue object: %s", jira_issue)
            self.jira_issues.append(jira_issue.key)
            return True
        except Exception as ex:
            self.logger.debug("JIRA issue does not exist! %s", ex)

        return False
