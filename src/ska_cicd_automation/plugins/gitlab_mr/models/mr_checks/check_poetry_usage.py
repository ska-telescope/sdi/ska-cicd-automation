# flake8: noqa E501
import logging

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook
from ska_cicd_automation.plugins.gitlab_mr.models.repo_tree_agent import (
    RepoTreeAgent,
)


class CheckPoetryUsage(Check):
    feature_toggle = "check-poetry-usage"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.failing_deps_checks = []

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):
        repo_tree_agent = RepoTreeAgent(self.api, proj_id, mr_event)
        await repo_tree_agent.process_repo_tree()
        repo_types = repo_tree_agent.get_repo_types()

        if not repo_types["python"]:
            return True

        forbidden_files = {"requirements.txt", "setup.py"}
        exceptions = {
            ".binder/requirements.txt",  # STS-967
        }

        self.failing_deps_checks.append(
            "Please set up poetry as a dependency manager"
        )  # NOQA: E501

        for item in repo_tree_agent.repo_tree:
            item_message = item.get("message")
            item_name = item.get("name")
            item_path = item.get("path")

            if item_message == "404 File Not Found":
                self.logger.error("Error fetching repository tree.")
                return False

            if item_name in forbidden_files and item_path not in exceptions:
                self.failing_deps_checks.append(
                    f"Remove the {item_name} usage in {item_path}"
                )  # NOQA: E501

            if item_name == "pyproject.toml":
                self.failing_deps_checks.remove(
                    "Please set up poetry as a dependency manager"
                )  # NOQA: E501

        return not self.failing_deps_checks

    async def type(self) -> MessageType:
        return MessageType.WARNING

    async def description(self) -> str:
        return "Poetry Usage"

    async def mitigation_strategy(self) -> str:
        init_msg = "Please update the dependency management of this repository:"  # NOQA: E501
        poetry_checks_mitigation = ""
        if self.failing_deps_checks:
            poetry_checks_mitigation = poetry_checks_mitigation.join(
                f"\n - {check}" for check in self.failing_deps_checks
            )

        return f"{init_msg}{poetry_checks_mitigation}"
