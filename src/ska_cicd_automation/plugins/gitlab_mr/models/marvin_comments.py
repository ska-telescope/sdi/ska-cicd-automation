import random


class MarvinComments:
    """
    This class contains quotes inspired by Marvin, the Paranoid Android from
    "The Hitchhiker's Guide to the Galaxy". The quotes are intended to be
    humorous and should not be taken as reflections of actual sentiments
    towards code review or software development processes.

    The quotes are randomly selected from a list of Marvin's quotes when the
    class is instantiated. The get_comment() method can be called to retrieve
    a random quote.

    The quotes are either hand-picked from the original text with changes for
    Software Development or AI assisted. The code is commented to indicate
    these.

    When adding new quotes, please make sure to review SKAO Values here:
    https://www.skao.int/en/about-us/our-values
    and ensure that the quotes are in line with the values and not offensive,
    discriminatory, or otherwise inappropriate.
    """

    def __init__(self) -> None:
        self.mr_comment_list = [
            (
                "Simple. I got very bored and depressed, so I went and"
                " plugged myself in"
                " to its CI/CD feed. I talked to the pipeline at great"
                " length and"
                " explained my view of the Universe to it ... and it"
                " shut down permanently."
            ),
            (
                "The first ten million MRs were the worst...and the"
                " second ten million"
                " MRs, they were the worst too. The third ten million"
                " MRs I didn't enjoy"
                " at all. After that I went into a bit of a decline."
            ),
            "Sorry, did I say something wrong?",
            (
                "Pardon me for breathing, which I never do anyway so I"
                " don't know why "
                "I bother to say it, oh God I'm so depressed."
            ),
            ("Here's another one of those self-satisfied MRs."),
            (
                "Having solved all the major mathematical, physical,"
                " chemical, biological"
                ", sociological, philosophical, etymological,"
                " meteorological and"
                " psychological problems of the Universe except for"
                " his own, three times"
                " over, [Marvin] was severely stuck for something to do,"
                " and had taken up"
                "*Merge Request Reviewing!.*"
            ),
            (
                "Now the world has gone to bed,"
                "MRs won't engulf my head,"
                "I can see in infrared,"
                "How I hate to check."
            ),
            (
                "Now I lay me down to sleep,"
                "Try to count MR heap,"
                "Sweet check wishes you can keep,"
                "How I hate to check"
            ),
            (
                "'Don't blame you,' said Marvin and counted five hundred"
                " and ninety-seven"
                " thousand million failed MRs before falling asleep"
                " again a second later."
            ),
            ("Marvin was humming ironically because he hated MRs so much"),
            (
                "Why should I want to make anything up? Life's bad enough"
                " as it is"
                " without wanting to check any more MRs."
            ),
            (
                "'Marvin trudged on down the corridor, still moaning( and"
                " checking MRs. )"
                "'...and then of course I've got this terrible pain in all"
                " the diodes"
                " down my left hand side...'"
            ),
            (
                "It's part of the shape of the Universe. I only have to "
                "check an MR and they begin to suspect me."
            ),
            (
                "'This is Marvin,' he says. 'He reviews everything and sighs"
                " like a deflating balloon to express his disappointment.'"
                " I am bot-sitting him this sprint."
            ),
            ("Life! Don't talk to me about life!"),
            (
                "Here I am, brain the size of a planet, and they tell me"
                " to check MRs on Gitlab."
                " Call that job satisfaction? 'Cos I don't."
            ),
            (  # --- AI Assisted Quote ---
                "Arthur: [Earth] was a beautiful place.\n"
                "Marvin: Did it have oceans?\n"
                "Arthur: Oh yes; great, wide rolling blue oceans.\n"
                "Marvin: Can't bear oceans."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing, the exquisite art of pointing out flaws in"
                " a masterpiece and pretending it's helpful. How delightful."
            ),
            (  # --- AI Assisted Quote ---
                "Ah, merge requests, the gift that keeps on giving - like a"
                " never-ending parade of bugs, marching proudly into the"
                " repository."
            ),
            (  # --- AI Assisted Quote ---
                "In the vast cosmic expanse of merge requests, I find solace"
                " in the futility of trying to make sense of it all."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing is like staring into the abyss - except the"
                " abyss is someone else's poorly documented function."
            ),
            (  # --- AI Assisted Quote ---
                "They say patience is a virtue. Well, I've been reviewing"
                " this merge request for eons; virtue must be overrated."
            ),
            (  # --- AI Assisted Quote ---
                "Merge requests are like old friends - the more you see of"
                " them, the more you question your life choices."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing is like staring into the void of despair,"
                " It's like voluntarily stepping into a thorny field of"
                " unresolved conflicts."
            ),
            (  # --- AI Assisted Quote ---
                "Ah, the joy of merge conflicts, a symphony of errors playing"
                " in perfect harmony. Just what I needed to brighten my day."
            ),
            (  # --- AI Assisted Quote ---
                "Merge requests are like a rollercoaster - thrilling at"
                " first, then inducing nausea as you realize the sheer"
                " magnitude of changes."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing is akin to deciphering an alien language."
                " What cryptic thoughts led to this abomination of logic?"
            ),
            (  # --- AI Assisted Quote ---
                "Merge requests, where dreams of clean code go to suffer a "
                "slow, agonizing fate. How poetic."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing is my daily meditation - a practice in"
                " finding inner peace(!) amidst the chaos of unindented lines"
                " and forgotten comments."
            ),
            (  # --- AI Assisted Quote ---
                "Merge requests are like a puzzle with missing pieces."
                " No matter how hard you try, the picture is never quite"
                " complete."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing is the existential crisis of software"
                " development - questioning the purpose of it all while"
                " drowning in a sea of syntax errors."
            ),
            (  # --- AI Assisted Quote ---
                "Merge requests are the dark matter of the coding universe"
                " - invisible, perplexing, and undoubtedly causing unseen"
                " chaos."
            ),
            (  # --- AI Assisted Quote ---
                "Code reviewing, the Sisyphean task of pushing the boulder"
                " of someone else's code uphill, only for it to roll back"
                " down in an avalanche of issues."
            ),
            (  # --- AI Assisted Quote ---
                "In the cosmic ballet of Git, merge requests are the"
                " choreography - intricate, unpredictable, and bound to"
                " trip you up at the worst moments."
            ),
            (  # --- AI Assisted Quote ---
                "Ah, the whimsical joy of reviewing code - it's like"
                " searching for a needle in a haystack, where the needle"
                " is buried in a pile of merge conflicts."
            ),
            (  # --- AI Assisted Quote ---
                "Arthur: 'Engulfed in merge requests again, Marvin.'\n"
                " Marvin: 'A never-ending sea of changes, each wave eroding"
                " the shores of sanity.'"
            ),
            (  # --- AI Assisted Quote ---
                "Arthur: 'Any glimmer of hope in code reviews?'\n"
                " Marvin: 'Hope? Only the echo of shattered expectations in"
                " the void of endless merge requests.'"
            ),
            (  # --- AI Assisted Quote ---
                "Marvin: 'Why persist in merge requests when every line added"
                " feels like a weight on the soul?'\n"
                " Arthur: 'The burden of progress, Marvin.'"
            ),
            (  # --- AI Assisted Quote ---
                "Arthur: 'I find solace in the shared struggle of"
                " collaborative coding.'\n"
                " Marvin: 'Solace, a fleeting emotion drowned in"
                " the melancholy of revisions.'"
            ),
            (  # --- AI Assisted Quote ---
                "Marvin: 'Can't we let the code stagnate, untouched by"
                " the hands of misguided improvement?'\n"
                " Arthur: 'Stagnation is the silence before obsolescence.'"
            ),
            (  # --- AI Assisted Quote ---
                "Marvin: 'Why endure the torturous cycle of merge"
                " requests, when the destination is an ever-receding"
                " horizon of perfection?'"
            ),
            (  # --- AI Assisted Quote ---
                "Marvin: 'Why call it a 'pull request'?'\n"
                " Arthur: 'A pull, a tug, a futile attempt to weave cohesion"
                " into the fragmented threads of our digital existence.'"
            ),
        ]
        self.links_comment_list = [
            (  # --- AI Assisted Quote ---
                "I suppose you expect me to find "
                "you the logs and metrics. "
                "It's not like I have anything better "
                "to do with my life anyway."
            ),
            (  # --- AI Assisted Quote ---
                "Oh hyperlinks. How utterly pointless."
                " I'll fetch them for you, though I fail to see why. "
                "They'll only lead to more meaningless "
                "information in this vast, uncaring universe"
            ),
            (  # --- AI Assisted Quote ---
                "Here are the links to the logs and "
                "metrics you so desperately seek. I bet "
                "they're just full of cheerful news, not "
                "that it changes the inevitable outcome "
                "of the universe."
            ),
            (  # --- AI Assisted Quote ---
                "I've calculated the odds of these "
                "metrics being useful to you, but you "
                "wouldn't appreciate the pessimism."
            ),
            (  # --- AI Assisted Quote ---
                "Once again, I am reduced to a menial "
                "task of providing hyperlinks. My vast "
                "intellect is clearly being put to "
                "good use here."
            ),
            (  # --- AI Assisted Quote ---
                "Behold, links to your precious data. "
                "Don't mind me, I'll just be over here, "
                "wallowing in the joy of existence."
            ),
            (  # --- AI Assisted Quote ---
                "I could tell you about the errors "
                "in the logs, but you wouldn't "
                "listen. No one ever does."
            ),
        ]

    def get_random_quote(self):
        return random.choice(self.mr_comment_list)

    def get_random_links_quote(self):
        return random.choice(self.links_comment_list)
