# Rules folder
The rules folder contains placeholders for the rules used in the gitlab_mr plugin
In a deployment scenario, they are mounted through a configmap and are described in the values file.
The files must exist in this folder for local tests to run.