# flake8: noqa E501
import logging
from io import BytesIO
from zipfile import ZipFile

from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import PipelineHook


class CheckTestCoverage(Check):
    feature_toggle = "check-test-coverage"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: PipelineHook, proj_id: int, mr_id=int):

        ci_metrics_job_exists_and_passed = False
        test_coverage_exists = False

        for job in mr_event.builds:
            if job.stage == ".post" and job.name == "create-ci-metrics":
                if job.status == "success":
                    ci_metrics_job_id = job.id
                    ci_metrics_job_exists_and_passed = True
                break

        if ci_metrics_job_exists_and_passed:
            artefacts_bytes = await self.api.get_job_artifacts(
                proj_id, ci_metrics_job_id
            )
            artefacts_stream = BytesIO(artefacts_bytes)
            with ZipFile(artefacts_stream, "r") as zip:
                for file in zip.namelist():
                    if "build/reports/code-coverage.xml" in file:
                        test_coverage_exists = True

        return not (
            ci_metrics_job_exists_and_passed and not test_coverage_exists
        )

    async def type(self) -> MessageType:
        return MessageType.WARNING

    @staticmethod
    async def description() -> str:
        return "Missing Test Coverage"

    async def mitigation_strategy(self) -> str:
        return (
            "This Project is missing test coverage \n"
            "Please have a look at the following [page]"
            "(https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html?highlight=coverage#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline)"  # NOQA: E501
        )
