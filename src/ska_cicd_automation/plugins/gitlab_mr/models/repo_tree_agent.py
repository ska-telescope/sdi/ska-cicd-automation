# flake8: noqa E501
import logging
import os
import re
from enum import Enum

import yaml
from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

REPO_RULES_FILE_PATH = os.path.join(
    os.path.dirname(__file__), "rules", "repository_rules.yaml"
)

logger = logging.getLogger("gunicorn.error")


class RepoType(Enum):
    PYTHON = "python"
    CPP = "cpp"
    HELM = "helm"
    OCI = "oci"
    RAW = "raw"
    ANSIBLE = "ansible"
    DOCS = "docs"
    NOTEBOOK = "notebook"


class RepositoryReport:
    is_repo: bool = False
    is_compliant: bool = False
    passed_compliance_rules: list[str] = []
    passed_inspection_rules: list[str] = []
    failed_inspection_rules: list[str] = []
    failed_compliance_rules: list[str] = []


class Rule:
    class FileType(Enum):
        BLOB = "blob"
        TREE = "tree"

    def __init__(
        self,
        description: str,
        file_type: FileType,
        rule: str,
        rule_flags: int = 0,
    ):
        self.description: str = description
        self.file_type: Rule.FileType = file_type
        self.rule: str = rule
        self.rule_flags: int = rule_flags

    @classmethod
    def create_rule(cls, rule_data: dict):
        description = rule_data.get("description")
        regex = rule_data.get("regex")
        file_type = cls.FileType(rule_data.get("file_type"))
        return cls(description, file_type, regex)

    def process(self, repo_tree: list[dict], project_slug: str) -> bool:
        rule_results = []
        rule = self.rule.format(project_slug=project_slug)
        for item in repo_tree:
            if (
                re.search(rule, item["path"], self.rule_flags)
            ) and self.file_type.value == item["type"]:
                rule_results.append(True)
            else:
                rule_results.append(False)

        return any(rule_results)


class RuleSet:
    class Operator(Enum):
        OR = "or"
        AND = "and"
        NOR = "nor"
        NAND = "nand"

        @staticmethod
        def calculate(operator: "RuleSet.Operator", list: list):
            if operator == RuleSet.Operator.OR:
                logic_result = any(list)
            elif operator == RuleSet.Operator.AND:
                logic_result = all(list)
            elif operator == RuleSet.Operator.NOR:
                logic_result = not any(list)
            elif operator == RuleSet.Operator.NAND:
                logic_result = not all(list)
            return logic_result

    def __init__(self, op: Operator, name: str):
        self.description: str = name
        self.op: RuleSet.Operator = op
        self.rules: list[Rule] = []
        self.results: list[bool] = []
        self.passed_rules: list[str] = []
        self.failed_rules: list[str] = []

    @classmethod
    def create_rule_set(cls, ruleset_data: dict):
        operator = cls.Operator(ruleset_data.get("op"))
        description = ruleset_data.get("rule_name")
        sub_rules_data = ruleset_data.get("rules")
        rule_set = cls(operator, description)
        for rule_data in sub_rules_data:
            if "rules" in rule_data:
                rule_set.rules.append(cls.create_rule_set(rule_data))
            else:
                rule_set.rules.append(Rule.create_rule(rule_data))
        return rule_set

    def process(self, repo_tree: list[dict], project_slug):
        for rule in self.rules:
            result = rule.process(repo_tree, project_slug)
            self.results.append(result)

        ruleset_result = RuleSet.Operator.calculate(self.op, self.results)

        for rule, result in zip(self.rules, self.results):
            if not result:
                self.failed_rules.extend(
                    rule.failed_rules
                    if isinstance(rule, RuleSet)
                    else [rule.description]
                )
            else:
                self.passed_rules.extend(
                    rule.passed_rules
                    if isinstance(rule, RuleSet)
                    else [rule.description]
                )

        if ruleset_result:
            self.failed_rules.clear()

        return ruleset_result


class RepoTreeItemInspector:
    def __init__(
        self, mr_hook: MRHook, repo_reports: dict[RepoType, RepositoryReport]
    ) -> None:
        self.mr_hook: MRHook = mr_hook
        self.reports: dict[RepoType, RepositoryReport] = repo_reports
        self.inspection_rules: dict[
            RepoType, RuleSet
        ] = self._read_rules_from_yaml("inspection_rules")
        self.compliance_rules: dict[
            RepoType, RuleSet
        ] = self._read_rules_from_yaml("compliance_rules")

    def _read_rules_from_yaml(self, rule_type):
        rules = {}
        with open(REPO_RULES_FILE_PATH, "r", encoding="utf-8") as f:
            file_content = f.read()
            rules_data = yaml.safe_load(file_content)
            rules_content = rules_data.get(rule_type, [])

            for rule in rules_content:
                repo_type = RepoType(rule.get("repo_type"))
                rules[repo_type] = self._read_nested_rules(rule)

        return rules

    def _read_nested_rules(self, main_rule):
        operator = RuleSet.Operator(main_rule.get("op"))
        main_rule_description = main_rule.get("rule_name")
        main_ruleset = RuleSet(operator, main_rule_description)
        rules = main_rule.get("rules", [])

        for rule_data in rules:
            if rule_data.get("rules") is not None:
                main_ruleset.rules.append(RuleSet.create_rule_set(rule_data))
            else:
                main_ruleset.rules.append(Rule.create_rule(rule_data))

        return main_ruleset

    def _process_ruleset(
        self, repo_tree: list[dict], rule_set: RuleSet, project_slug: str
    ) -> bool:
        """
        Args:
            tree_item (dict): current tree item to be processed
            rule_set: (RuleSet): RuleSet with the operation and rules
            passed_rules (set[str]): set to save the results of satisfied rules
            failed_rules (set[str]): set to save the description of rules
        """
        result = rule_set.process(repo_tree, project_slug)
        passed_rules = rule_set.passed_rules
        failed_rules = rule_set.failed_rules

        return result, passed_rules, failed_rules

    def process_rules(self, repo_tree: list[dict], project_slug: str) -> None:

        for repo_type, inspection_rules in self.inspection_rules.items():
            # NOTE: There's probably a better way to account for these edge cases
            if repo_type in ["raw", "oci", "helm"]:
                project_slug = project_slug.replace("-", "_")
            (
                self.reports[repo_type].is_repo,
                self.reports[repo_type].passed_inspection_rules,
                self.reports[repo_type].failed_inspection_rules,
            ) = self._process_ruleset(
                repo_tree, inspection_rules, project_slug
            )

        for repo_type, compliance_rules in self.compliance_rules.items():
            if repo_type in ["raw", "oci", "helm"]:
                project_slug = project_slug.replace("-", "_")
            (
                self.reports[repo_type].is_compliant,
                self.reports[repo_type].passed_compliance_rules,
                self.reports[repo_type].failed_compliance_rules,
            ) = self._process_ruleset(
                repo_tree, compliance_rules, project_slug
            )


class RepoTreeAgent:
    """Process repository structure based on SKAO guidelines"""

    def __init__(
        self, gitlab_api: GitLabApi, proj_id: int, mr_hook: MRHook
    ) -> None:
        self._gitlab_api = gitlab_api
        self._proj_id = proj_id
        self._mr_hook = mr_hook
        self.repo_tree = None
        self.reports = {
            repo_type: RepositoryReport() for repo_type in RepoType
        }
        self.inspector = RepoTreeItemInspector(mr_hook, self.reports)
        self.is_processed = False

    async def _init_repo_tree(self):
        repo_tree = await self._gitlab_api.get_repo_tree(
            proj_id=self._proj_id,
            ref=self._mr_hook.object_attributes.source_branch,
            recursive=True,
        )
        self.repo_tree = [tree_item async for tree_item in repo_tree]

    def get_repo_types(self) -> dict[str, bool]:
        """Returns whether the repository is publishing artefacts
        for every supported artefact type

        i.e.
        {
            "python": True,
            "cpp": False,
            ...
        }

        Returns:
            dict[str, bool]: boolean values for the keys for artefact types
        """
        return {
            repo_type.value: report.is_repo
            for repo_type, report in self.reports.items()
        }

    def get_repo_compliances(self) -> dict[str, bool]:
        """Returns whether the repository structure is compliant
        for every supported artefact type

        i.e.
        {
            "python": True,
            "cpp": False,
            ...
        }

        Returns:
            dict[str, bool]: boolean values for the keys for artefact types
        """
        return {
            repo_type.value: report.is_compliant
            for repo_type, report in self.reports.items()
        }

    async def process_repo_tree(self) -> None:

        if self.is_processed:
            return

        all_compliances = [
            report.is_compliant for report in self.inspector.reports.values()
        ]
        if all(all_compliances):
            return

        # Get the repo tree from the API if we don't have it
        if not self.repo_tree:
            await self._init_repo_tree()

        project_slug = self._mr_hook.project.path_with_namespace.split("/")[-1]

        self.inspector.process_rules(self.repo_tree, project_slug)

        self.is_processed = True
