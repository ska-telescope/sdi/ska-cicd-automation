# flake8: noqa E501
from abc import ABC, abstractmethod

from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook


class Check(ABC):

    feature_toggle: str = NotImplemented

    @abstractmethod
    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int) -> bool:
        pass

    @abstractmethod
    async def type(self) -> MessageType:
        pass

    @abstractmethod
    async def description(self) -> str:
        pass

    @abstractmethod
    async def mitigation_strategy(self) -> str:
        pass


class CheckResult:
    def __init__(
        self, name: str, result: bool, type: MessageType, runtime: float
    ) -> None:
        self.name = name
        # block_mr is True if the check is failure type and the result is False
        self.block_mr: bool = (
            True if type == MessageType.FAILURE else False
        ) and not result
        self.result: bool = result
        self.runtime: float = runtime
