# flake8: noqa E501
import logging
from enum import Enum

import prettytable
from ska_cicd_services_api.gitlab_api import GitLabApi

from ska_cicd_automation.plugins.gitlab_mr.models.marvin_comments import (
    MarvinComments,
)


class ChangeType(Enum):
    SUCCESS = ":white_check_mark:"
    FAILURE = ":x:"

    def order(self):
        order = {"FAILURE": 2, "SUCCESS": 1}
        return order[self.name]


def get_change_type(enum_value: str):
    for enum in ChangeType:
        if enum.value == enum_value:
            return enum

    return None


class FixCommentGenerator:
    def __init__(self, logger_name, api: GitLabApi):
        self.api = api
        self.logger = logging.getLogger(logger_name)
        self.pretext = (
            "I fixed some settings, please review the following table and "
            "consult [the developer portal](https://developer.skatelescope.org/en/latest/tools/git.html#merge-request-quality-checks) "  # NOQA: E501
            "for further information:"
        )
        self.table = prettytable.PrettyTable()
        self.table.align = "c"
        self.table.field_names = ["Type", "Description", "Change"]
        self.posttext = f'*"{MarvinComments().get_random_quote()}"*'

    async def add_entry(
        self, type: ChangeType, description, outcome_success, outcome_failure
    ):
        if type == ChangeType.SUCCESS:
            self.table.add_row([type.value, description, outcome_success])
        else:
            self.table.add_row([type.value, description, outcome_failure])

    async def get_message(self):
        return "\n\n".join(
            [
                self.pretext,
                self.table.get_html_string(
                    sort_key=lambda x: get_change_type(x[0]).order()
                    if get_change_type(x[0]) is not None
                    else x,
                    sortby="Type",
                    reversesort=True,
                ),
                self.posttext,
            ]
        )

    async def execute(self, proj_id, mr_id):
        if len(self.table.rows) > 0:
            self.logger.info("Adding a new comment...")
            result = await self.api.add_comment(
                proj_id, mr_id, await self.get_message()
            )
            self.logger.debug("Command result: %s", result)
            return result
