import html
import json
import logging

import prettytable


class LinksMessageGenerator:
    message_marker = "ska-devsecops-mr-links-id"

    def __init__(self, logger_name):
        self.logger = logging.getLogger(logger_name)
        self.pretext = ""
        self.content = ""
        self.posttext = ""
        self.links = {}

    async def generate_metadata(self) -> str:
        """
        Serializes the links dictionary to JSON and wraps it in HTML comments.

        Returns:
            str: A string containing HTML comments with metadata information.
        """
        dict_json = json.dumps(self.links)

        metadata = [
            "<!--",
            f"MRServiceID: {LinksMessageGenerator.message_marker}",
            f"links: {dict_json}",
            "-->",
        ]
        return "\n".join(metadata)

    async def add_pretext(self, pretext) -> None:
        self.pretext = pretext

    async def add_posttext(self, posttext) -> None:
        self.posttext = posttext

    async def get_message(self) -> None:
        """
        Combines metadata, pretext, content, and posttext.

        Returns:
            str: The complete message assembled from the different parts.
        """
        metadata = await self.generate_metadata()
        return "\n\n".join(
            [
                metadata,
                self.pretext,
                self.content,
                self.posttext,
            ]
        )

    async def add_links_template(self, urls: dict) -> None:
        """
        Generates a HTML segment containing links organized in a table format.

        Args:
            urls (dict): A dictionary containing the URLs for the hyperlinks.
        """
        (
            ds_links,
            logs_links,
            metrics_links,
        ) = await self.generate_links(urls)

        ds_entry = await self.generate_list(ds_links)
        logs_entry = await self.generate_list(logs_links)
        metrics_entry = await self.generate_list(metrics_links)

        namespace_head = await self.generate_namespace_header(
            urls["deployment_type"], urls["kube_namespace"], urls["cluster"]
        )

        table = prettytable.PrettyTable()
        table.align = "c"
        table.field_names = [
            "Headlamp Dashboards",
            "Kibana Logs",
            "Grafana Metrics",
        ]
        table.add_row([ds_entry, logs_entry, metrics_entry])
        table_html = html.unescape(table.get_html_string())

        timestamp_str = (
            f"<em>This deployment happened at {urls['timestamp']}.</em>"
        )
        self.content += "\n\n".join(
            [
                "<details>",
                namespace_head,
                table_html,
                timestamp_str,
                "</details>",
            ]
        )

    async def generate_namespace_header(
        self, deployment_type: str, namespace_name: str, cluster: str
    ) -> str:
        return " | ".join(
            [
                f"<summary>:label: <b>Deployment type:</b> {deployment_type}",
                f"<b>Namespace:</b> {namespace_name}",
                f"<b>Cluster:</b> {cluster}</summary>",
            ]
        )

    async def generate_links(
        self,
        urls: dict,
    ) -> tuple[list[str], list[str], list[str]]:
        """
        Generates lists of HTML strings based on provided URLs.

        Args:
            urls (dict): Dictionary containing various URL endpoints.

        Returns:
            tuple: Lists of HTML strings for dashboard, log and metric links
        """
        # Be sure to use f'' instead of f"" for correct templating

        ds_links = [
            f'<a href="{urls["namespace_ds"]}">Namespace</a>',
            f'<a href="{urls["deviceserver_ds"]}">Device Server</a>',
            f'<a href="{urls["database_ds"]}">DatabaseDS</a>',
        ]

        logs_links = [
            f'<a href="{urls["job_logs"]}">Job</a>',
            f'<a href="{urls["testpod_logs"]}">Test Pod</a>',
            f'<a href="{urls["namespace_logs"]}">Namespace</a>',
            f'<a href="{urls["deviceconf_logs"]}">Device Configuration Logs/Operator</a>',  # noqa: E501
            f'<a href="{urls["deviceserver_logs"]}">DeviceServer</a>',
            f'<a href="{urls["databaseds_logs"]}">DatabaseDS</a>',
        ]

        metrics_links = [
            f'<a href="{urls["namespace_metrics"]}">Namespace Summary</a>',
            f'<a href="{urls["deviceserver_metrics"]}">DeviceServer</a>',
            f'<a href="{urls["workload_metrics"]}">Compute Resources by Workload</a>',  # noqa: E501
            f'<a href="{urls["pod_metrics"]}">Compute resources by Pod</a>',
        ]

        return (ds_links, logs_links, metrics_links)

    async def generate_list(self, items: list[str]) -> str:
        return "\n".join(f"- {item}" for item in items)
