import logging
import os

from prometheus_client import (
    Counter,
    Summary,
    generate_latest,
    write_to_textfile,
)
from prometheus_client.core import CollectorRegistry

from ska_cicd_automation.models.utils.feature_toggle import FeatureToggler
from ska_cicd_automation.models.utils.singleton import SingletonClass

logger = logging.getLogger(__name__)

METRICS_FILE = os.getenv("MR_METRICS_REGISTRY_PATH")


class MRMetrics(metaclass=SingletonClass):
    """
    A class to manage and expose metrics for merge requests and checks.

    This class is designed to track various metrics related to merge requests
    (MRs) and their associated checks using Prometheus. It ensures that only
    one instance of the class is created (Singleton pattern), and initializes
    Prometheus metrics for counters and summaries. It provides methods to
    increment counters and record runtimes, conditionally enabled based on
    feature toggles.

    Attributes:
        registry (CollectorRegistry): A Prometheus registry to collect metrics
        mr_pass_counter (Counter): Counts the number of MRs that have passed
        mr_fail_counter (Counter): Counts the number of MRs that have failed
        check_pass_counter (Counter): Counts the number of passed checks
        check_fail_counter (Counter): Counts the number of failed checks
        check_exception_counter (Counter): Counts the number of exceptions
        during checks.
        mr_runtime_summary (Summary): Tracks the runtime of MRs
        check_runtime_summary (Summary): Tracks the runtime of checks

    Methods:
        create_counter(name, description, labels): Creates a Prometheus counter
        create_summary(name, description, labels): Creates a Prometheus summary
        init_counters(): Initializes all counters
        init_summaries(): Initializes all summaries
        get_metrics(): Returns the latest metrics in Prometheus format
        increment_counter(counter, **labels): Increments the specified counter
        record_runtime(summary, runtime, **labels): Records the runtime in the
        specified summary.
    """

    __allow_reinitialization = False

    def __init__(self):
        self.feature_toggle = FeatureToggler()
        self.registry = CollectorRegistry()
        self.init_counters()
        self.init_summaries()
        self.load_metrics()
        self.metrics = None
        self.mr_metrics_toggle = "mr-metrics"

    def create_counter(self, name, description, labels):
        return Counter(name, description, labels, registry=self.registry)

    def create_summary(self, name, description, labels):
        return Summary(
            name,
            description,
            labels,
            registry=self.registry,
        )

    def init_counters(self):
        self.mr_pass_counter = self.create_counter(
            "mr_pass_count",
            "Number of merge requests passed with proj_id and mr_id label",
            ["proj_id", "mr_id"],
        )
        self.mr_fail_counter = self.create_counter(
            "mr_fail_count",
            "Number of merge requests failed with proj_id  and mr_id label",
            ["proj_id", "mr_id"],
        )
        self.check_pass_counter = self.create_counter(
            "check_pass_count",
            "Number of successfull check with check name label",
            ["check_name", "proj_id"],
        )
        self.check_fail_counter = self.create_counter(
            "check_fail_count",
            "Number of check failures with check name label",
            ["check_name", "proj_id"],
        )
        self.check_exception_counter = self.create_counter(
            "check_exception_count",
            "Number of exceptions during checks",
            ["check_name", "exception_type", "proj_id", "mr_id"],
        )

    def init_summaries(self):
        self.mr_runtime_summary = self.create_summary(
            "mr_runtime_seconds",
            "Time required to run the MR.",
            ["proj_id", "mr_id"],
        )

        self.check_runtime_summary = self.create_summary(
            "check_runtime_seconds",
            "Time required to run a check.",
            ["check_name", "proj_id"],
        )

    def get_metrics(self):
        return generate_latest(self.registry)

    def increment_counter(self, counter, **labels):
        if self.is_feature_enabled():
            counter.labels(**labels).inc()

    def record_runtime(self, summary, runtime, **labels):
        if self.is_feature_enabled():
            summary.labels(**labels).observe(runtime)

    def is_feature_enabled(self):
        return self.feature_toggle.unleash_client.is_enabled(
            self.mr_metrics_toggle,
            fallback_function=lambda feature_name, context: True,
        )

    def scheduled_metrics_update(self):
        self.save_metrics()
        self.metrics = self.get_metrics()

    def save_metrics(self):
        os.makedirs(os.path.dirname(METRICS_FILE), exist_ok=True)
        logger.debug("Saving metrics to: %s", METRICS_FILE)
        write_to_textfile(METRICS_FILE, self.registry)

    def load_metrics(self):
        if os.path.exists(METRICS_FILE):
            with open(METRICS_FILE, "r", encoding="utf-8") as f:
                for line in f:
                    # Skip comment lines and metadata lines
                    if line.startswith("#"):
                        continue
                    # Remove leading/trailing whitespaces
                    line = line.strip()
                    # Process only lines with exactly one space
                    if " " in line:
                        parts = line.split(" ")
                    else:
                        logger.warning(
                            "Skipping line without space separator: %s", line
                        )
                        continue
                    # Ensure there are exactly two parts
                    if len(parts) != 2:
                        logger.warning("Skipping malformed line: %s", line)
                        continue
                    metric, value = parts
                    try:
                        value = float(value)
                    except ValueError:
                        logger.warning(
                            "Invalid value for metric %s: %s", metric, value
                        )
                        continue
                    # Check if metric has labels
                    if "{" in metric:
                        name, labels = metric.split("{")
                        labels = labels.rstrip("}")
                        try:
                            label_dict = dict(
                                item.split("=") for item in labels.split(",")
                            )
                            label_dict = {
                                k: v.strip('"') for k, v in label_dict.items()
                            }
                        except ValueError:
                            logger.warning(
                                "Malformed labels for metric: %s", metric
                            )
                            continue
                        self.update_metric(name, label_dict, value)

    def update_metric(self, name, labels, value):
        try:
            if name == "mr_pass_count_total":
                self.mr_pass_counter.labels(**labels).inc(value)
            elif name == "mr_fail_count_total":
                self.mr_fail_counter.labels(**labels).inc(value)
            elif name == "check_pass_count_total":
                self.check_pass_counter.labels(**labels).inc(value)
            elif name == "check_fail_count_total":
                self.check_fail_counter.labels(**labels).inc(value)
            elif name == "check_exception_count_total":
                self.check_exception_counter.labels(**labels).inc(value)
            elif name == "mr_runtime_seconds_sum":
                self.mr_runtime_summary.labels(**labels)._sum.set(value)
            elif name == "mr_runtime_seconds_count":
                self.mr_runtime_summary.labels(**labels)._count.set(value)
            elif name == "check_runtime_seconds_sum":
                self.check_runtime_summary.labels(**labels)._sum.set(value)
            elif name == "check_runtime_seconds_count":
                self.check_runtime_summary.labels(**labels)._count.set(value)
            else:
                logger.info("Unknown metric name: %s", name)
        except Exception as e:
            logger.error("Error updating metric %s: %s", name, e)
