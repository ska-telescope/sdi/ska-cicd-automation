"""
Module for managing GitLab Merge Request web hooks.
"""
import asyncio
import importlib
import inspect
import json
import logging
import os
import re
import time
from typing import Dict, List, Optional, Type, Union

import aiohttp
import cachetools
import yaml
from fastapi import APIRouter, BackgroundTasks, HTTPException, Request, status
from fastapi.responses import JSONResponse
from prometheus_client.exposition import CONTENT_TYPE_LATEST
from ska_cicd_services_api.gitlab_api import GitLabApi
from ska_cicd_services_api.jira_api import JiraApi
from ska_cicd_services_api.readthedocs_api import ReadTheDocsApi
from starlette.responses import Response

from ska_cicd_automation.models.utils.feature_toggle import FeatureToggler
from ska_cicd_automation.plugins.gitlab_mr.apis.nexus import NexusApi
from ska_cicd_automation.plugins.gitlab_mr.models.check import (
    Check,
    CheckResult,
)
from ska_cicd_automation.plugins.gitlab_mr.models.comment_executor import (
    CommentExecutor,
)
from ska_cicd_automation.plugins.gitlab_mr.models.links_message_generator import (  # noqa: E501
    LinksMessageGenerator,
)
from ska_cicd_automation.plugins.gitlab_mr.models.marvin_comments import (
    MarvinComments,
)
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import (
    MessageGenerator,
    MessageType,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import (
    JobURLs,
    MRHook,
    PipelineHook,
)
from ska_cicd_automation.plugins.gitlab_mr.models.mrmetrics import MRMetrics

logger = logging.getLogger("uvicorn.error")

router = APIRouter()

cache = cachetools.LRUCache(maxsize=512)

project_tags_blocklist = ["external"]

# "Dashboards and Logs" comment is a shared resource with persistent data.
# use a lock to access it to keep state safe.
comment_lock = asyncio.Lock()

# Add more API classes as needed
# Be sure to add them to process_checks as well
api_classes = [JiraApi, GitLabApi, ReadTheDocsApi, NexusApi]

MARVIN_USERNAME = "marvin-42"
MR_APPROVAL_FEATURE_TOGGLE = "marvin_approval"
MARVIN_STATUS_CHECK = "Marvin"
STATUS_CHECK_FEATURE_TOGGLE = "status-checks"

CHECK_TYPES = ["mr_checks", "pipeline_checks"]
MODELS_FOLDER = "ska_cicd_automation/plugins/gitlab_mr/models/"
MODULE_PATH = "ska_cicd_automation.plugins.gitlab_mr.models.{}.{}"
CHECKS_FILTER_FILE_PATH = os.path.join(
    os.path.dirname(__file__), "..", "models", "rules", "checks_filter.yaml"
)

with open(CHECKS_FILTER_FILE_PATH, "r", encoding="utf-8") as f:
    file_content = f.read()
    check_filters = yaml.safe_load(file_content)

feature_toggler_instance = FeatureToggler()
mr_metrics = MRMetrics()

feature_toggler = feature_toggler_instance.unleash_client


class AioHttpSessionManager:
    """
    A manager for managing aiohttp ClientSessions.
    """

    def __init__(self):
        self.sessions: List[aiohttp.ClientSession] = []

    async def __aenter__(self) -> aiohttp.ClientSession:
        session: aiohttp.ClientSession = aiohttp.ClientSession()
        self.sessions.append(session)
        return session

    async def __aexit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[Type[BaseException]],
    ) -> None:
        await asyncio.gather(*(session.close() for session in self.sessions))
        self.sessions.clear()


@router.get("/testAuth")
async def test(request: Request):
    """
    Test authentication route.

    Args:
        request (Request): FastAPI request object.

    Returns:
        str: Test successful message.
    """
    logger.info(request)
    return "Test successful"


@router.post("/pipeline/")
async def new_event_pipeline(
    body: PipelineHook, background_tasks: BackgroundTasks
):
    """
    Triggered when the status of a pipeline changes.

    Args:
        body (MRHook): Webhook payload.
        background_tasks (BackgroundTasks): Background tasks for processing.

    Returns:
        str: Acknowledgment message.
    """
    background_tasks.add_task(process_pipeline, body)

    return "Pipeline acknowledged, sent to processing!"


@router.post("/events/")
async def new_event(body: MRHook, background_tasks: BackgroundTasks):
    """
    Triggered when a new merge request is created, an existing merge request
    was updated/merged/closed, or a commit is added in the source branch.

    Args:
        body (MRHook): Webhook payload.
        background_tasks (BackgroundTasks): Background tasks for processing.

    Returns:
        str: Acknowledgment message.
    """
    if body.object_attributes.source_branch.startswith("marvin-quarantine-"):
        return f"Skipping checks, Marvin generated merge request: {body}"

    background_tasks.add_task(process_mr, body)

    return "MR acknowledged, sent to processing!"


@router.get("/metrics")
async def metrics():
    """
    Endpoint to be used by prometheus to scrap the metrics generated
    by the MR.

    data:
        'generate_latest' function from the prometheus_client library
        is called with the REGISTRY as its argument.
        'REGISTRY' is the default registry where all the metrics are
        registered' generate_latest(REGISTRY)' collects all the current
        metrics and formats them into the Prometheus text-based
        exposition format.

    Returns:
        content=data: The body of the response contains the metrics data
        generated by generate_latest(REGISTRY).

        media_type=CONTENT_TYPE_LATEST: This sets the Content-Type
        header of the response to text/plain; version=0.0.4.
        CONTENT_TYPE_LATEST is a constant from the
        prometheus_client.exposition module that specifies this content
        type, indicating that the response body contains Prometheus
        metrics in the latest exposition format.
    """
    if feature_toggler.is_enabled(
        mr_metrics.mr_metrics_toggle,
        fallback_function=lambda feature_name, context: True,
    ):
        try:
            return Response(
                content=mr_metrics.metrics, media_type=CONTENT_TYPE_LATEST
            )
        except Exception as e:
            logger.error("Failed to generate metrics: %s", e)
            raise HTTPException(
                status_code=500, detail="Failed to Generate Metrics"
            )
    else:
        return JSONResponse(
            content={"message": "METRICS EXPORTER disabled"},
            status_code=status.HTTP_404_NOT_FOUND,
        )


@router.post("/links")
async def links(body: JobURLs, background_tasks: BackgroundTasks):
    """
    Triggered when a new deployment job that generates links started.

    Args:
        body (JobURLs): JobURLs payload.
        background_tasks (BackgroundTasks): Background tasks for processing.

    Returns:
        str: Acknowledgment message.
    """
    background_tasks.add_task(process_urls, body)

    return "URLs received, sent to processing!"


async def process_pipeline(body: PipelineHook) -> str:
    """
    Triggered when the status of a pipeline changes.

    Args:
        body (PipelineHook): Pipeline webhook payload.

    Returns:
        str: Processing result message.
    """
    if not body.merge_request:
        return f"Skipping test coverage check, not a merge request: {body}"

    # passed wid warnings also returns sucess
    if body.object_attributes.status != "success":
        return f"Skipping test coverage check, Pipeline not in sucess state: {body}"  # NOQA: E501

    proj_id = body.project.id
    logger.info("Received proj_id: %s", proj_id)
    mr_id = body.merge_request.iid
    logger.info("Received mr_id: %s", mr_id)
    mr_state = body.merge_request.state
    logger.info("MR state: %s", mr_state)

    logger.debug("Received MR Hook: %s", body)

    mr_message = MessageGenerator("gunicorn.error")
    results = await process_checks(
        body, proj_id, mr_id, "pipeline_checks", mr_message
    )
    await generate_marvin_review(proj_id, mr_id, results, mr_message)
    return "Pipeline Checks are performed successfully"


async def process_mr(body: MRHook) -> str:
    """
    Triggered when a new merge request is created, an existing merge request
    was updated/merged/closed, or a commit is added in the source branch.

    Args:
        body (MRHook): MR webhook payload.

    Returns:
        str: Processing result message.
    """
    mr_id = body.object_attributes.iid
    logger.info("Received mr_id: %s", mr_id)
    proj_id = body.project.id
    logger.info("Received proj_id: %s", proj_id)
    mr_state = body.object_attributes.state
    logger.info("MR state: %s", mr_state)
    mr_url = body.object_attributes.url
    logger.info("MR url: %s", mr_url)
    commit = body.object_attributes.last_commit.id
    logger.info("Commit: %s", commit)

    if mr_state in ("merged", "closed"):
        return f"No check is performed: MR is {mr_state}"

    async with aiohttp.ClientSession() as session:
        gitlab_api = GitLabApi(session, cache=cache)
        proj_settings = await gitlab_api.get_project_settings_info(proj_id)

    proj_tags = proj_settings["tag_list"]
    matching_tags = list(set(proj_tags) & set(project_tags_blocklist))

    if matching_tags:
        return (
            f"No check is performed! Project is in blocklist: {matching_tags}"
        )

    await set_status_check(proj_id, mr_id, commit, mr_url, "pending")

    mr_message = MessageGenerator("gunicorn.error")
    check_results = await process_checks(
        body, proj_id, mr_id, "mr_checks", mr_message
    )

    await generate_marvin_review(proj_id, mr_id, check_results, mr_message)
    await clear_dashboards_and_logs_comment(proj_id, mr_id)

    if any(result.block_mr for result in check_results):
        await set_status_check(proj_id, mr_id, commit, mr_url, "failed")
        mr_metrics.increment_counter(
            mr_metrics.mr_fail_counter, proj_id=proj_id, mr_id=mr_id
        )
    else:
        await set_status_check(proj_id, mr_id, commit, mr_url, "passed")
        mr_metrics.increment_counter(
            mr_metrics.mr_pass_counter, proj_id=proj_id, mr_id=mr_id
        )

    total_runtime = 0

    for result in check_results:
        mr_metrics.record_runtime(
            mr_metrics.check_runtime_summary,
            runtime=result.runtime,
            check_name=result.name,
            proj_id=proj_id,
        )
        total_runtime += result.runtime

    mr_metrics.record_runtime(
        mr_metrics.mr_runtime_summary,
        runtime=total_runtime,
        proj_id=proj_id,
        mr_id=mr_id,
    )

    return "MR Checks are performed successfully"


async def process_checks(
    body: MRHook,
    proj_id: int,
    mr_id: int,
    check_type: str,
    mr_message: MessageGenerator,
) -> List[bool]:
    """
    Process checks based on the given check type.

    Args:
        body (MRHook): MR webhook payload.
        proj_id (int): Project ID.
        mr_id (int): Merge Request ID.
        check_type (str): Type of checks to perform.
        mr_message (MessageGenerator): MR Message Generator.

    Returns:
        List[bool]: List of results for each check.
    """
    async with AioHttpSessionManager() as manager:
        rtd_session = await manager.__aenter__()
        gitlab_session = await manager.__aenter__()
        # Add more sessions as needed

        api_instances = {
            ReadTheDocsApi: ReadTheDocsApi(rtd_session),
            JiraApi: JiraApi(),
            GitLabApi: GitLabApi(gitlab_session, cache=cache),
            NexusApi: NexusApi(),
            # Add more API classes and instances as needed
            # Be sure to add them on top of the file as well
        }

        # Gather check information
        # Checks are imported dynamically (bottom of file)
        check_calls = []
        for check_data in checks.get(check_type, []):
            check_name = check_data["name"]
            check_class = check_data["class"]
            apis = check_data["apis"]
            api_args = [api_instances[api] for api in apis]

            # Checks that should run are added to a list in the form of a callable  # NOQA: E501
            if check_should_run(body, check_name):
                check_call = perform_check(
                    check_class(*api_args, "gunicorn.error"),
                    mr_message,
                    body,
                    proj_id,
                    mr_id,
                )
                check_calls.append(check_call)
            else:
                logger.info("Check: %s - SKIPPED!\n\n", check_class.__name__)

        # Execute the checks
        check_results = await asyncio.gather(
            *check_calls, return_exceptions=True
        )

        valid_results = []
        for check_result in check_results:
            if isinstance(check_result.result, Exception):
                exception_type = type(check_result.result).__name__
                mr_metrics.increment_counter(
                    mr_metrics.check_exception_counter,
                    check_name=check_result.name,
                    exception_type=exception_type,
                    proj_id=proj_id,
                    mr_id=mr_id,
                )
            else:
                valid_results.append(check_result)

    return valid_results


async def perform_check(
    check: Check, mr_message: MessageGenerator, mr_event, proj_id, mr_id
):
    """
    Perform a check and update MR table based on the check result.

    Args:
        check (Check): Check to perform.
        mr_message (MessageGenerator): MR Message Generator.
        mr_event: MR event data.
        proj_id: Project ID of the Merge Request.
        mr_id: Merge Request id (iid field in API call).

    Returns:
        bool: True if the check passes (or it is disabled), False if it fails.
    """
    if feature_toggler.is_initialized:
        feature_toggler.unleash_verbose_log_level = logger.getEffectiveLevel()

    if not feature_toggler.is_enabled(
        check.feature_toggle,
        fallback_function=lambda feature_name, context: True,
    ):
        logger.info("Feature toggle %s is disabled", check.feature_toggle)
        return CheckResult(check.__class__.__name__, True, MessageType.INFO, 0)

    start = time.time()
    try:
        result = await check.check(mr_event, proj_id, mr_id)
    except Exception as e:
        result = e

    runtime = time.time() - start
    type = await check.type()
    description = await check.description()
    mitigation = await check.mitigation_strategy()
    name = check.__class__.__name__

    if isinstance(result, Exception):
        return CheckResult(name, result, type, runtime)
    elif not result:
        await mr_message.add_entry(type, description, mitigation)
        mr_metrics.increment_counter(
            mr_metrics.check_fail_counter,
            check_name=name,
            proj_id=proj_id,
        )
        logger.info("Check: %s - FAILED!", name)
        logger.info("Runtime: %.2f", runtime)
        logger.info("Type: %s", type)
        logger.info("Description: %s", description)
        logger.info("Mitigation:\n%s\n\n", mitigation)
    else:
        mr_metrics.increment_counter(
            mr_metrics.check_pass_counter,
            check_name=name,
            proj_id=proj_id,
        )
        logger.info("Check: %s - PASSED!", name)
        logger.info("Runtime: %.2f\n", runtime)

    return CheckResult(name, result, type, runtime)


def import_check_classes() -> Dict[
    str, List[Dict[str, Union[Type[Check], List[Type]]]]
]:
    """
    Import check classes dynamically.

    Returns:
        Dict[str, List[Dict[str, Union[Type[Check], List[Type]]]]]:
        Imported check classes.
    """  # NOQA: E501
    check_types = CHECK_TYPES
    models_folder = MODELS_FOLDER
    module_path = MODULE_PATH

    # NOTE: This is to make tests work. Don't know of a better way
    if os.getcwd().split("/")[-1] == "ska-cicd-automation":
        models_folder = "src/" + models_folder

    imported_checks = {}

    for check_type in check_types:
        check_files = os.listdir(os.path.join(models_folder, check_type))

        for file in check_files:
            check_name = file.strip(".py")
            module_name = module_path.format(check_type, check_name)
            module = importlib.import_module(module_name)
            module_classes = [
                class_name
                for class_name in dir(module)
                if class_name.startswith("Check") and class_name != "Check"
            ]

            for class_name in module_classes:
                check_class = getattr(module, class_name)
                check_data = {"class": check_class}
                check_data["name"] = check_name
                check_attrs = inspect.signature(check_class.__init__)

                for _, param in check_attrs.parameters.items():
                    if param.annotation in api_classes:
                        check_data.setdefault("apis", []).append(
                            param.annotation
                        )

                imported_checks.setdefault(check_type, []).append(check_data)

    return imported_checks


def check_should_run(mr_body, check_name):
    project_id = mr_body.project.id
    project_path = mr_body.project.path_with_namespace
    project_subgroup = project_path.rsplit("/", 1)[0]

    for check_filter in check_filters:
        subgroups = check_filter.get("subgroups", [])
        projects = check_filter.get("projects", [])
        skip_checks = check_filter.get("skip_checks", [])
        run_checks = check_filter.get("run_checks", [])

        if project_id not in projects and project_subgroup not in subgroups:
            continue

        if skip_checks and check_name in skip_checks:
            return False

        elif run_checks and check_name not in run_checks:
            return False

    return True


async def generate_marvin_review(
    proj_id: int,
    mr_id: int,
    check_results: List[CheckResult],
    mr_message: MessageGenerator,
) -> None:
    """
    Generate a review for a Merge Request (MR) based on the results of quality checks.

    Args:
        proj_id (int): Project ID of the Merge Request.
        mr_id (int): Merge Request ID.
        results (List[bool]): List of boolean results for each quality check.
        mr_message (MessageGenerator): Message generator for constructing review messages.

    Returns:
        None: The function does not return a value.
    """  # NOQA: E501
    if any(not check_result.result for check_result in check_results):
        mr_message.pretext = (
            "There seems to be some issues with the Merge Request"
            " (MR), Please review the table below and consult "
            "[the developer portal](https://developer.skatelescope.org/en/latest/tools/git.html#merge-request-quality-checks) "  # NOQA: E501
            "for further information:"
        )

        if any(check_result.block_mr for check_result in check_results):
            mr_message.pretext += (
                "<br>**Note that any Failure (:no_entry_sign:) type of checks"
                " will result in Marvin Status Check failing"
                " which will cause MR to be blocked from merging as"
                " SKAO Policies**"
            )

        mr_message.posttext = f'*"{MarvinComments().get_random_quote()}"*'
        comment = await mr_message.get_message()
    else:
        comment = await mr_message.get_well_done_message()

    # logger.info("comment: %s\n", comment)

    async with aiohttp.ClientSession() as session:
        gitlab_api = GitLabApi(session, cache=cache)
        executor = CommentExecutor(gitlab_api, "gunicorn.error")
        await executor.action(
            proj_id, mr_id, comment, MessageGenerator.message_marker
        )

        mr_approvals = await gitlab_api.get_merge_request_approvals_info(
            proj_id, mr_id
        )
        marvin_approved = any(
            approver["user"]["username"] == MARVIN_USERNAME
            for approver in mr_approvals["approved_by"]
        )
        if not feature_toggler.is_enabled(
            MR_APPROVAL_FEATURE_TOGGLE,
            fallback_function=lambda feature_name, context: True,
        ):
            if MessageType.FAILURE.value in comment:
                if marvin_approved:  # if approved MR, unapprove
                    await gitlab_api.remove_approval_merge_request(
                        proj_id, mr_id
                    )
            else:
                if not marvin_approved:  # If unapproved MR, approve
                    await gitlab_api.add_approval_merge_request(proj_id, mr_id)


async def set_status_check(proj_id, mr_id, commit, url, check_status):
    if not feature_toggler.is_enabled(
        STATUS_CHECK_FEATURE_TOGGLE,
        fallback_function=lambda feature_name, context: True,
    ):
        logger.warning("Status check feature toggle is disabled.")
        check_status = "passed"  # Force the status to passed

    async with aiohttp.ClientSession() as session:
        gitlab_api = GitLabApi(session, cache=cache)
        status_checks = await gitlab_api.get_status_checks(proj_id, mr_id)

        status_check_id = None
        for status_check in status_checks:
            if status_check.get("name") == MARVIN_STATUS_CHECK:
                status_check_id = status_check.get("id")
                break

        if status_check_id is not None:
            await gitlab_api.set_status_check(
                proj_id, mr_id, commit, status_check_id, check_status
            )
            logger.info(
                "The status check for %s has been set to %s.",
                url,
                check_status,
            )
        else:
            logger.warning("No status check defined for project %s.", proj_id)


async def process_urls(job_urls: JobURLs) -> str:
    """
    Triggered when pipeline job starts and generates URLs.

    Args:
        job_urls (JobURLs): JobURLs payload.

    Returns:
        str: Processing result message.
    """

    async with comment_lock:
        project_id = job_urls.project_id
        mr_id = job_urls.mr_id

        mr_message = LinksMessageGenerator("uvicorn.error")
        async with aiohttp.ClientSession() as session:
            gitlab_api = GitLabApi(session, cache=cache)
            comments = await gitlab_api.get_merge_request_comments(
                project_id, mr_id
            )
            async for comment in comments:
                if LinksMessageGenerator.message_marker in comment["body"]:
                    # Extract JSON from the 'links' key inside HTML comment
                    pattern = r"<!--.*?links:\s*({.*?}}).*?-->"
                    match = re.search(pattern, comment["body"], re.DOTALL)

                    if match:
                        json_str = match.group(1)
                        try:
                            metadata_dict = json.loads(json_str)
                            mr_message.links = metadata_dict
                        except json.JSONDecodeError as e:
                            print("Failed to decode JSON:", e)
                    break

            mr_message.links[job_urls.kube_namespace] = job_urls.model_dump(
                exclude={"project_id", "mr_id"}
            )

            await mr_message.add_pretext(
                "# :page_with_curl: Dashboards and Logs"
            )

            # for each namespace add a table with links
            for namespace_name in mr_message.links:
                await mr_message.add_links_template(
                    mr_message.links[namespace_name]
                )

            await mr_message.add_posttext(
                f'*"{MarvinComments().get_random_links_quote()}"*'
            )
            comment = await mr_message.get_message()
            executor = CommentExecutor(gitlab_api, "uvicorn.error")
            await executor.action(
                project_id,
                mr_id,
                comment,
                LinksMessageGenerator.message_marker,
            )

        return "Comment with Links generated successfully."


async def clear_dashboards_and_logs_comment(proj_id: str, mr_id: str) -> None:
    """
    Used to clear the "Dashboards and Logs" comment.
    Should be triggered when a new pipeline starts.

    Args:
        proj_id (str): Gitlab project ID.

    Returns:
        mr_id (str): Merge Request ID.
    """
    mr_message = LinksMessageGenerator("uvicorn.error")
    await mr_message.add_pretext("# :page_with_curl: Dashboards and Logs")
    await mr_message.add_posttext(
        (
            '*"Oh, you wanted logs and metrics? '
            "The universe and I have something in "
            'common then—we both have nothing to offer."*'
        )
    )
    comment = await mr_message.get_message()
    async with comment_lock:
        async with aiohttp.ClientSession() as session:
            gitlab_api = GitLabApi(session, cache=cache)
            executor = CommentExecutor(gitlab_api, "uvicorn.error")
            await executor.action(
                proj_id, mr_id, comment, LinksMessageGenerator.message_marker
            )


checks = import_check_classes()
