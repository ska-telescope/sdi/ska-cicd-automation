import os

import aiohttp
from packaging.version import Version

NEXUS_URL = os.getenv("NEXUS_URL", "https://artefact.skao.int")
NEXUS_HELM_REPO = os.getenv("NEXUS_HELM_REPO", "helm-internal")
SEARCH_API = "{url}/service/rest/v1/search?repository={repository}&name={chart}&sort=version"  # NOQA: E501

# NOTE: This API is only here until ska-services's NexusAPI is updated.s


class NexusApi:
    def __init__(self) -> None:
        pass

    def _is_stable_version(self, version):
        parsed_version = Version(version)
        return not (
            parsed_version.is_prerelease or parsed_version.is_postrelease
        )

    async def get_latest_chart_version(
        self, chart, url=NEXUS_URL, repository=NEXUS_HELM_REPO
    ):
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(
                    SEARCH_API.format(
                        url=url, repository=repository, chart=chart
                    )
                ) as response:
                    charts = await response.json()

            versions = [
                chart["version"]
                for chart in charts["items"]
                if self._is_stable_version(chart["version"])
            ]
            if versions:
                return {"version": versions[0]}
            else:
                return {}
        except (
            aiohttp.ClientError,
            aiohttp.InvalidURL,
            aiohttp.ClientResponseError,
        ):
            return {}
        except Exception:
            return {}
