# SKA CI/CD Automation Services MR Checks

This plugin is used to check MR quality and provide feedback on the MR window by making comments and updating them.

It uses FastAPI to create webhook that can be set to listen for the GitLab Projects.
The following environment variables must be present, the token should have API access to the project:

```console
PRIVATE_TOKEN=...
REQUESTER=...
JIRA_URL=...
JIRA_USERNAME=...
JIRA_PASSWORD=...
GITLAB_TOKEN=...
GITLAB_HEADER=...
UNLEASH_API_URL=...
UNLEASH_INSTANCE_ID=...
RTD_TOKEN=...
```

## Monitoring

This plugin exposes several Prometheus metrics to monitor the effectiveness and efficiency of the processes. These metrics are accessible through a metrics endpoint and are useful for monitoring and alerting purposes.

- **Metrics Endpoint**: A dedicated endpoint that returns the latest metrics data in Prometheus format, allowing for integration with monitoring systems.
- **Feature Toggle**: Conditional feature execution to enable or disable metrics endpoint return and metrics incrementations.

<!-- markdownlint-disable -->
<table>
    <tr>
        <th>Type</th>
        <th>Description</th>
        <th>Labels</th>
    </tr>
    <tr>
        <td>Counter</td>
        <td><strong>Merge Request Pass Counter</strong> (`mr_pass_count`): Tracks the number of merge requests that have passed.</td>
        <td>
            <ul>
                <li><code>proj_id</code>: The project identifier associated with the merge request.</li>
                <li><code>mr_id</code>: The merge request identifier.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Counter</td>
        <td><strong>Merge Request Fail Counter</strong> (`mr_fail_count`): Counts the number of merge requests that have failed.</td>
        <td>
            <ul>
                <li><code>proj_id</code>: The project identifier associated with the merge request.</li>
                <li><code>mr_id</code>: The merge request identifier.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Counter</td>
        <td><strong>Check Pass Counter</strong> (`check_pass_count`): Records the number of times checks associated with merge requests pass successfully.</td>
        <td>
            <ul>
                <li><code>check_name</code>: The name of the check that passes.</li>
                <li><code>proj_id</code>: The project identifier associated with the merge request.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Counter</td>
        <td><strong>Check Fail Counter</strong> (`check_fail_count`): Records the number of times checks associated with merge requests fail.</td>
        <td>
            <ul>
                <li><code>check_name</code>: The name of the check that failed.</li>
                <li><code>proj_id</code>: The project identifier associated with the merge request.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Counter</td>
        <td><strong>Check Exception Counter</strong> (`check_exception_count`): Counts exceptions that occur during the execution of checks.</td>
        <td>
            <ul>
                <li><code>check_name</code>: The name of the check during which the exception occurred.</li>
                <li><code>exception_type</code>: The type of exception that was raised.</li>
                <li><code>proj_id</code>: The project identifier associated with the merge request.</li>
                <li><code>mr_id</code>: The merge request identifier.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Summary</td>
        <td><strong>Merge Request Runtime Summary</strong> (`mr_runtime_summary`): Measures the time required to run a merge request and the number of merge requests performed.</td>
        <td>
            <ul>
                <li><code>proj_id</code>: The project identifier for the merge request.</li>
                <li><code>mr_id</code>: The merge request identifier.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Summary</td>
        <td><strong>Check Runtime Summary</strong> (`check_runtime_seconds`): Measures the time required to run a check.</td>
        <td>
            <ul>
                <li><code>check_name</code>: The name of the check being timed.</li>
                <li><code>proj_id</code>: The project identifier associated with the merge request.</li>
            </ul>
        </td>
    </tr>
</table>
<!-- markdownlint-enable -->

## Checks

Each check should have:

- Feature Toggle Name: name of the check for runtime configuration
- Result Type: If the check is not successful, whether it should be marked as FAILURE, WARNING, or INFO
- Description: Brief description about what the check is about
- Mitigation Strategy: How to take corrective action to fix the broken check

Currently the plugin checks the MR are:

<!-- markdownlint-disable -->
<table>
    <tr>
        <th>Type</th>
        <th>Description</th>
        <th>Mitigation</th>
    </tr>
    <tr>
            <td>Warning</td>
            <td>Missing Test Coverage</td>
            <td> This Project is missing test coverage <br>Please have a look at the following [page](https://developer.skatelescope.org/en/latest/tools/ci-cd/continuous-integration.html?highlight=coverage#automated-collection-of-ci-health-metrics-as-part-of-the-ci-pipeline)</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Missing Assignee</td>
        <td>Please assign at least one person for the MR</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Source Branch Delete Setting</td>
        <td>Please check "Delete source branch when merge request is accepted.</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Missing Jira Ticket ID in MR Title</td>
        <td>Please uncheck Squash commits when merge request is accepted.</td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Docker-Compose Usage Found</td>
        <td>
            Please remove docker-compose from following files:
            <ul>
                <li>At file: `file_location` on line `line_number`</li>
                <li>At file: `file_location` on line `line_number`</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Missing CODEOWNERS file</td>
        <td>
            Please add a <a href=https://docs.gitlab.com/ee/user/project/code_owners.html>CODEOWNERS</a> file to the root folder.
        </td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Non-Complaint Project Slug Name</td>
        <td>
            Project Slug should start with <strong>ska-</strong>.
            To change the slug go into: Settings->Advanced->Change Path
        </td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Missing Jira Ticket ID in Branch Name</td>
        <td>Branch Name should start with a Jira ticket</td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Missing Jira Ticket ID in commits</td>
        <td>
            Following commit messages violate the <a href=https://developer.skatelescope.org/en/latest/tools/git.html#committing-code>formatting standards</a>:
            <ul>
                <li>At commit: `commit_hash`</li>
                <li>At commit: `commit_hash`</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Non-compliant License Information</td>
        <td>Please update the license information according to <a href=https://developer.skatelescope.org/en/latest/projects/licensing.html>developer portal guidelines</a> for your project</td>
    </tr>
    <tr>
        <td>Information</td>
        <td>Documentation Changes</td>
        <td>This MR doesn't introduce any documentation changes. Please consider updating documentation to reflect your changes</td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>ReadTheDocs Integration</td>
        <td>
           Please integrate this project with ReadtheDocs following <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#how-to-document>the guidelines</a>:
            <ul>
                <li>Please set up docs/ folder for sphinx documentation build following <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#documentation-on-git>the guidelines</a></li>
                <li>Please add this project as a subproject on Read the Docs following <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#add-project-as-a-sub-project-on-readthedocs>the guidelines</a></li>
                <li>Please <a href=https://developer.skatelescope.org/en/latest/tools/documentation.html#import-project-to-readthedocs>import</a> your project into Read the Docs</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Wrong Merge Request Settings</td>
        <td>
            Reconfigure Merge Request Settings according to the <a href="https://developer.skatelescope.org/en/latest/tools/git.html#merge-requests">guidelines</a>:
            <ul>
                <li>MR Settings Checks:</li>
                <li>You should assign one or more people as reviewer(s)</li>
                <li>Automatically resolve mr diff discussions should be checked</li>
                <li>Override approvers and approvals per MR should be checked</li>
                <li>Remove all approvals when new commits are pushed should be checked</li>
                <li>Prevent approval of MR by the author should be checked</li>
                <li>There should be at least 1 approval required</li>
                <li>Please check Delete source branch when merge request is accepted.</li>
                <li>Please uncheck Squash commits when merge request is accepted.</li>
            </ul>
            Project Settings Checks (You may need Maintainer rights to change these):
            <ul>
                <li>Pipelines must succeed should be checked</li>
                <li>Enable Delete source branch option by default should be checked</li>
                <li>Show link to create/view MR when pushing from the command line should be checked</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Failure</td>
        <td>Could not find needed pipeline jobs</td>
        <td>
            <p>Please create a pipeline on this Merge Request</p>
            <p style="text-align:center;">OR</p>                         
            The repository is not using any of the CI/CD Templates, Please include the following templates:
            <p> 
                <ul>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/finaliser.gitlab-ci.yml" >.post step finalisers eg: badges</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/oci-image.gitlab-ci.yml" >OCI Images jobs</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/helm.gitlab-ci.yml" >Helm Charts</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/python.gitlab-ci.yml" >Python packages build,lint, test and publish</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/docs.gitlab-ci.yml" >Docs pages</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/k8s.gitlab-ci.yml" >k8s steps</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/release.gitlab-ci.yml" >release page</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/ansible.gitlab-ci.yml" >Ansible Collections</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/release.gitlab-ci.yml" >Release Support</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/raw.gitlab-ci.yml" >Raw Packages</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/conan.gitlab-ci.yml" >Conan Packages</a></li>
                </ul>
            </p>
            <p style="text-align:center;">OR</p>                         
            The repository is only using a subset of the CI/CD templates(i.e. python-build.gitlab-ci.yml instead of python.gitlab-ci.yml) that's available. Please include the main template(i.e. python.gitlab-ci.yml) to cover the whole software lifecycle or contact #team-system-support if there's a reason you can't use it:
            <p> 
                <ul>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/finaliser.gitlab-ci.yml" >.post step finalisers eg: badges</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/oci-image.gitlab-ci.yml" >OCI Images jobs</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/helm.gitlab-ci.yml" >Helm Charts</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/python.gitlab-ci.yml" >Python packages build,lint, test and publish</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/docs.gitlab-ci.yml" >Docs pages</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/k8s.gitlab-ci.yml" >k8s steps</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/release.gitlab-ci.yml" >release page</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/ansible.gitlab-ci.yml" >Ansible Collections</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/release.gitlab-ci.yml" >Release Support</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/raw.gitlab-ci.yml" >Raw Packages</a></li>
                    <li><a href="https://gitlab.com/ska-telescope/templates-repository/-/blob/master/gitlab-ci/includes/conan.gitlab-ci.yml" >Conan Packages</a></li>
                </ul>
            </p>
        </td>
    </tr>
    <tr>
        <td>Warning</td>
        <td>Repository Structure is not following standardised <a href=" https://confluence.skatelescope.org/display/SE/Standardising+Project+Structure+and+Content">Project Structure</a></td>
        <td>
            <p>Following rules failed for the repository structure:</p>
            <p> 
                <ul>
                <li>Helm: There should be at least one chart in the `charts/` folder</li>
                <li>Python: There should be `pyproject.toml` file in the root folder</li>
                <li>Python: files should be under a python module starting with <proj_slug> in the `src/` folder</li>
                <li>...</li>
                </ul>
            </p>
        </td>
    </tr>
</table>
<!-- markdownlint-enable -->

### Automatic Fixing of Wrong Merge Request Settings

Marvin will attempt to automatically check the delete source branch
and uncheck the squash commits on merge settings.
Next to each of the other Wrong Merger Request Settings messages is a `Fix` link,
which will trigger Marvin to attempt to fix that setting after the user is authenticated.
Only users that are assigned to the merge request can trigger
this automatic setting fix feature.

### Marvin MR Approval

Marvin after creating the table will verify if there is any checks under
the failure category failed, if so Marvin does not approve the MR, and
in the case that that MR was already approved before by him he unapproves it.
If none of the checks under the failure category failed Marvin will approve the MR.

### Runtime Configuration

This service is using feature toggles to determine which checks
to enable/disable at the runtime.
It uses [Unleash](https://unleash.github.io/) integration provide
by GitLab to achieve this.

For the project level configuration, a project could be disabled using
`Project Tags/Topics`. The service uses a blocklist to determine whether
it should run the checks as well.

#### Precedence of configuration

- Project Level Configuration with Tags/Topics
- Feature Toggle Strategies

## How to Add a New Check

Each new check must use the abstract base class, [Check](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/src/ska_cicd_automation/plugins/gitlab_mr/models/check.py),
to ensure to define its `type`, `description`, `mitigation strategy`
and `check` action, which performs the actual checking on the MR and
returns a boolean indicating the result.

Base Class:

```python
class Check(ABC):

    feature_toggle: str = NotImplemented

    @abstractmethod
    async def check(self, mr_event, proj_id, mr_id) -> bool:
        pass

    @abstractmethod
    async def type(self) -> MessageType:
        pass

    @abstractmethod
    async def description(self) -> str:
        pass

    @abstractmethod
    async def mitigation_strategy(self) -> str:
        pass
```

Example Check:

```python
class CheckAssigneesComment(Check):
    feature_toggle = "check-assignees-comment"

    def __init__(self, api: GitLabApi, logger_name):
        self.api = api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event, proj_id, mr_id):
        mr = await self.api.get_merge_request_info(proj_id, mr_id)
        self.logger.debug("Retrieved MR: %s", mr)
        return len(mr["assignees"]) > 0

    async def type(self) -> MessageType:
        return MessageType.FAILURE

    async def description(self) -> str:
        return "Missing Assignee"

    async def mitigation_strategy(self) -> str:
        return "Please assign at least one person for the MR"
```

Then the necessary tests for the added checks should be added
in [tests](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/tests/unit/) folder. These tests should get picked up
by the main frameworks testing.

Finally, each check should be initialised and called
in the [mrevents](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/src/ska_cicd_automation/plugins/gitlab_mr/routers/mrevent.py) file to be included into the
list of checks that are performed for the MRs.
