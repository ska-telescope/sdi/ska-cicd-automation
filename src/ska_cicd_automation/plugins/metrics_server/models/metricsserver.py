import logging
import os

from prometheus_client import Counter, generate_latest, write_to_textfile
from prometheus_client.core import CollectorRegistry

from ska_cicd_automation.models.utils.feature_toggle import FeatureToggler
from ska_cicd_automation.models.utils.singleton import SingletonClass

logger = logging.getLogger(__name__)

METRICS_FILE = os.getenv("METRICS_SERVER_REGISTRY_PATH")


class MetricsServer(metaclass=SingletonClass):
    """
    A class to manage and expose metrics for merge requests and checks.

    This class is designed to track various metrics related to merge requests
    (MRs) and their associated checks using Prometheus. It ensures that only
    one instance of the class is created (Singleton pattern), and initializes
    Prometheus metrics for counters and summaries. It provides methods to
    increment counters and record runtimes, conditionally enabled based on
    feature toggles.

    Attributes:
        registry (CollectorRegistry): A Prometheus registry to collect
        metrics make_target_counter (Counter): Counts the nº of times a
        make target was called.

    Methods:
        create_counter(name, description, labels): Creates a Prometheus
        counter init_counters(): Initializes all counters
        get_metrics(): Returns the latest metrics in Prometheus format
        increment_counter(counter, **labels): Increments the specified
        counter.
    """

    __allow_reinitialization = False

    def __init__(self):
        self.feature_toggle = FeatureToggler()
        self.registry = CollectorRegistry()
        self.init_counters()
        self.load_metrics()
        self.metrics = None
        self.metrics_server_toggle = "metrics-server"

    def create_counter(self, name, description, labels):
        return Counter(name, description, labels, registry=self.registry)

    def init_counters(self):
        self.make_target_counter = self.create_counter(
            "make_target_runs",
            "Number of times make target was run",
            ["make_target", "proj_id"],
        )

    def get_metrics(self):
        return generate_latest(self.registry)

    def increment_counter(self, counter, **labels):
        if self.is_feature_enabled():
            counter.labels(**labels).inc()

    def is_feature_enabled(self):
        return self.feature_toggle.unleash_client.is_enabled(
            self.metrics_server_toggle,
            fallback_function=lambda feature_name, context: True,
        )

    def scheduled_metrics_update(self):
        self.save_metrics()
        self.metrics = self.get_metrics()

    def save_metrics(self):
        os.makedirs(os.path.dirname(METRICS_FILE), exist_ok=True)
        logger.debug("Saving metrics to: %s", METRICS_FILE)
        write_to_textfile(METRICS_FILE, self.registry)

    def load_metrics(self):
        if os.path.exists(METRICS_FILE):
            with open(METRICS_FILE, "r", encoding="utf-8") as f:
                for line in f:
                    # Skip comment lines and metadata lines
                    if line.startswith("#"):
                        continue
                    # Remove leading/trailing whitespaces
                    line = line.strip()
                    # Process only lines with exactly one space
                    if " " in line:
                        parts = line.split(" ")
                    else:
                        logger.warning(
                            "Skipping line without space separator: %s", line
                        )
                        continue
                    # Ensure there are exactly two parts
                    if len(parts) != 2:
                        logger.warning("Skipping malformed line: %s", line)
                        continue
                    metric, value = parts
                    try:
                        value = float(value)
                    except ValueError:
                        logger.warning(
                            "Invalid value for metric %s: %s", metric, value
                        )
                        continue
                    # Check if metric has labels
                    if "{" in metric:
                        name, labels = metric.split("{")
                        labels = labels.rstrip("}")
                        try:
                            label_dict = dict(
                                item.split("=") for item in labels.split(",")
                            )
                            label_dict = {
                                k: v.strip('"') for k, v in label_dict.items()
                            }
                        except ValueError:
                            logger.warning(
                                "Malformed labels for metric: %s", metric
                            )
                            continue
                        self.update_metric(name, label_dict, value)

    def update_metric(self, name, labels, value):
        try:
            if name == "make_target_runs_total":
                self.make_target_counter.labels(**labels).inc(value)
            else:
                logger.info("Unknown metric name: %s", name)
        except Exception as e:
            logger.error("Error updating metric %s: %s", name, e)
