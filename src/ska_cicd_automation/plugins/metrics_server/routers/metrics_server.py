# flake8: noqa E501
import logging

from fastapi import HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.routing import APIRouter
from prometheus_client.exposition import CONTENT_TYPE_LATEST
from pydantic import BaseModel
from starlette.responses import Response

from ska_cicd_automation.models.utils.feature_toggle import FeatureToggler
from ska_cicd_automation.plugins.metrics_server.models.metricsserver import (
    MetricsServer,
)

logger = logging.getLogger("gunicorn.error")

router = APIRouter()

metrics_server = MetricsServer()

feature_toggler_instance = FeatureToggler()
feature_toggler = feature_toggler_instance.unleash_client


@router.get("/metrics")
async def metrics():
    """
    Endpoint to be used by prometheus to scrap the metrics generated
    by the make target.
    """

    if feature_toggler.is_enabled(
        metrics_server.metrics_server_toggle,
        fallback_function=lambda feature_name, context: True,
    ):
        try:
            return Response(
                content=metrics_server.metrics,
                media_type=CONTENT_TYPE_LATEST,
            )
        except Exception as e:
            logger.error("Failed to generate metrics: %s", e)
            raise HTTPException(
                status_code=500, detail="Failed to Generate Metrics"
            )
    else:
        return JSONResponse(
            content={"message": "METRICS EXPORTER disabled"},
            status_code=status.HTTP_404_NOT_FOUND,
        )


class TargetMetadata(BaseModel):
    CI_JOB_ID: str
    MAKE_TARGET: str
    CI_PIPELINE_ID: str
    CI_PROJECT_ID: str
    CI_PROJECT_NAME: str


@router.post("/target")
async def send_target_metadata(metadata: TargetMetadata) -> str:
    """
    Get target metadata and increment the make target counter.
    Parameters:
        metadata (TargetMetadata): A model containing event data.

    Returns:
        str: A status message.

    Raises:
        HTTPException: If there's an error adding the event to the database.
    """
    # Log the received data
    logger.debug("Received data: %s", metadata)

    try:
        metrics_server.increment_counter(
            metrics_server.make_target_counter,
            make_target=metadata.MAKE_TARGET,
            proj_id=metadata.CI_PROJECT_ID,
        )
    except Exception as e:
        logger.error("Error processing metrics: %s", e)
        raise HTTPException(
            status_code=500, detail="Error processing metrics"
        ) from e

    return "Target metadata recieved, sent to Prometheus!"
