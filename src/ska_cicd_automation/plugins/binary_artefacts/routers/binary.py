import logging

from fastapi import status
from fastapi.responses import JSONResponse
from fastapi.routing import APIRouter

from ska_cicd_automation.plugins.binary_artefacts.apis.oras import (
    harbor_health,
)
from ska_cicd_automation.plugins.binary_artefacts.routers import binary_v1

logger = logging.getLogger("uvicorn.error")

router = APIRouter(prefix="/api")
router.include_router(binary_v1.router, prefix="/v1")


@router.get("/health")
def test():
    resp = harbor_health()
    return JSONResponse(
        content={"status": resp.json()["status"]},
        status_code=status.HTTP_200_OK,
    )
