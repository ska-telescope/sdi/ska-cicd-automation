import json
import logging
import shutil
from pathlib import Path as pathlibPath

from fastapi import (
    Body,
    HTTPException,
    Path,
    Query,
    Request,
    UploadFile,
    status,
)
from fastapi.responses import FileResponse, JSONResponse
from fastapi.routing import APIRouter

from ska_cicd_automation.plugins.binary_artefacts.apis.oras import (
    harbor_delete_artefact_by_sha,
    harbor_delete_artefact_by_tag,
    harbor_get_artefact,
    harbor_get_artefact_by_sha,
    harbor_get_artefact_count,
    harbor_list_repositories,
    harbor_repo_artefacts,
    oras_pull,
    oras_push,
    save_upload_file,
)

logger = logging.getLogger("uvicorn.error")

router = APIRouter()


@router.get("/artefacts", description="List all the artefacts.")
async def list_artefacts(
    page: int = Query(default=None, ge=1, description="The page number"),
    page_size: int = Query(
        default=None, le=100, description="The number of artefacts per page"
    ),
):
    try:
        json_proj = harbor_get_artefact_count()
        x_total_count = json_proj.json()["repo_count"]
        headers = {"X-Total-Count": str(x_total_count)}

        # each version of an artefact is hosted inside harbor in a repository
        # each repository is named after the artefact name
        if not (page and page_size):
            page = 1
            page_size = x_total_count

        json_repo_ls = harbor_list_repositories(page=page, page_size=page_size)

        repo_list = json_repo_ls.json()

        for repo in repo_list:
            repo["name"] = repo["name"].replace("external-artefacts/", "")

        if json_repo_ls.status_code != 200:
            raise HTTPException(status_code=json_repo_ls.status_code)

        if len(repo_list) != 0:
            base_url = "/artefacts"
            pagination_dict = {
                "_links": {
                    "next": f"{base_url}?page={page+1}&page_size={page_size}"
                }
            }
            repo_list.append(pagination_dict)

        return JSONResponse(
            content=repo_list,
            status_code=json_repo_ls.status_code,
            headers=headers,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail={"list_artefacts": "not ok", "exception": str(ex)},
        )


@router.get(
    "/artefacts/{artefact_name}",
    description="List all versions of an artefact.",
)
async def list_artefact_versions(
    artefact_name: str = Path(
        description="The name of the artefact."  # noqa E501
    ),
    page: int = Query(default=1, ge=1, description="The page number"),
    page_size: int = Query(
        default=10,
        le=100,
        description="The number of artefact versions per page",
    ),
):
    try:
        json_artefacts_ls = harbor_repo_artefacts(
            artefact_name, page=page, page_size=page_size
        )
        artefact_list = json_artefacts_ls.json()

        if json_artefacts_ls.status_code != 200:
            raise HTTPException(status_code=json_artefacts_ls.status_code)

        if len(artefact_list) != 0:
            base_url = f"/artefacts/{artefact_name}"
            pagination_dict = {
                "_links": {
                    "next": f"{base_url}?page={page+1}&page_size={page_size}"  # noqa E501
                }
            }
            artefact_list.append(pagination_dict)

        return JSONResponse(
            content=artefact_list,
            status_code=json_artefacts_ls.status_code,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        raise HTTPException(
            detail={"list_artefact_versions": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@router.get(
    "/artefacts/{artefact_name}/sha/{sha}",
    description="Get metadata for a specific artefact version by sha",
)
async def get_artefact_by_sha(
    artefact_name: str = Path(description="The name of the artefact."),
    sha: str = Path(description="The SHA identifier of the artefact."),
    format: str
    | None = Query(
        None,
        description="""
        Download an archive with the complete
        artefact. Supports zip currently.
        """,
    ),
):
    try:
        local_res = harbor_get_artefact_by_sha(
            artefact_name, sha.replace("sha256:", "")
        )

        tag_name = local_res.json()["tags"][0]["name"]
        if format:
            dir_name, _ = oras_pull(artefact_name, tag_name)
            shutil.make_archive(
                f"/tmp/download_{artefact_name}_{sha}",
                "zip",
                root_dir=dir_name,
            )
            shutil.rmtree(dir_name)
            return FileResponse(
                f"/tmp/download_{artefact_name}_{sha}.zip",
                media_type="application/zip",
                filename=f"{artefact_name}_{sha}.zip",
            )

        return JSONResponse(
            content=local_res.json(),
            status_code=local_res.status_code,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        return JSONResponse(
            content={"get_artefact_by_sha": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@router.get(
    "/artefacts/{artefact_name}/tags/{tag_name}",
    description="Get metadata for a specific artefact version.",
)
async def get_artefact_version_metadata(
    artefact_name: str = Path(
        description="The name of the artefact."  # noqa E501
    ),
    tag_name: str = Path(description="Tag of the artefact version to fetch"),
    format: str
    | None = Query(
        None,
        description="""
        Download an archive with the complete
        artefact. Supports zip currently.
        """,
    ),
):
    try:
        resp = harbor_get_artefact(artefact_name, tag_name)
        if resp.status_code != 200:
            return JSONResponse(
                content={"list_artefacts": "not ok"},
                status_code=resp.status_code,
            )

        if format:
            dir_name, _ = oras_pull(artefact_name, tag_name)
            shutil.make_archive(
                f"/tmp/download_{artefact_name}_{tag_name}",
                "zip",
                root_dir=dir_name,
            )
            shutil.rmtree(dir_name)
            return FileResponse(
                f"/tmp/download_{artefact_name}_{tag_name}.zip",
                media_type="application/zip",
                filename=f"{artefact_name}_{tag_name}.zip",
            )

        return JSONResponse(
            content=resp.json(),
            status_code=status.HTTP_200_OK,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        raise HTTPException(
            detail={"list_artefact_versions": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@router.get(
    "/artefacts/{artefact_name}/tags/{tag_name}/assets",
    description="List assets in artefact version.",
)
@router.get(
    "/artefacts/{artefact_name}/tags/{tag_name}/assets/{asset_name}",
    description="Download an asset in a artefact version.",
)
async def get_artefact_files(
    artefact_name: str = Path(
        description="The name of the artefact."  # noqa E501
    ),
    tag_name: str = Path(description="Tag of the artefact version to fetch"),
    asset_name: str | None = None,
):
    try:
        resp = harbor_get_artefact(artefact_name, tag_name)
        if resp.status_code != 200:
            return JSONResponse(
                content={"list_artefacts": "not ok"},
                status_code=resp.status_code,
            )

        json_content = resp.json()

        if "annotations" not in json_content:
            return JSONResponse(
                content={"get_artefact_files": "annotations field not found"},
                status_code=status.HTTP_404_NOT_FOUND,
            )

        if "included-files" not in json_content["annotations"]:
            return JSONResponse(
                content={
                    "get_artefact_files": "included-files field not found"
                },
                status_code=status.HTTP_404_NOT_FOUND,
            )

        if asset_name is not None:
            if asset_name in json_content["annotations"]["included-files"]:
                dir_name, _ = oras_pull(artefact_name, tag_name)
                return FileResponse(f"{dir_name}/{asset_name}")
            else:
                return JSONResponse(
                    content={"get_artefact_files": f"{asset_name} not found"},
                    status_code=status.HTTP_404_NOT_FOUND,
                )

        return JSONResponse(
            content=json_content["annotations"]["included-files"],
            status_code=status.HTTP_200_OK,
        )

    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        raise HTTPException(
            detail={"get_artefact_filenames": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@router.post(
    "/artefacts/{artefact_name}/tags/{tag_name}",
    description="Upload a new artefact.",
)
@router.put(
    "/artefacts/{artefact_name}/tags/{tag_name}",
    description="Overwrite an existing artefact by tag.",
)
async def create_new_artefact(
    request: Request,
    files: list[UploadFile],
    artefact_name: str = Path(description="The name of the artefact."),
    tag_name: str = Path(description="The tag of the artefact."),
    annotations: str = Body(
        default="{}", description="Additional metadata for the artefact."
    ),
):
    resp = harbor_get_artefact(artefact_name, tag_name)

    if request.method == "POST":
        if resp.status_code == 200:
            return JSONResponse(
                content=f"{artefact_name}:{tag_name} already exists. Please use other tag or use the PUT method to overwrite the current artefact",  # noqa E501
                status_code=status.HTTP_409_CONFLICT,
            )

    if request.method == "PUT":
        if resp.status_code == 404:
            return JSONResponse(
                content=f"{artefact_name}:{tag_name} does not exit. Please use the POST method to create a new artefact.",  # noqa E501
                status_code=status.HTTP_404_NOT_FOUND,
            )

    push_files = []
    for file in files:
        save_upload_file(file, pathlibPath(file.filename))
        push_files.append(file.filename)

    try:
        manifest_annotations = {
            "included-files": "; ".join(push_files),
            **json.loads(annotations),
        }
        local_status_code = oras_push(
            push_files, artefact_name, tag_name, manifest_annotations
        )
        return JSONResponse(
            content={"upload": "ok"}, status_code=local_status_code.status_code
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        return JSONResponse(
            content={"upload": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@router.delete(
    "/artefacts/{artefact_name}/tags/{tag_name}",
    description="Delete an artefact version by tag.",
)
async def delete_artefact_by_tag(
    artefact_name: str = Path(description="The name of the artefact."),
    tag_name: str = Path(description="The tag of the artefact."),
):
    try:
        local_res = harbor_delete_artefact_by_tag(artefact_name, tag_name)
        return JSONResponse(
            content={"delete_artefact_by_tag": "ok"},
            status_code=local_res.status_code,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        return JSONResponse(
            content={"delete_artefact_by_tag": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )


@router.delete(
    "/artefacts/{artefact_name}/sha/{sha}",
    description="Delete an artefact given its SHA identifier.",
)
async def delete_artefact_by_sha(
    artefact_name: str = Path(description="The name of the artefact."),
    sha: str = Path(description="The SHA identifier of the artefact"),
):
    try:
        local_res = harbor_delete_artefact_by_sha(
            artefact_name, sha.replace("sha256:", "")
        )
        return JSONResponse(
            content={"delete_artefact_by_sha": "ok"},
            status_code=local_res.status_code,
        )
    except Exception as ex:
        logger.error(str(ex), exc_info=True)
        return JSONResponse(
            content={"delete_artefact_by_sha": "not ok", "exception": str(ex)},
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )
