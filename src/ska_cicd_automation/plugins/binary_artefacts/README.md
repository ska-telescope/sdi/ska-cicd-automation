# SKA CI/CD Automation Services Binary artefacts

This plugin is used to upload oci images in the harbor repository with an internal token. 

It uses FastAPI to create the upload files end point. 
The following environment variables must be present, the token should have API access to the project:

```console
CAR_USERNAME=...
CAR_PASSWORD=...
CAR_TOKEN=...
```
