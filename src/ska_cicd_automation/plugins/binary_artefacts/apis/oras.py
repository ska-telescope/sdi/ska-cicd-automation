import logging
import os
import shutil
from pathlib import Path
from tempfile import NamedTemporaryFile

import oras.client
import oras.defaults
import oras.oci
import requests
from fastapi import Response, UploadFile
from requests.auth import HTTPBasicAuth

logger = logging.getLogger("uvicorn.error")

CAR_HOSTNAME = os.getenv("CAR_HOSTNAME", default="harbor.skao.int")
CAR_REPO = os.getenv("CAR_REPO", default="external-artefacts")
CAR_USERNAME = os.getenv("CAR_USERNAME")
CAR_PASSWORD = os.getenv("CAR_PASSWORD")

BASIC_AUTH = HTTPBasicAuth(CAR_USERNAME, CAR_PASSWORD)


def harbor_health() -> Response:
    url = f"https://{CAR_HOSTNAME}/api/v2.0/health"
    return requests.get(timeout=3, url=url, auth=BASIC_AUTH)


def save_upload_file(upload_file: UploadFile, destination: Path) -> None:
    """
    Utility method to save an fastapi UploadFile to a destination path.
    """
    try:
        with destination.open("wb") as buffer:
            shutil.copyfileobj(upload_file.file, buffer)
    finally:
        upload_file.file.close()


def save_upload_file_tmp(upload_file: UploadFile) -> Path:
    """
    Utility method to save an fastapi UploadFile to a temporary path.
    """
    try:
        suffix = Path(upload_file.filename).suffix
        with NamedTemporaryFile(delete=False, suffix=suffix) as tmp:
            shutil.copyfileobj(upload_file.file, tmp)
            tmp_path = Path(tmp.name)
    finally:
        upload_file.file.close()
    return tmp_path


def oras_push(push_files, name, tag, manifest_annnotations):
    """
    Push push_files in the harbor repository with name,
    tag and manifest_annnotations.
    """
    logger.info("Push Tag %s:%s with files %s", name, tag, push_files)
    client = oras.client.OrasClient(hostname=f"https://{CAR_HOSTNAME}")
    client.set_basic_auth(username=CAR_USERNAME, password=CAR_PASSWORD)
    target = f"{CAR_HOSTNAME}/{CAR_REPO}/{name}:{tag}"
    return client.push(
        config_path=None,
        disable_path_validation=True,
        files=push_files,
        manifest_config=None,
        annotation_file=None,
        manifest_annotations=manifest_annnotations,
        quiet=False,
        target=target,
    )


def oras_pull(name, tag):
    """
    Pull the files available in harbor with name and tag.
    """
    logger.info("Pull Tag %s:%s", name, tag)
    client = oras.client.OrasClient(hostname=f"https://{CAR_HOSTNAME}")
    client.set_basic_auth(username=CAR_USERNAME, password=CAR_PASSWORD)
    target = f"{CAR_HOSTNAME}/{CAR_REPO}/{name}:{tag}"
    dir_name = f"/tmp/{name}_{tag}"
    Path(dir_name).mkdir(parents=True, exist_ok=True)
    return dir_name, client.pull(
        config_path=None,
        allowed_media_type=None,
        overwrite=False,
        outdir=dir_name,
        target=target,
    )


###
# curl -u username:password \
# https://harbor.skao.int/api/v2.0/projects/external-artefacts/repositories
# [
#   {
#     "artifact_count": 1,
#     "creation_time": "2024-05-07T10:58:45.601Z",
#     "id": 403,
#     "name": "external-artefacts/test-file-demosh",
#     "project_id": 10,
#     "pull_count": 0,
#     "update_time": "2024-05-07T10:58:45.601Z"
#   }
# ]
###
def harbor_list_repositories(page=1, page_size=15):
    """
    List repositories from the harbor repository using the harbor rest api.

    Keyword arguments:
    page -- page number to get
    page_size -- Size of a page
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories?page={page}&page_size={page_size}"  # noqa E501
    return requests.get(timeout=3, url=url, auth=BASIC_AUTH)


def harbor_get_artefact_count():
    """
    Get the total number of artefacts inside the CAR_REPO project
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}"  # noqa E501
    return requests.get(timeout=3, url=url, auth=BASIC_AUTH)


###
# $ curl -u username:password \
# https://harbor.skao.int/api/v2.0/projects/external-artefacts/repositories/nginx/artifacts
# [
#   {
#     "accessories": null,
#     "annotations": {
#       "included-files": "nginx.tar"
#     },
#     "digest": "sha256:9d383d6047e868935cd04daa42649a8a0952f107e19c1c864b18fff4f644889c",  # noqa E501
#     "icon": "sha256:da834479c923584f4cbcdecc0dac61f32bef1d51e8aae598cf16bd154efab49f",  # noqa E501
#     "id": 13356,
#     "labels": null,
#     "manifest_media_type": "application/vnd.oci.image.manifest.v1+json",
#     "media_type": "application/vnd.unknown.config.v1+json",
#     "project_id": 10,
#     "pull_time": "0001-01-01T00:00:00.000Z",
#     "push_time": "2024-05-15T11:20:43.985Z",
#     "references": null,
#     "repository_id": 399,
#     "size": 241336849,
#     "tags": [
#       {
#         "artifact_id": 13356,
#         "id": 8699,
#         "immutable": false,
#         "name": "stable-perl",
#         "pull_time": "0001-01-01T00:00:00.000Z",
#         "push_time": "2024-05-15T11:20:43.996Z",
#         "repository_id": 399
#       }
#     ],
#     "type": "UNKNOWN"
#   },
###
def harbor_repo_artefacts(repo, page=1, page_size=15):
    """
    List artefacts from the harbor repository using the harbor rest api.

    Keyword arguments:
    repo -- repository reference
    page -- page number to get
    page_size -- Size of a page
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories/{repo}/artifacts?page={page}&page_size={page_size}"  # noqa E501
    return requests.get(timeout=3, url=url, auth=BASIC_AUTH)


# delete on https://harbor.skao.int/api/v2.0/projects/external-artefacts/repositories/nginx/artifacts/sha256:9d383d6047e868935cd04daa42649a8a0952f107e19c1c864b18fff4f644889c # noqa E501
def harbor_get_artefact_by_sha(repo, sha):
    """
    Get a specific artefact from the harbor
    repository using the harbor rest api.

    Keyword arguments:
    name -- artefact name
    sha -- artefact sha
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories/{repo}/artifacts/sha256:{sha}"  # noqa E501
    return requests.get(timeout=3, url=url, auth=BASIC_AUTH)


def harbor_get_artefact(name, tag):
    """
    Get a specific artefact from the harbor
    repository using the harbor rest api.

    Keyword arguments:
    name -- artefact name
    tag -- artefact tag
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories/{name}/artifacts/{tag}"  # noqa E501
    return requests.get(timeout=3, url=url, auth=BASIC_AUTH)


# delete on https://harbor.skao.int/api/v2.0/projects/external-artefacts/repositories/testagain # noqa E501
def harbor_delete_repository(repo):
    """
    Delete a repository with name "repo" from harbor
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories/{repo}"  # noqa E501
    return requests.delete(timeout=3, url=url, auth=BASIC_AUTH)


# delete on https://harbor.skao.int/api/v2.0/projects/external-artefacts/repositories/nginx/artifacts/sha256:9d383d6047e868935cd04daa42649a8a0952f107e19c1c864b18fff4f644889c # noqa E501
def harbor_delete_artefact_by_sha(repo, sha):
    """
    Delete a artefact from a repository with name "repo" and hash "sha"
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories/{repo}/artifacts/sha256:{sha}"  # noqa E501
    return requests.delete(timeout=3, url=url, auth=BASIC_AUTH)


# delete on https://harbor.skao.int/api/v2.0/projects/external-artefacts/repositories/nginx/artifacts/sha256:9d383d6047e868935cd04daa42649a8a0952f107e19c1c864b18fff4f644889c # noqa E501
def harbor_delete_artefact_by_tag(repo, tag):
    """
    Delete a artefact from a repository with name "repo" and tag
    """
    artefact = harbor_get_artefact(repo, tag)
    json_artefact = artefact.json()
    try:
        for ar_tag in json_artefact["tags"]:
            if ar_tag["name"] == tag:
                return harbor_delete_artefact_by_sha(
                    repo, str(json_artefact["digest"]).replace("sha256:", "")
                )
    except Exception:
        return Response(status_code=404, content="Tag not Found")


def harbor_delete_artefact_tag(repo, tag):
    """
    Delete a tag from a repository with name "repo" and tag name "tag"
    """
    url = f"https://{CAR_HOSTNAME}/api/v2.0/projects/{CAR_REPO}/repositories/{repo}/artifacts/{tag}/tags/{tag}"  # noqa E501
    return requests.delete(timeout=3, url=url, auth=BASIC_AUTH)
