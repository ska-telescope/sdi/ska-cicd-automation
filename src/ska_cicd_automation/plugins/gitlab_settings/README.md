# SKA CI/CD Automation Services Gitlab Manual Settings Fix

This plugin is used to trigger Gitlab API calls that fix certain settings as recommended by the SKAO.

It uses FastAPI to create webhook that can be set to listen for events triggered by merge request assignees clicking on fix links on Marvin's merge request comments.

Then the plugin validates that an authorized user issued the request through OAuth2, using the access token obtained this way to ensure the user is one of the assigned Gitlab users to the merge request.
