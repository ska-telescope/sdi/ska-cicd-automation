# flake8: noqa E501
""" GitLab Route for managing MR web hooks """

import logging

import aiohttp
import cachetools
from authlib.integrations.starlette_client import OAuth, OAuthError
from fastapi import APIRouter, Request
from fastapi.responses import RedirectResponse
from ska_cicd_services_api.gitlab_api import GitLabApi
from starlette.config import Config
from starlette.responses import HTMLResponse

from ska_cicd_automation.plugins.gitlab_mr.models.fix_comment_generator import (
    ChangeType,
    FixCommentGenerator,
)

logger = logging.getLogger("gunicorn.error")

router = APIRouter()

cache = cachetools.LRUCache(maxsize=512)

config = Config()
oauth = OAuth(config)

CONF_URL = "https://gitlab.com/.well-known/openid-configuration"
oauth.register(
    name="gitlab",
    server_metadata_url=CONF_URL,
    client_kwargs={"scope": "read_user openid profile email"},
)


@router.get("/manual_fix")
async def login(request: Request):
    """Triggered on a MR when Marvin detects that a project has incorrect
    settings and the 'Fix' button is pressed on Marvin's comment"""

    # ST-1030: Initiate Gitlab OAuth2 Authentication
    redirect_uri = request.url_for("fix")

    # ST-1030: Add query params to the redirect_uri
    if request.query_params:
        redirect_uri = str(redirect_uri) + "?" + str(request.query_params)

    # ST-1030: Authorized Redirect
    return await oauth.gitlab.authorize_redirect(
        request, str(redirect_uri).replace("http", "https")
    )


# ST-1030: Linking from GitLab comments only allows GET endpoints to be used
@router.get("/fix")
async def fix(proj_id: int, mr_id: int, setting: str, request: Request):
    """Triggered by the manual_fix OAuth redirection"""

    try:
        # ST-1030: Obtain the user access token and required parameters
        access_token = await get_user_access_token(request)

        logger.debug("User access token: %s", access_token)

        # ST-1030: Establish a Gitlab session
        async with aiohttp.ClientSession() as session:
            api = GitLabApi(session, cache=cache)

            # ST-1030: Build Merge Request web url for redirect
            proj_info: dict = await api.get_project_settings_info(proj_id)
            proj_url = proj_info.get("web_url")
            mr_url: str = f"{str(proj_url)}/merge_requests/{str(mr_id)}"

            # ST-1030: Validate that the user can issue fix requests
            if await validate_user(api, proj_id, mr_id, access_token):
                # ST-1030: Fix the requested setting
                res, error = await fix_setting(api, proj_id, mr_id, setting)

                # ST-1030: Log if an error occurs
                if error:
                    logger.warning("Failed to fix setting: %s", error)

                    logger.debug(proj_url)

                    # ST-1030: Respond to the user with the error outcome
                    if proj_url:
                        return HTMLResponse(
                            f'<h1>{res}</h1><a href="{mr_url}">Go To Merge Request</a>',
                            400,
                        )
                    return HTMLResponse(f"<h1>{res}</h1>", 400)

                # ST-1030: Respond to the user or redirect back
                # to the Merge Request
                if proj_url:
                    return RedirectResponse(mr_url, 302)
                return HTMLResponse(f"<h1>{res}</h1>", 200)
            else:
                logger.info("Invalid User, Project or Merge Request")

                return HTMLResponse(
                    f'<h1>Invalid User, Project or Merge Request</h1><a href="{mr_url}">&larr; Go To Merge Request</a>',
                    status_code=401,
                )

    except OAuthError as error:
        logger.warning("Failed to authenticate user: %s", error)
        return HTMLResponse(f"<h1>{error.error}</h1>", 401)


async def get_user_access_token(request: Request):
    token = await oauth.gitlab.authorize_access_token(request)
    return token.get("access_token")


async def validate_user(api: GitLabApi, proj_id, mr_id, token) -> bool:

    try:
        # ST-1030: Get Gitlab user info using the access token
        user = await api.get_current_user(access_token=token)
    except Exception as e:
        logger.error(e)
        return False
    logger.info("Obtained user info: %s", user)

    # ST-1030: Check if valid user information was obtained
    if not user or "error" in user:
        return False

    # ST-1030: Get the merge request information

    try:
        mr = await api.get_merge_request_info(proj_id=proj_id, mr_id=mr_id)
    except Exception as e:
        logger.error(e)
        return False

    logger.info("Obtained MR info: %s", mr)

    # ST-1030: Check if valid merge request information was obtained
    if not mr or "error" in mr:
        return False

    # ST-1030: Determine that the user is one of the assignees for the MR
    return user["id"] in [mra["id"] for mra in mr["assignees"]]


async def fix_setting(api: GitLabApi, proj_id, mr_id, setting):
    # ST-1030: Log valid received requests for debugging
    logger.info(
        "Fixing setting [%s] for project %s and mr %s.",
        setting,
        proj_id,
        mr_id,
    )

    result = {"error": f"Unknown setting [{setting}]"}
    changeType = ChangeType.SUCCESS

    # ST-1030: Instantiate a comment generator to display the changes made
    commentor = FixCommentGenerator(__name__, api)

    # -------------------- General Settings --------------------

    if setting == "request_access_enabled":
        result = await api.modify_project(
            proj_id,
            request_access_enabled=False,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Request member access not allowed'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "security_and_compliance_access_level":
        result = await api.modify_project(
            proj_id,
            security_and_compliance_access_level="private",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Visibility of security and compliance must be private'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "issues_access_level":
        result = await api.modify_project(
            proj_id,
            issues_access_level="disabled",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Issues is disabled'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "auto_cancel_pending_pipelines":
        result = await api.modify_project(
            proj_id,
            auto_cancel_pending_pipelines="enabled",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Auto-cancel pending pipelines is enabled'.",
            "Setting checked",
            "Failed to check setting",
        )

    # -------------------- Merge Request Settings --------------------

    # ST-1030: Fix the requested setting
    if setting == "force_remove_source_branch":
        result = await api.modify_merge_request(
            proj_id,
            mr_id,
            remove_source_branch=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'delete source branch when MR is accepted'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "squash":
        result = await api.modify_merge_request(
            proj_id,
            mr_id,
            squash=False,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Uncheck 'squash commits when MR is accepted'.",
            "Setting unchecked",
            "Failed to uncheck setting",
        )

    if setting == "squash_option":
        result = await api.modify_project(
            proj_id,
            squash_option="never",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Squash option must be never'.",
            "Setting unchecked",
            "Failed to change setting",
        )

    if setting == "merge_method":
        result = await api.modify_project(
            proj_id,
            merge_method="merge",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Merge Method should be 'Merge Commit'.",
            "Merge Method changed to 'Merge Commit'",
            "Failed to change Merge Method to 'Merge Commit'",
        )

    if setting == "resolve_outdated_diff_discussions":
        result = await api.modify_project(
            proj_id,
            resolve_outdated_diff_discussions=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'automatically resolve mr diff discussions'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "printing_merge_request_link_enabled":
        result = await api.modify_project(
            proj_id,
            printing_merge_request_link_enabled=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            (
                "Check 'show link to create/view MR when pushing "
                "from the command line'."
            ),
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "remove_source_branch_after_merge":
        result = await api.modify_project(
            proj_id,
            remove_source_branch_after_merge=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'enable delete source branch option by default'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "only_allow_merge_if_pipeline_succeeds":
        result = await api.modify_project(
            proj_id,
            only_allow_merge_if_pipeline_succeeds=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'pipelines must succeed'.",
            "Setting checked",
            "Failed to check setting",
        )

    # ST-1030: If the number of approvals required is 0, it needs to be
    # set to at least 1
    if setting == "approvals_required":
        logger.info("Increasing approvals required to merge")

        # ST-1030: Determine if the number of approvals on the MR
        # can be changed by checking the project-level approval rules
        proj_mr_approvals = await api.get_project_merge_request_approvals_info(
            proj_id
        )

        # ST-1030: Overriding approvers per merge request needs to be
        # enabled to allow modifying the number of approvals
        if proj_mr_approvals["disable_overriding_approvers_per_merge_request"]:
            logger.info("Temporarily overriding approvers per merge")
            await api.modify_project_level_mr_approval_settings(
                proj_id,
                disable_overriding_approvers_per_merge_request=False,
            )

        # ST-1030: Set the number of required approvals to at least 1
        result = await api.modify_mr_level_mr_approval_settings(
            proj_id, mr_id, approvals_required=1
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            (
                "Set at least 1 approval rule to ensure merge "
                "request is reviewed and approved."
            ),
            "Approval rule set",
            "Failed to set approval rule",
        )

        # ST-1030: Restore approvers per merge request if previously
        # overwritten and is not to be fixed
        if proj_mr_approvals["disable_overriding_approvers_per_merge_request"]:
            logger.info("Restoring overriding approvers per merge")
            await api.modify_project_level_mr_approval_settings(
                proj_id,
                disable_overriding_approvers_per_merge_request=True,
            )

    if setting == "disable_overriding_approvers_per_merge_request":
        result = await api.modify_project_level_mr_approval_settings(
            proj_id,
            disable_overriding_approvers_per_merge_request=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Prevent editing approval rules in merge requests'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "reset_approvals_on_push":
        result = await api.modify_project_level_mr_approval_settings(
            proj_id,
            reset_approvals_on_push=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            (
                "Check 'remove all approvals when commits are added "
                "to the source branch'."
            ),
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "merge_requests_author_approval":
        result = await api.modify_project_level_mr_approval_settings(
            proj_id,
            merge_requests_author_approval=False,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Prevent approval by author'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "allow_merge_on_skipped_pipeline":
        result = await api.modify_project(
            proj_id,
            allow_merge_on_skipped_pipeline=False,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'MRs cannot be merged with skipped jobs'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "only_allow_merge_if_all_status_checks_passed":
        result = await api.modify_project(
            proj_id,
            only_allow_merge_if_all_status_checks_passed=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Status checks must succeed'.",
            "Setting checked",
            "Failed to check setting",
        )

    # -------------------- CI/CD Settings --------------------

    if setting == "auto_devops_enabled":
        result = await api.modify_project(
            proj_id,
            auto_devops_enabled=False,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Auto Deveops is Disabled'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "build_git_strategy":
        result = await api.modify_project(
            proj_id,
            build_git_strategy="fetch",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Git strategy is fetch'.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "ci_config_path":
        result = await api.modify_project(
            proj_id,
            ci_config_path="",
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Path to CI configuration file is default.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "keep_latest_artifact":
        result = await api.modify_project(
            proj_id,
            keep_latest_artifact=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Keep the latest artifact is enabled.",
            "Setting checked",
            "Failed to check setting",
        )

    if setting == "job_token_scope":
        result = await api.modify_job_token_access(
            proj_id,
            enabled=True,
        )
        if "error" in result:
            changeType = ChangeType.FAILURE
        await commentor.add_entry(
            changeType,
            "Check 'Job token scope access limited.",
            "Setting checked",
            "Failed to check setting",
        )

    # ST-1030: Add a comment (only when an entry was added)
    await commentor.execute(proj_id, mr_id)

    # ST-1030: Check for errors
    if "error" in result:
        return "Failed to fix setting", result["error"]

    return "Setting fixed successfully", None
