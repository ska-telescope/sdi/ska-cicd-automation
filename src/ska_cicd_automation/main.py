"""
This is a template web server for ska-devops related jobs
"""
import asyncio
import logging
import os
import sys
from contextlib import asynccontextmanager
from pathlib import Path

import yaml
from fastapi import FastAPI, Request, Security, status
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from starlette.middleware.sessions import SessionMiddleware

from ska_cicd_automation.models.Authentication import FactoryAuthenticator
from ska_cicd_automation.models.utils.periodic import Periodic
from ska_cicd_automation.plugins.gitlab_mr.models.mrmetrics import MRMetrics
from ska_cicd_automation.plugins.metrics_server.models.metricsserver import (
    MetricsServer,
)

logger = logging.getLogger("gunicorn.error")

try:
    config_path = Path(os.environ["JSON_CONFIG_PATH"])
    logger.info("JSON_CONFIG_PATH: %s", config_path)
except KeyError:
    sys.exit("ERROR: JSON_CONFIG_PATH environment variable not set.")

plugins = {}
with open(file=config_path, mode="r", encoding="utf-8") as f:
    plugins = yaml.safe_load(f)

logger.info("########### Plugins: \n%s", plugins)

NAMESPACE_PREFIX = os.getenv("NAMESPACE_PREFIX", default="/ska-devops")
APP_NAME = os.getenv("APP_NAME", default="ska-cicd-automation")


METRICS_UPDATE_PERIOD = os.environ["METRICS_UPDATE_PERIOD"]
MR_METRICS_LIFESPAN = os.environ["MR_METRICS_LIFESPAN"].lower() == "true"
METRICS_SERVER_LIFESPAN = (
    os.environ["METRICS_SERVER_LIFESPAN"].lower() == "true"
)


@asynccontextmanager
async def mrmetrics_lifespan():
    if MR_METRICS_LIFESPAN:
        mr_metrics = MRMetrics()
        if mr_metrics.is_feature_enabled():
            periodic = Periodic(
                mr_metrics.scheduled_metrics_update,
                int(METRICS_UPDATE_PERIOD),
                "scheduled_metrics_update",
            )
            task = asyncio.create_task(periodic.start())
            yield
            await periodic.stop()
            await task
        else:
            yield
    else:
        yield


@asynccontextmanager
async def metricsserver_lifespan():
    if METRICS_SERVER_LIFESPAN:
        metrics_server = MetricsServer()
        if metrics_server.is_feature_enabled():
            periodic = Periodic(
                metrics_server.scheduled_metrics_update,
                int(METRICS_UPDATE_PERIOD),
                "scheduled_metrics_update",
            )
            task = asyncio.create_task(periodic.start())
            yield
            await periodic.stop()
            await task
        else:
            yield
    else:
        yield


@asynccontextmanager
async def lifespan(app: FastAPI):  # pylint: disable=W0621,W0613
    """Combine multiple lifespan context managers."""
    async with mrmetrics_lifespan(), metricsserver_lifespan():
        yield


app = FastAPI(
    docs_url=f"{NAMESPACE_PREFIX}/{APP_NAME}/docs",
    openapi_url=f"{NAMESPACE_PREFIX}/{APP_NAME}/openapi.json",
    lifespan=lifespan,
)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(
    request: Request, exc: RequestValidationError
):
    exc_str = f"{exc}".replace("\n", " ").replace("   ", " ")
    logging.error("%s: %s", request, exc_str)
    content = {"status_code": 10422, "message": exc_str, "data": None}
    return JSONResponse(
        content=content, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY
    )


# OpenID/Oauth2.0 Middleware
app.add_middleware(
    SessionMiddleware,
    secret_key=os.environ["OPENID_SECRET"],  # NOQA: E501
)


@app.get("/")
def test():
    return 200


for plugin_conf in plugins.get("plugins"):

    module = __import__(
        plugin_conf["router"]["module"],
        fromlist=[plugin_conf["router"]["class"]],
    )
    clazz = getattr(module, plugin_conf["router"]["class"])

    try:
        authentication = FactoryAuthenticator(plugin_conf)
    except ValueError as e:
        error = f"Bad config on router {plugin_conf.get('name')}: {str(e)}"
        sys.exit(error)

    if authentication.authenticator is None:
        app.include_router(
            clazz.router,
            prefix=plugin_conf["prefix"].rstrip("/"),
            tags=plugin_conf["tags"],
            responses={404: {"description": "Not found"}},
        )
    else:
        # initiate router with the authentication dependency
        app.include_router(
            clazz.router,
            prefix=plugin_conf["prefix"].rstrip("/"),
            tags=plugin_conf["tags"],
            dependencies=[Security(authentication.authenticate)],
            responses={404: {"description": "Not found"}},
        )
