import logging
import os

from UnleashClient import UnleashClient

from ska_cicd_automation.models.utils.singleton import SingletonClass

logger = logging.getLogger("uvicorn.error")


class FeatureToggler(metaclass=SingletonClass):
    __allow_reinitialization = False

    def __init__(self):
        # Check if the instance is already initialized
        self.init_feature_toggler()
        self.initialized = True

    def init_feature_toggler(self) -> None:
        """
        Initialize the feature toggler.
        """
        if "UNLEASH_INACTIVE" not in os.environ:
            try:
                self.unleash_client = UnleashClient(
                    url=os.environ["UNLEASH_PROXY_URL"] + "/proxy",
                    app_name=os.getenv(
                        "UNLEASH_ENVIRONMENT", default="development"
                    ),
                    custom_headers={
                        "Authorization": os.environ["UNLEASH_PROXY_SDK_TOKEN"]
                    },
                    disable_metrics=True,
                    disable_registration=True,
                    verbose_log_level=logger.getEffectiveLevel(),
                    refresh_interval=60,  # Refresh every 60 seconds
                )
                self.unleash_client.initialize_client()
                if not self.unleash_client.is_initialized:
                    self.unleash_client.unleash_verbose_log_level = (
                        logging.NOTSET
                    )
            except Exception as e:
                logger.error("Error initializing feature toggler: %s", e)
                self.unleash_client.unleash_verbose_log_level = logging.NOTSET
