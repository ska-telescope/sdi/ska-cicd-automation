class SingletonClass(type):
    """
    A metaclass to implement the Singleton pattern.

    This metaclass ensures only one instance of the derived class is created.
    If an instance already exists, the existing instance is returned.
    """

    _instances = {}
    # We are redefining (overriding) the __call__ method to control how
    # instances are created.

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            # If no instance exists, create one and store it in _instances.
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        else:
            instance = cls._instances[cls]
            # If the class allows reinitialization, call __init__ again.
            if (
                hasattr(cls, "__allow_reinitialization")
                and cls.__allow_reinitialization
            ):
                instance.__init__(*args, **kwargs)
        return instance
