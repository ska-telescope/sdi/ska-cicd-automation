"""
Module: Authentication

This module provides classes for implementing various authentication methods in a FastAPI application.

Classes:
    - TokenAuthenticator: Authenticates requests based on a predefined token.
    - PasswordAuthenticator: Authenticates requests using a basic username/password mechanism.
    - HmacSignatureAuthenticator: Authenticates requests using HMAC (Hash-based Message Authentication Code) signatures.
    - FactoryAuthenticator: Main class for handling authentication based on the specified authentication type.

"""  # NOQA: E501
import base64
import hashlib
import hmac
import logging
import os

from fastapi import HTTPException, Request, status
from fastapi.security.api_key import APIKeyHeader

logger = logging.getLogger("gunicorn.error")


class TokenAuthenticator:
    def __init__(self, config):
        try:
            self.token_header = os.getenv(config.get("token_header_env"))
            self.token = os.getenv(config.get("token_env"))
        except TypeError:
            raise TypeError("Token authentication fields are invalid.")

    async def authenticate(self, request: Request):
        key_retriever = APIKeyHeader(name=self.token_header, auto_error=True)
        token = await key_retriever(request)
        if token != self.token:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid API Key",
            )


class PasswordAuthenticator:
    def __init__(self, config):
        try:
            self.username = os.getenv(config.get("username_env"))
            self.password = os.getenv(config.get("password_env"))
        except TypeError:
            raise TypeError("Password authentication fields are invalid.")

    async def authenticate(self, request: Request):
        auth_code = request.headers.get("authorization")
        if auth_code is None:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Authorization failed.",
            )

        user_password_coded = auth_code.split(" ")[1]
        assert (
            user_password_coded is not None
        ), "Bad basic authorization request"

        user_password_bytes = user_password_coded.encode("utf-8")
        user_password = base64.b64decode(user_password_bytes).decode("utf-8")

        username, password = user_password.split(":")

        if username != self.username or password != self.password:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid username and/or password",
            )


class HmacSignatureAuthenticator:
    def __init__(self, config):
        try:
            self.hmac_signature_header = config.get("hmac_signature_header")
            self.hmac_secret = os.getenv(config.get("hmac_secret_env"))
            if self.hmac_signature_header is None:
                raise TypeError(
                    "Hmac Signature authentication fields are invalid."
                )
        except TypeError:
            raise TypeError(
                "Hmac Signature authentication fields are invalid."
            )

    async def authenticate(self, request: Request):
        key_retriever = APIKeyHeader(
            name=self.hmac_signature_header, auto_error=True
        )
        hmac_signature = await key_retriever(request)

        rq_body = await request.body()
        secret_encoded = bytes(self.hmac_secret, "utf-8")

        digest = self.generate_hmac_signature(rq_body, secret_encoded)

        if hmac_signature != digest:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid HMAC Signature or corrupted payload received",
            )

    def generate_hmac_signature(self, data, secret):
        digest_maker = hmac.new(secret, data, hashlib.sha1)
        return digest_maker.hexdigest()


class FactoryAuthenticator:
    """
    Authentication

    Class for handling authentication based on the specified authentication type.

    Methods:
        - authenticate(self, request: Request): Authenticates the request based on the configured authentication type.
    """  # NOQA: E501

    def __init__(self, auth_config):
        self.authenticator = None
        auth_type = auth_config.get("auth_type")

        match auth_type:
            case "token":
                self.authenticator = TokenAuthenticator(auth_config)
            case "password":
                self.authenticator = PasswordAuthenticator(auth_config)
            case "hmac_signature":
                self.authenticator = HmacSignatureAuthenticator(auth_config)
            case "none":
                self.authenticator = None
            case _:
                raise ValueError(
                    "Authentication type field is invalid. Requested type: %s"
                    % auth_type,
                )

    async def authenticate(self, request: Request):
        if self.authenticator is not None:
            await self.authenticator.authenticate(request)
