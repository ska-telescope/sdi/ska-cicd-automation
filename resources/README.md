### MR Body
The `mr_body.json` file contains the body of a request from a [GitLab Merge request](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/merge_requests/17).
It can be used in a local environment to trigger Marvin to comment on that Merge Request.