# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define the Docker tag 
# for this project. The definition below inherits the standard value for 
# CAR_OCI_REGISTRY_HOST (=artefact.skao.int) and overwrites PROJECT to
# give a final Docker tag of artefact.skao.int/ska-cicd-automation
#
PROJECT = ska-cicd-automation

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-devops

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= ska-cicd-automation

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART ?= ska-cicd-automation
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
TEST_RUNNER ?= runner-$(CI_JOB_ID)-$(RELEASE_NAME)##name of the pod running the k8s_tests

# Helm version
HELM_VERSION = v3.4.0
# kubectl version
KUBERNETES_VERSION = v1.19.2

# Docker, K8s and Gitlab CI variables
# gitlab-runner debug mode - turn on with non-empty value
RDEBUG ?=
# DOCKER_HOST connector to gitlab-runner - local domain socket for shell exec
DOCKER_HOST ?= unix:///var/run/docker.sock
# DOCKER_VOLUMES pass in local domain socket for DOCKER_HOST
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
# registry credentials - user/pass/registry - set these in PrivateRules.mak
DOCKER_REGISTRY_USER_LOGIN ?=  ## registry credentials - user - set in PrivateRules.mak
CI_REGISTRY_PASS_LOGIN ?=  ## registry credentials - pass - set in PrivateRules.mak
CI_REGISTRY ?= gitlab.com

JSON_CONFIG_PATH ?= "src/ska_cicd_automation/plugins.conf.yaml"

# Gitlab and Jira vars: set in PrivateRules.mak and as environment variables
SKAO_GITLAB_URL ?= https://gitlab.com
GITLAB_API_PRIVATE_TOKEN ?= mandatory## Privte Gitlab Access token - generate at https://gitlab.com/-/profile/personal_access_tokens
GITLAB_API_REQUESTER ?=  mandatory## Developer's Gitlab access token - stored as env variable on Gitlab for production
JIRA_URL ?=
JIRA_USERNAME ?=
JIRA_PASSWORD ?=  ## Jira password
CAR_USERNAME ?=
CAR_PASSWORD ?=  ## Jira password
SLACK_BOT_TOKEN ?= ## Token for Slack bot - create on Slack admin page
SLACK_SIGNING_SECRET ?= ## Slack signing secret - create on Slack admin page
GITLAB_TOKEN ?=
GITLAB_HEADER ?= X-Gitlab-Token
CAR_TOKEN ?=
CAR_HEADER ?= X-Car-Token
UNLEASH_API_URL ?=
UNLEASH_INSTANCE_ID ?=
UNLEASH_ENVIRONMENT ?= default
RTD_TOKEN ?= "mandatory"
NEXUS_HMAC_SIGNATURE_SECRET ?= trytoguess
MONGO_METRICS_DB ?= metrics
NAMESPACE_PREFIX ?= false
MR_METRICS_LIFESPAN ?=
METRICS_SERVER_LIFESPAN ?=
METRICS_UPDATE_PERIOD ?=

STORAGE ?= standard## Redis StorageClass for Persistent Volume(s)

# Validation alerting vars: set in PrivateRules.mak and as environment variables
SLACK_WEBHOOK_URL ?=

# ST-1169: Whether the deployment is targetting minikube or not
MINIKUBE ?= false

# Run from local image only, requires either a pulled or local image 
# always run "latest" by default in dev environment
CUSTOM_VALUES ?= --set image.pullPolicy=Never \
	--set image.tag=latest

ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set image.repository=$(CI_REGISTRY)/ska-telescope/sdi/$(PROJECT)/$(PROJECT) \
	--set image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/sdi/$(PROJECT)/$(PROJECT):$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
CUSTOM_VALUES = --set image.repository=harbor.skao.int/staging/$(PROJECT) \
	--set image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST = harbor.skao.int/staging/$(PROJECT):$(VERSION)
endif

# In production environment get docker image from Nexus (not GitLab)
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep production)
ifneq ($(ENV_CHECK),)
CUSTOM_VALUES = 
endif

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

K8S_TEST_AUX_DIRS ?= 

HELM_RELEASE = ska-cicd-automation

# XAUTHORITYx ?= ${XAUTHORITY}
# THIS_HOST := $(shell ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
# DISPLAY := $(THIS_HOST):0
K8S_TEST_TO_RUN ?= ./tests
K8S_TEST_TEST_COMMAND ?= $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) \
						pytest \
						$(PYTHON_VARS_AFTER_PYTEST) $(K8S_TEST_TO_RUN) \
						| tee pytest.stdout ## k8s-test test command to run in container


#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/oci.mk
include .make/k8s.mk
include .make/python.mk
include .make/helm.mk
include .make/base.mk

# define private overrides for above variables in here
-include PrivateRules.mak

.PHONY: help

PLUGIN_CONFIG ?= data/default.yaml
MULTI_CONFIG ?= gitlab_mr.yaml jira_support.yaml nexus_webhook.yaml


K8S_CHART_PARAMS = \
	--set ingress.class=$(INGRESS) \
	--set celery.redis.auth.password=$(REDIS_PASSWORD) \
	--set celery.mongodb.auth.rootPassword=$(MONGO_ROOT_PASSWORD) \
	--set global.env.jira_url=$(JIRA_URL) \
	--set global.env.jira_username=$(JIRA_USERNAME) \
	--set global.env.jira_password=$(JIRA_PASSWORD) \
	--set global.env.gitlab_api_requester='"$(GITLAB_API_REQUESTER)"' \
	--set global.env.gitlab_api_private_token='$(GITLAB_API_PRIVATE_TOKEN)' \
	--set global.env.gitlab_header=$(GITLAB_HEADER) \
	--set global.env.gitlab_token=$(GITLAB_TOKEN) \
	--set global.env.unleash_proxy_url=$(UNLEASH_PROXY_URL) \
	--set global.env.unleash_proxy_sdk_token=$(UNLEASH_PROXY_SDK_TOKEN) \
	--set global.env.unleash_environment=$(UNLEASH_ENVIRONMENT) \
	--set global.env.slack_signing_secret=$(SLACK_SIGNING_SECRET) \
	--set global.env.slack_bot_token=$(SLACK_BOT_TOKEN) \
	--set global.env.rtd_token=$(RTD_TOKEN) \
	--set global.env.google_api_key=$(GOOGLE_API_KEY) \
	--set global.env.google_spreadsheet_id=$(GOOGLE_SPREADSHEET_ID) \
	--set global.env.nexus_hmac_signature_secret=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	--set global.env.openid_secret=$(OPENID_SECRET) \
	--set global.env.binary_gitlab_client_id=$(GITLAB_CLIENT_ID) \
	--set global.env.binary_gitlab_client_token=$(GITLAB_CLIENT_TOKEN) \
	--set global.env.gitlab_client_secret=$(GITLAB_CLIENT_SECRET) \
	--set plugin.configuration='$(PLUGIN_CONFIG)' \
	--set global.storageClass='$(STORAGE)' \
	--set global.env.car_username=$(CAR_USERNAME) \
	--set global.env.car_password=$(CAR_PASSWORD) \
	--set global.env.car_header=$(CAR_HEADER) \
	--set global.env.car_token=$(CAR_TOKEN) \
	--set global.env.mr_metrics_lifespan=${MR_METRICS_LIFESPAN} \
	--set global.env.metrics_server_lifespan=${METRICS_SERVER_LIFESPAN} \
    --set global.env.metrics_update_period=$(METRICS_UPDATE_PERIOD) \
	--set ingress.namespacePrefix=$(NAMESPACE_PREFIX) \
	$(CUSTOM_VALUES)


k8s_test_kubectl_run_args = \
	$(k8s_test_runner) --restart=Never --pod-running-timeout=$(K8S_TIMEOUT) \
	--image-pull-policy=IfNotPresent --image=$(K8S_TEST_IMAGE_TO_TEST) \
	--env=INGRESS_HOST=$(INGRESS_HOST) \
	--env="NEXUS_URL=$(TEST_NEXUS_URL)" \
	--env="NEXUS_API_USERNAME=$(TEST_NEXUS_API_USERNAME)" \
	--env="NEXUS_API_PASSWORD=$(TEST_NEXUS_API_PASSWORD)" \
	--env="CELERY_BROKER=$(CELERY_BROKER)" \
	--env="MONGO_URL=$(MONGO_URL)" \
	--env="MONGO_METRICS_DB=$(MONGO_METRICS_DB)" \
	--env="GITLAB_API_PRIVATE_TOKEN=$(GITLAB_API_PRIVATE_TOKEN)" \
	--env="GITLAB_API_REQUESTER=$(GITLAB_API_REQUESTER)" \
	--env="UNLEASH_PROXY_URL=$(UNLEASH_PROXY_URL)" \
	--env="UNLEASH_PROXY_SDK_TOKEN=$(UNLEASH_PROXY_SDK_TOKEN)" \
	--env="MR_METRICS_LIFESPAN=${MR_METRICS_LIFESPAN}" \
	--env="METRICS_SERVER_LIFESPAN=${METRICS_SERVER_LIFESPAN}" \
	--env="METRICS_UPDATE_PERIOD=${METRICS_UPDATE_PERIOD}" \
	$(PROXY_VALUES)


PYTHON_VARS_BEFORE_PYTEST = \
	SKAO_GITLAB_URL=$(SKAO_GITLAB_URL) \
	PRIVATE_TOKEN=$(PRIVATE_TOKEN) REQUESTER=$(REQUESTER) \
	GITLAB_TOKEN=$(GITLAB_TOKEN) GITLAB_HEADER=$(GITLAB_HEADER) \
	UNLEASH_PROXY_URL=$(UNLEASH_PROXY_URL) UNLEASH_PROXY_SDK_TOKEN=$(UNLEASH_PROXY_SDK_TOKEN) \
	JIRA_PASSWORD=$(JIRA_PASSWORD) JIRA_USERNAME=$(JIRA_USERNAME) JIRA_URL=$(JIRA_URL) \
	SLACK_SIGNING_SECRET=$(SLACK_SIGNING_SECRET) SLACK_BOT_TOKEN=$(SLACK_BOT_TOKEN) \
	GOOGLE_API_KEY=$(GOOGLE_API_KEY) GOOGLE_SPREADSHEET_ID=$(GOOGLE_SPREADSHEET_ID) \
	CAR_PASSWORD=$(CAR_PASSWORD) CAR_USERNAME=$(CAR_USERNAME) CAR_TOKEN=$(CAR_TOKEN) CAR_HEADER=$(CAR_HEADER) \
	RTD_TOKEN=$(RTD_TOKEN) \
	JSON_CONFIG_PATH=$(JSON_CONFIG_PATH) \
	NEXUS_HMAC_SIGNATURE_SECRET=$(NEXUS_HMAC_SIGNATURE_SECRET) \
	OPENID_SECRET=$(OPENID_SECRET) \
	GITLAB_CLIENT_ID=$(GITLAB_CLIENT_ID) \
	GITLAB_CLIENT_SECRET=$(GITLAB_CLIENT_SECRET) \
	MR_METRICS_REGISTRY_PATH=${MR_METRICS_REGISTRY_PATH"} METRICS_SERVER_REGISTRY_PATH=${METRICS_SERVER_REGISTRY_PATH} \
	MR_METRICS_LIFESPAN=${MR_METRICS_LIFESPAN} METRICS_SERVER_LIFESPAN=${METRICS_SERVER_LIFESPAN} \
	METRICS_UPDATE_PERIOD=${METRICS_UPDATE_PERIOD} \
	PYTHONPATH=${PYTHONPATH}:.:./src

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment' --disable-pytest-warnings

ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
PYTHON_VARS_AFTER_PYTEST := -m 'post_deployment' --disable-pytest-warnings
endif

gitlab_login:
	docker login registry.gitlab.com -u $(DOCKER_REGISTRY_USER_LOGIN) -p $(CI_REGISTRY_PASS_LOGIN)

oci-redeploy-updated-docker-images: ## Build updated Docker images and redeploy on k8s
	eval $$(minikube docker-env); \
	make docker-build DOCKER_BUILD_ARGS=--no-cache; \
	make multiple-reinstall-charts JSON_CONFIG_PATH=$(JSON_CONFIG_PATH);

#
# IMAGE_TO_TEST defines the tag of the Docker image to test
#
ifneq ($(CI_JOB_ID),)
IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/sdi/$(PROJECT):$(VERSION)
else
IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/$(PROJECT):$(VERSION)
endif

OCI_PORT=3003
#
# Never use the network=host mode when running CI jobs, and add extra
# distinguishing identifiers to the network name and container names to
# prevent collisions with jobs from the same project running at the same
# time.
#
ifneq ($(CI_JOB_ID),)
NETWORK_MODE := ska-devops-$(CI_JOB_ID)
CONTAINER_NAME_PREFIX := $(PROJECT)-$(CI_JOB_ID)-
else
CONTAINER_NAME_PREFIX := $(PROJECT)-
endif

oci-interactive:  ## start an interactive session using the project image (caution: R/W mounts source directory to /src/ska_cicd_automation)
	docker run --rm -it -p $(OCI_PORT):80 --name=$(CONTAINER_NAME_PREFIX)dev \
	  -e LOG_LEVEL="debug" --env-file=PrivateRules.mak -e JSON_CONFIG_PATH="ska_cicd_automation/plugins.conf.yaml" -v $(CURDIR)/src/:/code $(IMAGE_TO_TEST) /bin/bash

oci-development: ## start an interactive local development with hot-reload enabled
	docker run --rm -it -p $(OCI_PORT):80 --name=$(CONTAINER_NAME_PREFIX)dev \
	  -e LOG_LEVEL="debug" --env-file=PrivateRules.mak -e JSON_CONFIG_PATH=$(JSON_CONFIG_PATH) -v $(CURDIR)/src/:/code $(IMAGE_TO_TEST) uvicorn ska_cicd_automation.main:app --proxy-headers --host 0.0.0.0 --port 80 --reload

prepare-k8s-test:
	@echo "test file" > tests/upload.txt

k8s-do-test: prepare-k8s-test

# https://stackoverflow.com/a/59097246/4556564
vars: ## Print out defined variables
	$(foreach v, $(.VARIABLES), $(if $(filter file,$(origin $(v))), $(info $(v)=$($(v)))))
.PHONY: vars
