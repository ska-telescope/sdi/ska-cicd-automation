# SKA CI/CD Automation Services

This project is the template projects that is intended for use by SKA CICD Services API Applications (_mainly bots_).
The project and generated projects from this template should follow the [SKA Developer Portal](https://developer.skatelescope.org/en/latest/) closely.
Please check the latest guidelines, standards and practices before moving forward!

## Structure

Project structure is following a basic python file structure for [FastAPI](https://fastapi.tiangolo.com/) as below:

```console
.
├── charts
│   └── ...
├── docs
│   └── ...
├── src
│   └── ...
└── tests
│   └── ...
├── Dockerfile
├── LICENSE
├── Makefile
├── pyproject.toml
└── README.md

```

## Technologies:

- Docker: To build a docker image that exposes port _80_ for API endpoints.
- Kubernetes and Helm: Project also includes a helm chart to deploy the above image in a loadbalanced kubernetes cluster.
- Python Package: Project also includes a python package so that it can be downloaded as such. (**The usability of this capability highly depends on the actual implementation!**)

## Local Development

### General Workflow

Update and initialize the submodules used in the project:
```console
git submodule update --init --recursive
```

Install [Poetry](https://python-poetry.org/) for Python package and environment management.
```console
curl -sSL https://install.python-poetry.org | python3 -
```

Next, you need to define the variables that are used in each plugin (see each plugin's README file for information on the variables) in your `PrivateRules.mak`. This is necessary for makefile targets.
You may also specify them in a `.env` file (needed for tests from vscode and interactive docker development).

```console
LOG_LEVEL=...
JSON_CONFIG_PATH=...
UNLEASH_API_URL=...
UNLEASH_ENVIRONMENT=...
UNLEASH_INSTANCE_ID=...
GITLAB_API_REQUESTER=...
GITLAB_API_PRIVATE_TOKEN=...
GITLAB_HEADER=...
GITLAB_TOKEN=...
JIRA_URL=...
JIRA_USERNAME=...
JIRA_PASSWORD=...
SLACK_BOT_TOKEN=...
SLACK_SIGNING_SECRET=...
GOOGLE_SPREADSHEET_ID=...
GOOGLE_API_KEY=...
RTD_TOKEN=...
MONGO_URL=...
MONGO_METRICS_DB=...
OPENID_SECRET=...
```

Now, the project is ready for local development.

**Note:** depending on the IDE (_vscode is suggested_), `PYTHONPATH` may need to be adjusted (ex: `PYTHONPATH=:.:./src`) so that IDE picks up imports and tests correctly.


### Deploying the project locally

#### Deploying to local cluster
Make sure the repository has no outstanding changes before deploying.
The project can be deployed using the ```make k8s-install-chart``` target.
By default this will deploy the latest image in the OCI registry. You can specify the image version with `VERSION=0.18.2`.
If you wish to deploy a local image, modify the image specified in the deployment file.

Please note that some modules may break due to incorrect environment variables. You may need to modify the values in the ```PrivateRules.mak```.

#### Running Container
To run the application in a container, use the `make oci-development`, which will use the environment variables defined in ```PrivateRules.mak```. You may also run the container in interactive mode through `make oci-interactive`.
To specify the image, use `IMAGE_TO_TEST=<your-local-image>`.

Please note that you may need to modify the `JSON_CONFIG_PATH` variable to point to the correct path inside the container.

### Interacting with the service
The service exposes multiple endpoints, specified in [src/ska_cicd_automation/plugins.conf.yaml](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/src/ska_cicd_automation/plugins.conf.yaml), for example:
```console
/gitlab/mr/events
/gitlab/mr/pipeline
```
To trigger the Merge Request service locally, you can use the `resources/mr_body.json` file as a payload for the `/gitlab/mr/events` endpoint, which will trigger Marvin to verify the [Merge Request](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/merge_requests/17).

### Live testing
To trigger the Merge Request service directly from GitLab, expose the service port using your preferred tunneling service (eg. Ngrok, localtunnel, loophole.cloud, etc.).

In a Gitlab repository, add a webhook trigger for Merge requests that calls the ```/gitlab/mr/events``` endpoint.

## Adding features to the application
The application contains a series of plugins that implement different functionalities. These are organized in a way that makes their inclusion straightforward.

### Adding a new cicd automation service

To add a new plugin to the main host, follow the appropriate project structure:

```console
.
└── src
    └── ska_cicd_automation
        └── plugins
            └── thenewplugin
                ├── README.md
                ├── routers
                │   └── endpoints.py
                └── modules
                    └── logic.py
.
└── tests
    └── thenewplugin
        ├── pytest.ini
        ├── thenewplugin.feature
        └── test_logic.py
```

The plugin must have the following features:

- there must be one module containing a router object created according to the FastAPI documentation;
- a plugin configuration must be added to [charts/ska-cicd-automation/data/](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/charts/ska-cicd-automation/data/);
- the above plugin configuration must also be added to [app/plugins.conf.yaml](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/app/plugins.conf.yaml);

The configuration item corresponds to the input parameter prefix, tags and a name of the [include router operation](https://fastapi.tiangolo.com/tutorial/bigger-applications/#another-module-with-apirouter) of the FastAPI framework. There's another parameter in the configuration file which specify the package and module file name where to find the router object to be added in the host.

The plugin should also have a README file on its root folder.

#### Plugin Authentication

Every plugin should have its type of authentication. The file [src/ska_cicd_automation/models/Authentication.py](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/src/ska_cicd_automation/models/Authentication.py) already contains the following authentication methods, but can be expanded:

- TokenAuthenticator: Authenticates requests based on a predefined token.
- PasswordAuthenticator: Authenticates requests using a basic username/password mechanism.
- HmacSignatureAuthenticator: Authenticates requests using HMAC (Hash-based Message Authentication Code) signatures.


The authentication configuration should be specified in plugin's [configuration file](https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/blob/master/src/ska_cicd_automation/plugins.conf.yaml).

- `auth_type` represents the type of authentication that the plugin will have. It can be equal to `token`, `password`, `hmac_signature` or `none`.
- `Token`:
  - `token_env` name of the environment variable that contains the token.
  - `token_header_env` name of the environment variable that corresponds to the type of token header of the service to be authenticated, for example for gitlab the token header would be `X-Gitlab-Token`.
- `Username` and `Password`:
  - `username_env` name of the environment variable that contains the username.
  - `password_env` name of the environment variable that contains the password.
- `HMAC Signature`:
  - `hmac_signature_header` the hmac header.
  - `hmac_secret_env` name of the environment variable that contains the hmac secret.

### Adding a new Gitlab Merge Request Check

To add a new Gitlab MR check, use the following template and include in *src/ska_cicd_automation/plugins/gitlab_mr/models/mr_checks*.

```python
import logging
from ska_cicd_services_api.gitlab_api import GitLabApi
from ska_cicd_automation.plugins.gitlab_mr.models.check import Check
from ska_cicd_automation.plugins.gitlab_mr.models.message_generator import MessageType
from ska_cicd_automation.plugins.gitlab_mr.models.mrhook import MRHook

# The name of the class must start with "Check"
class CheckThisIsANewCheck(Check):
    # This toggle must be added to https://gitlab.com/ska-telescope/sdi/ska-cicd-automation/-/feature_flags
    feature_toggle = "new_check_toggle"

    # Add what APIs you need to the __init__ function, but make sure to use type hinting!
    # Example: def __init__(self, api: GitLabApi, rtd_api: ReadTheDocsApi, jira_api: JiraApi, logger_name: str):
    def __init__(self, gitlab_api: GitLabApi, logger_name: str):
        self.gitlab_api = gitlab_api
        self.logger = logging.getLogger(logger_name)

    async def check(self, mr_event: MRHook, proj_id: int, mr_id: int):

        # True means the check passed and there is no problem
        # False will trigger Marvin to write a mitigation strategy
        did_check_pass = True or False

        return did_check_pass

    async def type(self) -> MessageType:
        # This determines the type of message Marvin will issue:

        # Available message types:
        # MessageType.FAILURE - Blocks the MR
        # MessageType.WARNING - Does not block
        # MessageType.INFO

        # You can change the type according to the severity of the check:
        # Example:
        # if self.bad_problem:
        #   return MessageType.FAILURE
        # else:
        #   return MessageType.WARNING

        return MessageType.FAILURE

    async def description(self) -> str:
        return "Description that appears in the Marvin comment"

    async def mitigation_strategy(self) -> str:
        return "Message which should tell the user how to fix the problem"
```

Unit tests should be created using Pytest-BDD. Use the available structure in:

- *tests/unit/gitlab_mr/test_gitlab.py*
- *tests/unit/gitlab_mr/gitlab.feature*

Use the ```make python-test``` target to run the unit tests.
